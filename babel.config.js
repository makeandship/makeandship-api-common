module.exports = {
  presets: ["@babel/preset-env"],
  plugins: ["@babel/plugin-transform-runtime", "add-module-exports"],
  sourceMaps: true,
  retainLines: true
};
