import VirtualFieldsTransformer from "../../src/transformers/VirtualFieldsTransformer";

describe("VirtualFieldsTransformer", () => {
  it("should add virtual fields to the tansformed result", done => {
    const results = {
      _id: 1,
      name: "Brian"
    };
    const transformer = new VirtualFieldsTransformer();
    const result = transformer.transform(results, {
      virtualFields: { nickname: "brain" }
    });

    expect(result._id).toEqual(1);
    expect(result.name).toEqual("Brian");
    expect(result.nickname).toEqual("brain");

    done();
  });
});
