import WhitelistTransformer from "../../src/transformers/WhitelistTransformer";

describe("WhitelistTransformer", () => {
  it("should only add whitelisted fields", done => {
    const results = {
      _id: 1,
      name: "Brian",
      nickname: "brian",
      password: "password"
    };
    const transformer = new WhitelistTransformer();
    const result = transformer.transform(results, {
      whitelist: ["name", "nickname"]
    });

    expect(result._id).toBeUndefined();
    expect(result.name).toEqual("Brian");
    expect(result.nickname).toEqual("brian");
    expect(result.password).toBeUndefined();

    done();
  });
});
