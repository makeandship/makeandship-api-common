import ArrayJSONTransformer from "../../src/transformers/ArrayJSONTransformer";
import ModelJSONTransformer from "../../src/transformers/ModelJSONTransformer";

describe("ArrayJSONTransformer", () => {
  describe("when transforming an empty array", () => {
    it("should return an empty array", done => {
      const results = [];

      const transformer = new ArrayJSONTransformer();
      const result = transformer.transform(results, {
        transformer: ModelJSONTransformer
      });
      expect(result).toEqual([]);

      done();
    });
  });
  describe("when passing a transformmer class", () => {
    it("should transform the array and objects", done => {
      const results = [
        {
          _id: 1,
          name: "Brian"
        },
        {
          _id: 2,
          name: "Thomas"
        }
      ];
      const transformer = new ArrayJSONTransformer();
      const result = transformer.transform(results, {
        transformer: ModelJSONTransformer
      });

      expect(result.length).toEqual(2);
      expect(result[0].id).toBeTruthy();
      expect(result[0]._id).toBeUndefined();

      done();
    });
  });

  describe("when no options passed", () => {
    it("should keep array and objects", done => {
      const results = [
        {
          _id: 1,
          name: "Brian"
        },
        {
          _id: 2,
          name: "Thomas"
        }
      ];
      const transformer = new ArrayJSONTransformer();
      const result = transformer.transform(results, {});

      expect(result.length).toEqual(2);
      expect(result[0].id).toBeUndefined();
      expect(result[0]._id).toBeTruthy();

      done();
    });
  });
});
