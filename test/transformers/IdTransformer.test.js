import IdTransformer from "../../src/transformers/IdTransformer";

describe("IdTransformer", () => {
  it("remove the _id and replace it with id", done => {
    const results = {
      _id: 1,
      name: "Brian"
    };
    const transformer = new IdTransformer();
    const result = transformer.transform(results, {});

    expect(result.id).toBeTruthy();
    expect(result.id).toEqual("1");
    expect(result._id).toBeUndefined();

    done();
  });
});
