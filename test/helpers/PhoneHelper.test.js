import PhoneHelper from "../../src/helpers/PhoneHelper";

describe("PhoneHelper", () => {
  describe("#getPhone", () => {
    describe("when in international format", () => {
      it("should return an E164 formatted number", () => {
        const phone = PhoneHelper.getPhone("+447719480910");
        expect(phone).toEqual("+447719480910");
      });
    });
    describe("when in display format", () => {
      it("should return an E164 formatted number", () => {
        const phone = PhoneHelper.getPhone("+44 7719 480910");
        expect(phone).toEqual("+447719480910");
      });
    });
  });
  describe("#isValid", () => {
    describe("when valid", () => {
      describe("when in international format", () => {
        it("should return true", () => {
          const valid = PhoneHelper.isValid("+447719480910");
          expect(valid).toEqual(true);
        });
      });
      describe("when in display format", () => {
        it("should return true", () => {
          const valid = PhoneHelper.isValid("+44 7719 480910");
          expect(valid).toEqual(true);
        });
      });
    });
    describe("when not valid", () => {
      describe("when a local number", () => {
        it("should return false", () => {
          const valid = PhoneHelper.isValid("07719480910");
          expect(valid).toEqual(false);
        });
      });
      describe("when not a phone number", () => {
        it("should return false", () => {
          const valid = PhoneHelper.isValid("not-a-number");
          expect(valid).toEqual(false);
        });
      });
      describe("when not a valid international number", () => {
        it("should return false", () => {
          const valid = PhoneHelper.isValid("+999829013131");
          expect(valid).toEqual(false);
        });
      });
    });
  });
});
