import { ErrorUtil, APIError } from "../../../src/modules/error";

describe("ErrorUtil functions", () => {
  describe("#convertAjvToApi", () => {
    describe("when the ajv error is empty", () => {
      const ajvError = {};
      it("should convert an empty object", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError);

        expect(validationError.data).toEqual({ errors: {} });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });

    describe("when the ajv error is populated", () => {
      const ajvError = {
        errors: {
          code: {
            path: "code",
            type: "enum",
            params: {
              allowedValues: ["AA", "AB", "A"]
            },
            message: "should be equal to one of the allowed values"
          }
        }
      };

      it("should convert a populated ajv error", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError);

        expect(validationError.data).toEqual({
          errors: {
            code: {
              message: "should be equal to one of the allowed values",
              params: { allowedValues: ["AA", "AB", "A"] },
              path: "code",
              type: "enum"
            }
          }
        });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convertAjvToApi(ajvError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });
  });
  describe("#convertMongooseToApi", () => {
    describe("when the mongoose error is empty", () => {
      const mongooseError = {};
      it("should convert an empty object", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError);

        expect(validationError.data).toEqual({ errors: {} });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });

    describe("when the mongoose error is populated", () => {
      const mongooseError = {
        name: "ValidationError",
        errors: {
          code: {
            path: "code",
            kind: "enum",
            message: "should be equal to one of the allowed values"
          }
        }
      };

      it("should convert a populated mongoose error", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError);

        expect(validationError.data).toEqual({
          errors: {
            code: {
              kind: "enum",
              message: "should be equal to one of the allowed values",
              path: "code"
            }
          }
        });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convertMongooseToApi(mongooseError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });
  });
  describe("#convert", () => {
    describe("when converting a mongoose error", () => {
      const mongooseError = {
        name: "ValidationError",
        errors: {
          code: {
            path: "code",
            kind: "enum",
            message: "should be equal to one of the allowed values"
          }
        }
      };

      it("should convert a populated mongoose error", async () => {
        const validationError = ErrorUtil.convert(mongooseError);

        expect(validationError.data).toEqual({
          errors: {
            code: {
              kind: "enum",
              message: "should be equal to one of the allowed values",
              path: "code"
            }
          }
        });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convert(mongooseError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convert(mongooseError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });

    describe("when converting an ajv error", () => {
      const ajvError = {
        errors: {
          code: {
            path: "code",
            type: "enum",
            params: {
              allowedValues: ["AA", "AB", "A"]
            },
            message: "should be equal to one of the allowed values"
          }
        }
      };

      it("should convert a populated ajv error", async () => {
        const validationError = ErrorUtil.convert(ajvError);

        expect(validationError.data).toEqual({
          errors: {
            code: {
              message: "should be equal to one of the allowed values",
              params: { allowedValues: ["AA", "AB", "A"] },
              path: "code",
              type: "enum"
            }
          }
        });
      });

      it("should set the httpStatus", async () => {
        const validationError = ErrorUtil.convert(ajvError);

        expect(validationError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const validationError = ErrorUtil.convert(ajvError, 10000);

        expect(validationError.code).toEqual(10000);
      });
    });

    describe("when converting unknown error", () => {
      const error = { message: "service unavailable" };

      it("should convert to an api error", async () => {
        const convertedError = ErrorUtil.convert(error);

        expect(convertedError.message).toEqual("service unavailable");
        expect(convertedError instanceof APIError).toBe(true);
      });

      it("should set the httpStatus", async () => {
        const convertedError = ErrorUtil.convert(error);

        expect(convertedError.httpStatus).toEqual(200);
      });

      it("should add the code", async () => {
        const convertedError = ErrorUtil.convert(error, 10000);

        expect(convertedError.code).toEqual(10000);
      });
    });
  });
});
