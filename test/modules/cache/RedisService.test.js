import { redisService } from "../../../src/modules/cache/services";

beforeAll(async done => {
  // seed the cache
  await redisService.set("key0", "value0");
  await redisService.set("key1", "value1");
  await redisService.set("key2", "value2");
  done();
});

afterAll(async () => {
  await redisService.disconnect();
});

describe("RedisService", () => {
  describe("#get", () => {
    describe("when the key exists", () => {
      it("should return the value feom the cache", async () => {
        const value0 = await redisService.get("key0");
        expect(value0).toEqual("value0");

        const value1 = await redisService.get("key1");
        expect(value1).toEqual("value1");

        const value2 = await redisService.get("key2");
        expect(value2).toEqual("value2");
      });
    });
    describe("when the key doesnt exist", () => {
      it("should return null", async () => {
        const value = await redisService.get("invalid");
        expect(value).toEqual(null);
      });
    });
  });
  describe("#set", () => {
    describe("when no expiry is given", () => {
      it("should set the value in the cache", async () => {
        await redisService.set("key3", "value3");

        const value3 = await redisService.get("key3");
        expect(value3).toEqual("value3");
      });
    });
    describe("when an expiry is given", () => {
      it("should remove the item from the cache after expiry has passed", async done => {
        const timeout = ms => new Promise(res => setTimeout(res, ms));

        await redisService.set("key4", "value4", 1);
        const value4 = await redisService.get("key4");
        expect(value4).toEqual("value4");
        await timeout(1050);
        const value4expired = await redisService.get("key4");
        expect(value4expired).toEqual(null);
        done();
      });
    });
  });
  describe("#keys", () => {
    describe("when no pattern is given", () => {
      it("should return null", async done => {
        const keys = await redisService.keys();
        expect(keys).toEqual(null);
        done();
      });
    });
    describe("when a pattern is given", () => {
      it("should return matching keys", async done => {
        const keys = await redisService.keys("key*");
        expect(keys.length).toEqual(4);
        for (const key of keys) {
          expect(["key0", "key1", "key2", "key3"].includes(key)).toEqual(true);
        }
        done();
      });
      it("should return matching keys", async done => {
        const keys = await redisService.keys("key2*");
        expect(keys.length).toEqual(1);
        for (const key of keys) {
          expect(["key2"].includes(key)).toEqual(true);
        }
        for (const key of keys) {
          expect(["key1", "key3"].includes(key)).toEqual(false);
        }
        done();
      });
    });
  });
  describe("#del", () => {
    describe("when invalid", () => {
      describe("when an no key is given", () => {
        it("should do nothing", async () => {
          const keysBefore = await redisService.keys("key*");
          expect(keysBefore.length).toEqual(4);
          await redisService.del();
          const keysAfter = await redisService.keys("key*");
          expect(keysAfter.length).toEqual(4);
        });
      });
    });
    describe("when valid", () => {
      describe("when an array is given", () => {
        it("should remove the keys", async done => {
          await redisService.set("key4", "value4");
          await redisService.set("key5", "value5");
          const key4 = await redisService.get("key4");
          expect(key4).toEqual("value4");
          const key5 = await redisService.get("key5");
          expect(key5).toEqual("value5");

          await redisService.del(["key4", "key5"]);
          const key4deleted = await redisService.get("key4");
          expect(key4deleted).toEqual(null);
          const key5deleted = await redisService.get("key5");
          expect(key5deleted).toEqual(null);

          done();
        });
      });
      describe("when a key is given", () => {
        it("should remove the key", async done => {
          await redisService.set("key4", "value4");
          const key4 = await redisService.get("key4");
          expect(key4).toEqual("value4");

          await redisService.del("key4");
          const key4deleted = await redisService.get("key4");
          expect(key4deleted).toEqual(null);

          done();
        });
      });
    });
  });
});
