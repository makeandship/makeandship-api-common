import SchemaRedisCacheProvider from "../../../src/modules/cache/providers/SchemaRedisCacheProvider";

const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        role: {
          type: "string",
          enumNames: ["administrator", "subscriber", "editor"],
          enum: ["ADM", "SUB", "EDIT"]
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        },
        businessType: {
          title: "Business Type",
          type: "string",
          enum: ["IT", "IND", "TR", "TEL"],
          enumNames: ["Information Technology", "Industry", "Transportation", "Telecom"]
        }
      }
    }
  }
};

let cache = null;

beforeAll(() => {
  const provider = new SchemaRedisCacheProvider(schema);
  cache = provider.getCachePayload();
});

describe("SchemaRedisCacheProvider", () => {
  describe("#getCachePayload", () => {
    it("should return cache keys for attributes with enumNames", () => {
      expect(cache["user.role:ADM"]).toEqual("administrator");
      expect(cache["user.role:SUB"]).toEqual("subscriber");
      expect(cache["user.role:EDIT"]).toEqual("editor");
    });
    it("should return cache keys for nested attributes with enumNames", () => {
      expect(cache["user.organisation.businessType:IT"]).toEqual("Information Technology");
      expect(cache["user.organisation.businessType:IND"]).toEqual("Industry");
      expect(cache["user.organisation.businessType:TR"]).toEqual("Transportation");
      expect(cache["user.organisation.businessType:TEL"]).toEqual("Telecom");
    });
  });
});
