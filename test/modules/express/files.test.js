import files from "../../../src/modules/express/files";
import File from "../../../src/util/file";

describe("files functions", () => {
  describe("#when uploading without a file", () => {
    it("should return the object without file attributes", async () => {
      const response = await files.upload({}, {});
      expect(response).toBe(null);
    });
  });
  describe("#when uploading with a file", () => {
    describe("when uploading standard attachments", () => {
      const file = {
        originalname: "1.json",
        destination: "tmp/",
        filename: "1.json",
        path: "test/fixtures/files/1.json",
        size: 22
      };
      const options = {
        uploadDir: "/tmp/uploads",
        filename: "1.json",
        route: "user/file",
        target: "users/files",
        protocol: "http",
        host: "localhost:3000"
      };

      let response = null;

      beforeEach(async done => {
        response = await files.upload(file, options);
        done();
      });
      it("should return a url to download the file", async done => {
        expect(response).toHaveProperty("url");
        expect(response.url).toEqual("http://localhost:3000/user/file/1.json");
        done();
      });
      it("should return metadata", done => {
        expect(response.filename).toEqual("1.json");
        expect(response.size).toEqual(22);
        done();
      });
      it("should create the file in the system", async done => {
        try {
          const url = response.url;
          const urlElements = url.split("/");
          const name = urlElements[urlElements.length - 1];
          const filePath = `${options.uploadDir}/${options.target}/${name}`;

          const exists = await File.verifyFile(filePath, 50, 3);
          expect(exists).toEqual(filePath);
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    describe("when uploading images", () => {
      const file = {
        originalname: "1.png",
        destination: "tmp/",
        filename: "1.png",
        path: "test/fixtures/files/1.png",
        size: 96973
      };
      const options = {
        uploadDir: "/tmp/uploads",
        imageSizes: [128, 256, 512],
        filename: "1.png",
        route: "user/avatar",
        target: "users/123/avatars",
        protocol: "http",
        host: "localhost:3000"
      };

      let response = null;
      beforeEach(async done => {
        response = await files.upload(file, options);
        done();
      });
      it("should return a file url", done => {
        expect(response).toHaveProperty("url");
        expect(response.url).toEqual("http://localhost:3000/user/avatar/1.png");
        done();
      });
      it("should return a file metadata", done => {
        expect(response.filename).toEqual("1.png");
        expect(response.size).toEqual(96973);
        done();
      });
      it("should return thumbnails", done => {
        expect(response).toHaveProperty("url");
        expect(response).toHaveProperty("thumbnails");

        expect(response.thumbnails.length).toEqual(3);
        done();
      });
      it("should store thumbnails", async () => {
        const url = response.thumbnails[0].url;
        const urlElements = url.split("/");
        const name = urlElements[urlElements.length - 1];
        const code = name.split("-")[0];
        const path128 = `${options.uploadDir}/${options.target}/${code}-128.png`;
        const path256 = `${options.uploadDir}/${options.target}/${code}-256.png`;
        const path512 = `${options.uploadDir}/${options.target}/${code}-512.png`;

        const exists128 = await File.verifyFile(path128, 50, 3);
        expect(exists128).toEqual(path128);

        const exists256 = await File.verifyFile(path256, 50, 3);
        expect(exists256).toEqual(path256);

        const exists512 = await File.verifyFile(path512, 50, 3);
        expect(exists512).toEqual(path512);
      });
    });
  });
});
