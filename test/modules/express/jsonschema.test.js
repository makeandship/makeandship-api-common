import { schemaValidation, trim } from "../../../src/modules/express/middleware/jsonschema";

describe("#schemaValidation", () => {
  describe("when no schema provided", () => {
    it("should throw an error", done => {
      try {
        schemaValidation(null, [], "test");
        done.fail();
      } catch (e) {
        expect(e).toBeTruthy();
        expect(e.message).toEqual("Please provide a validation schema");
        done();
      }
    });
  });
  describe("when the schema is provided", () => {
    it("should return a valid middleware function", done => {
      try {
        const middleware = schemaValidation({}, [], "test");
        expect(middleware).toEqual(expect.any(Function));
        done();
      } catch (e) {
        done.fail();
      }
    });
    describe("when validating", () => {
      describe("when valid", () => {
        describe("when nested attributes", () => {
          it("should call next", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddies: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      name: {
                        type: "string",
                        index: true
                      }
                    }
                  }
                }
              },
              required: ["firstname", "lastname", "buddies"]
            };
            const middleware = schemaValidation(schema, [], {});
            const req = {
              body: {
                firstname: "Mark",
                lastname: "Thomsit",
                buddies: [
                  {
                    name: "Yassire"
                  }
                ]
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);
            expect(res.jerror.mock.calls.length).toEqual(0);
            expect(next.mock.calls.length).toEqual(1);

            done();
          });
        });
        describe("when flat attributes", () => {
          it("should call next", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddy: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              },
              required: ["firstname", "lastname", "buddy"]
            };
            const middleware = schemaValidation(schema, [], {});
            const req = {
              body: {
                firstname: "Mark",
                lastname: "Thomsit",
                "buddy.name": "Yassire"
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);

            expect(res.jerror.mock.calls.length).toEqual(0);
            expect(next.mock.calls.length).toEqual(1);

            done();
          });
        });
        describe("when array attributes", () => {
          it("should call next", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddies: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      name: {
                        type: "string",
                        index: true
                      }
                    }
                  }
                }
              },
              required: ["firstname", "lastname", "buddies"]
            };
            const middleware = schemaValidation(schema, [], {});
            const req = {
              body: {
                firstname: "Mark",
                lastname: "Thomsit",
                "buddies[0].name": "Yassire"
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);
            expect(res.jerror.mock.calls.length).toEqual(0);
            expect(next.mock.calls.length).toEqual(1);

            done();
          });
        });
        describe("when formatting required on attributes", () => {
          it("should call next", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddies: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      name: {
                        type: "string",
                        index: true
                      }
                    }
                  }
                },
                retired: {
                  type: "boolean"
                }
              },
              required: ["firstname", "lastname", "buddies", "retired"]
            };
            const middleware = schemaValidation(schema, [], {});
            const req = {
              body: {
                firstname: "Mark",
                lastname: "Thomsit",
                "buddies[0].name": "Yassire",
                retired: "true"
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);
            expect(res.jerror.mock.calls.length).toEqual(0);
            expect(next.mock.calls.length).toEqual(1);

            done();
          });
        });
      });
      describe("when not valid", () => {
        describe("when message is provided", () => {
          it("should set a message in the output", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddies: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      name: {
                        type: "string",
                        index: true
                      }
                    }
                  }
                },
                retired: {
                  type: "boolean"
                }
              },
              required: ["firstname", "lastname", "buddies", "retired"]
            };
            const middleware = schemaValidation(schema, {
              message: "Override message"
            });
            const req = {
              body: {
                firstname: 1,
                lastname: "Thomsit",
                "buddies[0].name": "Yassire",
                retired: "true"
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);

            expect(next).toHaveBeenCalled();
            const nextAttribute = next.mock.calls[0][0];
            expect(nextAttribute).toHaveProperty("message", "Override message");

            done();
          });
        });
        describe("when message is not provided", () => {
          it("should set a default message in the output", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                buddies: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      name: {
                        type: "string",
                        index: true
                      }
                    }
                  }
                },
                retired: {
                  type: "boolean"
                }
              },
              required: ["firstname", "lastname", "buddies", "retired"]
            };
            const middleware = schemaValidation(schema);
            const req = {
              body: {
                firstname: 1,
                lastname: "Thomsit",
                "buddies[0].name": "Yassire",
                retired: "true"
              }
            };
            const res = {
              jerror: jest.fn()
            };
            const next = jest.fn();
            middleware(req, res, next);

            expect(next).toHaveBeenCalled();
            const nextAttribute = next.mock.calls[0][0];
            expect(nextAttribute).toHaveProperty("message", "Validation failed");

            done();
          });
        });
      });
    });
  });
});

describe("#trim", () => {
  describe("when no schema provided", () => {
    it("should throw an error", done => {
      try {
        trim(null);
        done.fail();
      } catch (e) {
        expect(e).toBeTruthy();
        expect(e.message).toEqual("Please provide a validation schema");
        done();
      }
    });
  });
  describe("when the schema is provided", () => {
    it("should return a valid middleware function", done => {
      try {
        const middleware = trim({});
        expect(middleware).toEqual(expect.any(Function));
        done();
      } catch (e) {
        done.fail();
      }
    });
    describe("when triming", () => {
      describe("when no additional fields", () => {
        it("should call next", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              buddies: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              }
            },
            required: ["firstname", "lastname", "buddies"]
          };
          const middleware = trim(schema);
          const req = {
            body: {
              firstname: "Mark",
              lastname: "Thomsit",
              buddies: [
                {
                  name: "Yassire"
                }
              ]
            }
          };
          const res = {
            jerror: jest.fn()
          };
          const next = jest.fn();
          middleware(req, res, next);
          expect(res.jerror.mock.calls.length).toEqual(0);
          expect(next.mock.calls.length).toEqual(1);

          done();
        });
        it("should keep all the fields", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              buddies: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              }
            },
            required: ["firstname", "lastname", "buddies"]
          };
          const middleware = trim(schema);
          const req = {
            body: {
              firstname: "Mark",
              lastname: "Thomsit",
              buddies: [
                {
                  name: "Yassire"
                }
              ]
            }
          };
          const res = {
            jerror: jest.fn()
          };
          const next = jest.fn();
          middleware(req, res, next);
          expect(req.body).toHaveProperty("firstname");
          expect(req.body).toHaveProperty("lastname");
          expect(req.body).toHaveProperty("buddies");

          done();
        });
      });
      describe("when additional fields exist", () => {
        it("should call next", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              buddies: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              }
            },
            required: ["firstname", "lastname", "buddies"]
          };
          const middleware = trim(schema);
          const req = {
            body: {
              firstname: "Mark",
              lastname: "Thomsit",
              buddies: [
                {
                  name: "Yassire"
                }
              ],
              field1: "test"
            }
          };
          const res = {
            jerror: jest.fn()
          };
          const next = jest.fn();
          middleware(req, res, next);
          expect(res.jerror.mock.calls.length).toEqual(0);
          expect(next.mock.calls.length).toEqual(1);
          expect(req.body).not.toHaveProperty("field1");

          done();
        });
        it("should remove additional fields", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              buddies: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              }
            },
            required: ["firstname", "lastname", "buddies"]
          };
          const middleware = trim(schema);
          const req = {
            body: {
              firstname: "Mark",
              lastname: "Thomsit",
              buddies: [
                {
                  name: "Yassire"
                }
              ],
              field1: "test"
            }
          };
          const res = {
            jerror: jest.fn()
          };
          const next = jest.fn();
          middleware(req, res, next);
          expect(req.body).not.toHaveProperty("field1");

          done();
        });
        it("should keep valid fields", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              buddies: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    name: {
                      type: "string",
                      index: true
                    }
                  }
                }
              }
            },
            required: ["firstname", "lastname", "buddies"]
          };
          const middleware = trim(schema);
          const req = {
            body: {
              firstname: "Mark",
              lastname: "Thomsit",
              buddies: [
                {
                  name: "Yassire"
                }
              ],
              field1: "test"
            }
          };
          const res = {
            jerror: jest.fn()
          };
          const next = jest.fn();
          middleware(req, res, next);
          expect(req.body).toHaveProperty("firstname");
          expect(req.body).toHaveProperty("lastname");
          expect(req.body).toHaveProperty("buddies");

          done();
        });
      });
    });
  });
});
