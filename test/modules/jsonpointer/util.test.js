import util from "../../../src/modules/jsonpointer/util";

describe("jsonpointer/util", () => {
  describe("#getPointers", () => {
    it("should return the original path where no wildcards exist", done => {
      const pointers = util.getPointers("/type/subType", {
        type: { subType: "Sub Type" }
      });
      expect(pointers.length).toEqual(1);
      expect(pointers.shift()).toEqual("/type/subType");

      done();
    });
    it("should return multiple paths for wildcards", done => {
      const pointers = util.getPointers("/people//firstname", {
        people: [
          {
            firstname: "Mark",
            lastname: "Thomsit"
          },
          {
            firstname: "Adam",
            lastname: "Thomsit"
          }
        ]
      });

      expect(pointers.length).toEqual(2);

      expect(pointers[0]).toEqual("/people/0/firstname");
      expect(pointers[1]).toEqual("/people/1/firstname");
      done();
    });
    it("should return multiple paths for nested wildcards", done => {
      const pointers = util.getPointers("/people//likes//name", {
        people: [
          {
            firstname: "Mark",
            lastname: "Thomsit",
            likes: [
              {
                name: "Football"
              },
              {
                name: "Running"
              }
            ]
          },
          {
            firstname: "Adam",
            lastname: "Thomsit",
            likes: [
              {
                name: "Sicily"
              },
              {
                name: "Cornwall"
              }
            ]
          }
        ]
      });
      expect(pointers.length).toEqual(4);

      expect(pointers[0]).toEqual("/people/0/likes/0/name");
      expect(pointers[1]).toEqual("/people/0/likes/1/name");
      expect(pointers[2]).toEqual("/people/1/likes/0/name");
      expect(pointers[3]).toEqual("/people/1/likes/1/name");

      done();
    });
    it("should return multiple paths for nested wildcards without objects", done => {
      const pointers = util.getPointers("/people//likes//", {
        people: [
          {
            firstname: "Mark",
            lastname: "Thomsit",
            likes: ["Football", "Running"]
          },
          {
            firstname: "Adam",
            lastname: "Thomsit",
            likes: ["Sicily", "Cornwall"]
          }
        ]
      });
      expect(pointers.length).toEqual(4);

      expect(pointers[0]).toEqual("/people/0/likes/0");
      expect(pointers[1]).toEqual("/people/0/likes/1");
      expect(pointers[2]).toEqual("/people/1/likes/0");
      expect(pointers[3]).toEqual("/people/1/likes/1");

      done();
    });
    it("should return 0 index when no array exists for wildcards", done => {
      const pointers = util.getPointers("/people//firstname", {});

      expect(pointers.length).toEqual(1);

      expect(pointers[0]).toEqual("/people/0/firstname");

      done();
    });

    it("should return 0 index when no array exists for nested wildcards", done => {
      const pointers = util.getPointers("/people//likes//name", {});

      expect(pointers.length).toEqual(1);

      expect(pointers[0]).toEqual("/people/0/likes/0/name");

      done();
    });
  });
});
