import Ajv from "ajv";
import WhenKeyword from "../../../../src/modules/ajv/keywords/when-keyword";

describe("ajv/WhenKeyword", () => {
  describe("when required paths are empty", () => {
    it("should be valid", done => {
      const ajv = new Ajv({ allErrors: true, $data: true });
      WhenKeyword(ajv);

      const schema = {
        $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
        type: "object",
        $schema: "http://json-schema.org/draft-07/schema#",
        properties: {
          status: {
            type: "string",
            enum: ["Draft", "Submitted"]
          },
          type: {
            type: "object",
            propeties: {
              type: {
                type: "string"
              },
              subType: {
                type: "string"
              }
            }
          }
        },
        when: [
          {
            case: {
              properties: {
                status: { enum: ["Draft"] }
              }
            },
            deepRequired: []
          },
          {
            case: {
              properties: {
                status: { enum: ["Submitted"] }
              }
            },
            deepRequired: ["/type/type", "/type/subType"]
          }
        ]
      };
      const data = {
        status: "Draft",
        type: {}
      };
      const validate = ajv.compile(schema);
      const valid = validate(data);

      if (!valid) {
        throw new Error("Shouldn't produce errors");
      } else {
        done();
      }
    });
  });
  describe("when using simple requred paths", () => {
    describe("when paths exist", () => {
      it("should be valid", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            type: {
              type: "object",
              propeties: {
                type: {
                  type: "string"
                },
                subType: {
                  type: "string"
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Draft"] }
                }
              },
              deepRequired: []
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: ["/type/type", "/type/subType"]
            }
          ]
        };
        const data = {
          status: "Submitted",
          type: {
            type: "Set",
            subType: "Set"
          }
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (!valid) {
          throw new Error("Should not produce errors");
        } else {
          done();
        }
      });
      it("should ignore invalid cases", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            type: {
              type: "object",
              propeties: {
                type: {
                  type: "string"
                },
                subType: {
                  type: "string"
                },
                category: {
                  type: "string",
                  enum: ["One", "Two", "Three"]
                }
              }
            },
            another: {
              type: "object",
              properties: {
                something: {
                  type: "string"
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Draft"] }
                }
              },
              deepRequired: []
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] },
                  type: {
                    properties: {
                      category: {
                        enum: ["Two"]
                      }
                    }
                  }
                },
                required: ["status", "type"]
              },
              deepRequired: ["/another/something"]
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: []
            }
          ]
        };

        const data = {
          status: "Submitted"
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (valid) {
          done();
        } else {
          throw new Error("Should only match Submitted");
        }
      });
    });
    describe("when paths are missing", () => {
      let valid = null;
      let errors = null;

      beforeEach(done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            type: {
              type: "object",
              propeties: {
                type: {
                  type: "string"
                },
                subType: {
                  type: "string"
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Draft"] }
                }
              },
              deepRequired: []
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: ["/type/type", "/type/subType"]
            }
          ]
        };
        const data = {
          status: "Submitted",
          type: {}
        };
        const validate = ajv.compile(schema);
        valid = validate(data);
        errors = validate.errors;

        done();
      });
      it("should be invalid", done => {
        if (!valid) {
          done();
        } else {
          throw new Error("Should produce errors");
        }
      });
      it("should return params", done => {
        errors.forEach(error => {
          expect(error.params).toBeTruthy();
          expect(error.params).toHaveProperty("case");
          expect(error.params).toHaveProperty("required");
        });
        done();
      });
    });
  });
  describe("when using wildcard requred paths", () => {
    describe("when paths exist", () => {
      it("should be valid", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            people: {
              type: "array",
              items: {
                properties: {
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Draft"] }
                }
              },
              deepRequired: []
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: ["/people//firstname"]
            }
          ]
        };
        const data = {
          status: "Submitted",
          people: [
            {
              firstname: "Mark",
              lastname: "Thomsit"
            },
            {
              firstname: "Adam",
              lastname: "Thomsit"
            }
          ]
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (!valid) {
          throw new Error("Should not produce errors");
        } else {
          done();
        }
      });
      it("should match valid cases", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            people: {
              type: "array",
              items: {
                properties: {
                  type: {
                    enum: ["Staff", "Patient"],
                    type: "string"
                  },
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] },
                  people: {
                    items: {
                      properties: {
                        type: {
                          enum: ["Staff"]
                        }
                      }
                    }
                  }
                }
              },
              deepRequired: ["/people//firstname", "/people//lastname"]
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: []
            }
          ]
        };

        const data = {
          status: "Submitted",
          people: [{ type: "Staff" }]
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (valid) {
          throw new Error("Should only match Submitted");
        } else {
          done();
        }
      });
      it("should not match invalid cases", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            people: {
              type: "array",
              items: {
                kind: {
                  enum: ["Staff", "Patient"],
                  type: "string"
                },
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] },
                  people: {
                    items: {
                      properties: {
                        kind: {
                          enum: ["Patient"]
                        }
                      }
                    }
                  }
                }
              },
              deepRequired: ["/people//firstname", "/people//lastname"]
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: []
            }
          ]
        };

        const data = {
          status: "Submitted",
          people: [{ kind: "Staff" }]
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (valid) {
          done();
        } else {
          throw new Error("Should not match people.kind");
        }
      });
    });
    describe("when paths are missing", () => {
      it("should be invalid when for array attributes when some are not set", done => {
        const ajv = new Ajv({ allErrors: true, $data: true });
        WhenKeyword(ajv);

        const schema = {
          $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
          type: "object",
          $schema: "http://json-schema.org/draft-07/schema#",
          properties: {
            status: {
              type: "string",
              enum: ["Draft", "Submitted"]
            },
            people: {
              type: "array",
              items: {
                properties: {
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            }
          },
          when: [
            {
              case: {
                properties: {
                  status: { enum: ["Draft"] }
                }
              },
              deepRequired: []
            },
            {
              case: {
                properties: {
                  status: { enum: ["Submitted"] }
                }
              },
              deepRequired: ["/people//firstname"]
            }
          ]
        };
        const data = {
          status: "Submitted",
          people: [
            {
              lastname: "Thomsit"
            },
            {
              firstname: "Adam",
              lastname: "Thomsit"
            }
          ]
        };
        const validate = ajv.compile(schema);
        const valid = validate(data);

        if (!valid) {
          const errors = validate.errors;
          expect(errors.length).toEqual(1);

          const error = errors.shift();
          expect(error.message).toEqual("should have required property 'firstname'");
          expect(error.dataPath).toEqual("people[0].firstname");

          done();
        } else {
          throw new Error("Should not produce errors");
        }
      });
    });
  });
  describe("when using json pointers", () => {
    it("should return paths with jsonPointers", done => {
      const ajv = new Ajv({ allErrors: true, $data: true, jsonPointers: true });
      WhenKeyword(ajv);

      const schema = {
        $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
        type: "object",
        $schema: "http://json-schema.org/draft-07/schema#",
        properties: {
          status: {
            type: "string",
            enum: ["Draft", "Submitted"]
          },
          people: {
            type: "array",
            items: {
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                }
              }
            }
          }
        },
        when: [
          {
            case: {
              properties: {
                status: { enum: ["Draft"] }
              }
            },
            deepRequired: []
          },
          {
            case: {
              properties: {
                status: { enum: ["Submitted"] }
              }
            },
            deepRequired: ["/people//firstname"]
          }
        ]
      };
      const data = {
        status: "Submitted",
        people: [
          {
            lastname: "Thomsit"
          },
          {
            firstname: "Adam",
            lastname: "Thomsit"
          }
        ]
      };
      const validate = ajv.compile(schema);
      const valid = validate(data);

      if (!valid) {
        const errors = validate.errors;
        expect(errors.length).toEqual(1);

        const error = errors.shift();
        expect(error.message).toEqual("should have required property 'firstname'");
        expect(error.dataPath).toEqual("/people/0/firstname");

        done();
      } else {
        throw new Error("Should not produce errors");
      }
    });
  });
});
