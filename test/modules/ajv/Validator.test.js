import Validator from "../../../src/modules/ajv/Validator";

describe("Validator", () => {
  describe("#validate", () => {
    describe("when schema has no nesting", () => {
      describe("when properties are missing", () => {
        let errors = null;
        beforeEach(done => {
          const schema = {
            $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
            type: "object",
            $schema: "http://json-schema.org/draft-07/schema#",
            properties: {
              status: {
                type: "string",
                enum: ["Unconfirmed", "Confirmed"]
              },
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              },
              username: {
                type: "string"
              }
            },
            required: ["status", "firstname", "lastname", "username"]
          };
          const validator = new Validator(schema);
          errors = validator.validate({});

          done();
        });
        it("should include paths", done => {
          expect(Object.keys(errors).length).toEqual(4);

          done();
        });
        it("should include full paths", done => {
          ["status", "firstname", "lastname", "username"].forEach(attribute => {
            expect(Object.keys(errors).includes(attribute)).toEqual(true);
          });
          done();
        });
        it("should set type", done => {
          ["status", "firstname", "lastname", "username"].forEach(path => {
            expect(errors[path].type).toEqual("required");
          });
          done();
        });
      });
      describe("when properties are invalid", () => {
        describe("with invalid format", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                username: {
                  type: "string",
                  format: "email"
                }
              },
              required: ["status", "firstname", "lastname", "username"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ username: "john" });

            done();
          });
          it("should include paths", done => {
            expect(Object.keys(errors).length).toEqual(4);

            done();
          });
          it("should include full paths", done => {
            ["status", "firstname", "lastname"].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            ["status", "firstname", "lastname"].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            ["username"].forEach(path => {
              expect(errors[path].type).toEqual("format");
            });
            done();
          });
        });
        describe("with invalid enum", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                username: {
                  type: "string",
                  format: "email"
                }
              },
              required: ["status", "firstname", "lastname", "username"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ status: "Submitted" });

            done();
          });
          it("should include paths", done => {
            expect(Object.keys(errors).length).toEqual(4);

            done();
          });
          it("should include full paths", done => {
            ["status", "firstname", "lastname", "username"].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            ["firstname", "lastname", "username"].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            ["status"].forEach(path => {
              expect(errors[path].type).toEqual("enum");
            });
            done();
          });
        });
        describe("with a value exceeding maximum", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                username: {
                  type: "string",
                  format: "email"
                },
                age: {
                  type: "integer",
                  maximum: 199
                }
              },
              required: ["status", "firstname", "lastname", "username", "age"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ age: 250 });

            done();
          });
          it("should include paths", done => {
            expect(Object.keys(errors).length).toEqual(5);

            done();
          });
          it("should include full paths", done => {
            ["status", "firstname", "lastname", "username", "age"].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            ["status", "firstname", "lastname", "username"].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            ["age"].forEach(path => {
              expect(errors[path].type).toEqual("maximum");
            });
            done();
          });
        });
        describe("with a value below a minimum", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                },
                username: {
                  type: "string",
                  format: "email"
                },
                age: {
                  type: "integer",
                  minimum: 0
                }
              },
              required: ["status", "firstname", "lastname", "username", "age"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ age: -1 });

            done();
          });
          it("should include paths", done => {
            expect(Object.keys(errors).length).toEqual(5);

            done();
          });
          it("should include full paths", done => {
            ["status", "firstname", "lastname", "username", "age"].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            ["status", "firstname", "lastname", "username"].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            ["age"].forEach(path => {
              expect(errors[path].type).toEqual("minimum");
            });
            done();
          });
        });
      });
    });
    describe("when schema has nesting", () => {
      describe("when a single-level deep", () => {
        describe("when properties are missing", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                profile: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    }
                  },
                  required: ["firstname", "lastname"]
                },
                username: {
                  type: "string"
                }
              },
              required: ["status", "username", "profile"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ profile: {} });
            done();
          });
          it("should return all paths", done => {
            expect(Object.keys(errors).length).toEqual(4);

            done();
          });
          it("should return full paths", done => {
            ["status", "profile.firstname", "profile.lastname", "username"].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            ["status", "profile.firstname", "profile.lastname", "username"].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            done();
          });
        });
        describe("when properties are invalid", () => {
          describe("with invalid format", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      email: {
                        type: "string",
                        format: "email"
                      }
                    },
                    required: ["firstname", "lastname", "email"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({ profile: { email: "john" } });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(5);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.email",
                "username"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              ["status", "profile.firstname", "profile.lastname", "username"].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.email"].forEach(path => {
                expect(errors[path].type).toEqual("format");
              });
              done();
            });
          });
          describe("with invalid enum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      title: {
                        type: "string",
                        enum: ["Mr", "Mrs", "Miss"]
                      }
                    },
                    required: ["firstname", "lastname", "title"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({ profile: { title: "Lord" } });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(5);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.title",
                "username"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              ["status", "profile.firstname", "profile.lastname", "username"].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.title"].forEach(path => {
                expect(errors[path].type).toEqual("enum");
              });
              done();
            });
          });
          describe("with a value exceeding maximum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      age: {
                        type: "integer",
                        maximum: 199
                      }
                    },
                    required: ["firstname", "lastname", "age"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({ profile: { age: 250 } });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(5);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "username",
                "profile.firstname",
                "profile.lastname",
                "profile.age"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              ["status", "profile.firstname", "profile.lastname", "username"].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.age"].forEach(path => {
                expect(errors[path].type).toEqual("maximum");
              });
              done();
            });
          });
          describe("with a value below a minimum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      age: {
                        type: "integer",
                        minimum: 0
                      }
                    },
                    required: ["firstname", "lastname", "age"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({ profile: { age: -1 } });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(5);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "username",
                "profile.firstname",
                "profile.lastname",
                "profile.age"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              ["status", "profile.firstname", "profile.lastname", "username"].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.age"].forEach(path => {
                expect(errors[path].type).toEqual("minimum");
              });
              done();
            });
          });
        });
      });
      describe("when multiple levels deep", () => {
        describe("when properties are missing", () => {
          let errors = null;
          beforeEach(done => {
            const schema = {
              $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
              type: "object",
              $schema: "http://json-schema.org/draft-07/schema#",
              properties: {
                status: {
                  type: "string",
                  enum: ["Unconfirmed", "Confirmed"]
                },
                profile: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    },
                    notifications: {
                      type: "object",
                      properties: {
                        reminders: {
                          type: "boolean"
                        },
                        messages: {
                          type: "boolean"
                        }
                      },
                      required: ["reminders", "messages"]
                    }
                  },
                  required: ["firstname", "lastname", "notifications"]
                },
                username: {
                  type: "string"
                }
              },
              required: ["status", "username", "profile"]
            };
            const validator = new Validator(schema);
            errors = validator.validate({ profile: { notifications: {} } });

            done();
          });
          it("should return all paths", done => {
            expect(Object.keys(errors).length).toEqual(6);

            done();
          });
          it("should return full paths", done => {
            [
              "status",
              "profile.firstname",
              "profile.lastname",
              "profile.lastname",
              "profile.notifications.reminders",
              "profile.notifications.messages",
              "username"
            ].forEach(attribute => {
              expect(Object.keys(errors).includes(attribute)).toEqual(true);
            });
            done();
          });
          it("should set type", done => {
            [
              "status",
              "profile.firstname",
              "profile.lastname",
              "profile.lastname",
              "profile.notifications.reminders",
              "profile.notifications.messages",
              "username"
            ].forEach(path => {
              expect(errors[path].type).toEqual("required");
            });
            done();
          });
        });
        describe("when properties are invalid", () => {
          describe("with invalid format", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      settings: {
                        type: "object",
                        properties: {
                          specialty: {
                            type: "string"
                          },
                          yearsWorked: {
                            type: "number"
                          },
                          delegate: {
                            type: "string",
                            format: "email"
                          }
                        },
                        required: ["specialty", "yearsWorked", "delegate"]
                      }
                    },
                    required: ["firstname", "lastname", "settings"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({
                profile: { settings: { delegate: "john" } }
              });
              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(7);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.specialty",
                "profile.settings.yearsWorked",
                "profile.settings.delegate",
                "username"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.specialty",
                "profile.settings.yearsWorked",
                "username"
              ].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.settings.delegate"].forEach(path => {
                expect(errors[path].type).toEqual("format");
              });
              done();
            });
          });
          describe("with invalid enum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      settings: {
                        type: "object",
                        properties: {
                          specialty: {
                            type: "string",
                            enum: ["Paediatrics"]
                          },
                          yearsWorked: {
                            type: "number"
                          },
                          delegate: {
                            type: "string",
                            format: "email"
                          }
                        },
                        required: ["specialty", "yearsWorked", "delegate"]
                      }
                    },
                    required: ["firstname", "lastname", "settings"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({
                profile: { settings: { specialty: "Plumbing" } }
              });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(7);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.specialty",
                "profile.settings.yearsWorked",
                "profile.settings.delegate",
                "username"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.delegate",
                "profile.settings.yearsWorked",
                "username"
              ].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.settings.specialty"].forEach(path => {
                expect(errors[path].type).toEqual("enum");
              });
              done();
            });
          });
          describe("with a value exceeding maximum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      settings: {
                        type: "object",
                        properties: {
                          specialty: {
                            type: "string",
                            enum: ["Paediatrics"]
                          },
                          yearsWorked: {
                            type: "number",
                            maximum: 20
                          },
                          delegate: {
                            type: "string",
                            format: "email"
                          }
                        },
                        required: ["specialty", "yearsWorked", "delegate"]
                      }
                    },
                    required: ["firstname", "lastname", "settings"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({
                profile: { settings: { yearsWorked: 30 } }
              });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(7);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "username",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.specialty",
                "profile.settings.yearsWorked",
                "profile.settings.delegate"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.delegate",
                "profile.settings.specialty",
                "username"
              ].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.settings.yearsWorked"].forEach(path => {
                expect(errors[path].type).toEqual("maximum");
              });
              done();
            });
          });
          describe("with a value below a minimum", () => {
            let errors = null;
            beforeEach(done => {
              const schema = {
                $id: "https://api.carereport-dev.makeandship.com/schemas/incident.json",
                type: "object",
                $schema: "http://json-schema.org/draft-07/schema#",
                properties: {
                  status: {
                    type: "string",
                    enum: ["Unconfirmed", "Confirmed"]
                  },
                  username: {
                    type: "string",
                    format: "email"
                  },
                  profile: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      },
                      settings: {
                        type: "object",
                        properties: {
                          specialty: {
                            type: "string",
                            enum: ["Paediatrics"]
                          },
                          yearsWorked: {
                            type: "number",
                            minimum: 0
                          },
                          delegate: {
                            type: "string",
                            format: "email"
                          }
                        },
                        required: ["specialty", "yearsWorked", "delegate"]
                      }
                    },
                    required: ["firstname", "lastname", "settings"]
                  }
                },
                required: ["status", "profile", "username"]
              };
              const validator = new Validator(schema);
              errors = validator.validate({
                profile: { settings: { yearsWorked: -1 } }
              });

              done();
            });
            it("should include paths", done => {
              expect(Object.keys(errors).length).toEqual(7);

              done();
            });
            it("should include full paths", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.specialty",
                "profile.settings.yearsWorked",
                "profile.settings.delegate",
                "username"
              ].forEach(attribute => {
                expect(Object.keys(errors).includes(attribute)).toEqual(true);
              });
              done();
            });
            it("should set type", done => {
              [
                "status",
                "profile.firstname",
                "profile.lastname",
                "profile.settings.delegate",
                "profile.settings.specialty",
                "username"
              ].forEach(path => {
                expect(errors[path].type).toEqual("required");
              });
              ["profile.settings.yearsWorked"].forEach(path => {
                expect(errors[path].type).toEqual("minimum");
              });
              done();
            });
          });
        });
      });
    });
  });
});
