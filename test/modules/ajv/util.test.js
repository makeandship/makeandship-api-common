import { formatErrors } from "../../../src/modules/ajv/util";

describe("modules/ajv/util", () => {
  describe("#when errors array is empty", () => {
    it("should return empty errors json", () => {
      const formattedMessage = formatErrors([]);
      expect(formattedMessage).toBeTruthy();
      expect(formattedMessage).toHaveProperty("errors");
      expect(formattedMessage.errors).toBeTruthy();
    });
  });
  describe("#when errors array contain one error", () => {
    it("should return valid errors json", () => {
      const formattedMessage = formatErrors([
        { dataPath: "email", message: "invalid email format" }
      ]);
      expect(formattedMessage).toBeTruthy();
      expect(formattedMessage).toHaveProperty("errors");
      expect(formattedMessage.errors).toBeTruthy();
      expect(formattedMessage.errors.email.path).toEqual("email");
      expect(formattedMessage.errors.email.message).toEqual("invalid email format");
    });
  });
  describe("#when errors array contain multiple errors", () => {
    it("should return valid errors json", () => {
      const formattedMessage = formatErrors([
        { dataPath: "email", message: "invalid email format" },
        { dataPath: "firstname", message: "firstname is required" }
      ]);
      expect(formattedMessage).toBeTruthy();
      expect(formattedMessage).toHaveProperty("errors");
      expect(formattedMessage.errors).toBeTruthy();
      expect(formattedMessage.errors.email.path).toEqual("email");
      expect(formattedMessage.errors.email.message).toEqual("invalid email format");
      expect(formattedMessage.errors.firstname.path).toEqual("firstname");
      expect(formattedMessage.errors.firstname.message).toEqual("firstname is required");
    });
  });
  describe("#when errors array contain dataPath with dot", () => {
    it("should return valid errors json", () => {
      const formattedMessage = formatErrors([
        { dataPath: ".user.email", message: "invalid email format" }
      ]);
      expect(formattedMessage).toBeTruthy();
      expect(formattedMessage).toHaveProperty("errors");
      expect(formattedMessage.errors).toBeTruthy();
      expect(formattedMessage.errors["user.email"].path).toEqual("user.email");
      expect(formattedMessage.errors["user.email"].message).toEqual("invalid email format");
    });
  });
});
