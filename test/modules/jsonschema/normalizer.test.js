import normalizer from "../../../src/modules/jsonschema/normalizer";

describe("normalizer", () => {
  describe("#normalize", () => {
    describe("when flat attributes", () => {
      it("should call next", done => {
        const schema = {
          type: "object",
          properties: {
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            buddy: {
              type: "object",
              properties: {
                name: {
                  type: "string",
                  index: true
                }
              }
            }
          },
          required: ["firstname", "lastname", "buddy"]
        };
        const body = {
          firstname: "Mark",
          lastname: "Thomsit",
          "buddy.name": "Yassire"
        };
        const normalized = normalizer.normalize(schema, body);
        expect(normalized).toHaveProperty("buddy");
        expect(normalized.buddy).toHaveProperty("name", "Yassire");

        done();
      });
    });
    describe("when array attributes", () => {
      it("should call next", done => {
        const schema = {
          type: "object",
          properties: {
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            buddies: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: {
                    type: "string",
                    index: true
                  }
                }
              }
            }
          },
          required: ["firstname", "lastname", "buddies"]
        };
        const body = {
          firstname: "Mark",
          lastname: "Thomsit",
          "buddies[0].name": "Yassire"
        };
        const normalized = normalizer.normalize(schema, body);
        expect(normalized).toHaveProperty("buddies");
        expect(normalized.buddies.length).toEqual(1);
        expect(normalized.buddies[0]).toHaveProperty("name", "Yassire");

        done();
      });
    });
    describe("when formatting boolean on attributes", () => {
      it("should call next", done => {
        const schema = {
          type: "object",
          properties: {
            firstname: {
              type: "string"
            },
            lastname: {
              type: "string"
            },
            buddies: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: {
                    type: "string",
                    index: true
                  }
                }
              }
            },
            retired: {
              type: "boolean"
            }
          },
          required: ["firstname", "lastname", "buddies", "retired"]
        };
        const body = {
          firstname: "Mark",
          lastname: "Thomsit",
          "buddies[0].name": "Yassire",
          retired: "true"
        };

        const normalized = normalizer.normalize(schema, body);
        expect(normalized).toHaveProperty("retired");
        expect(typeof normalized.retired).toEqual("boolean");
        expect(normalized.retired).toEqual(true);

        done();
      });
    });
  });
});
