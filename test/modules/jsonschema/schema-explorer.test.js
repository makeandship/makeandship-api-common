import fs from "fs";
import path from "path";
import SchemaExplorer from "../../../src/modules/jsonschema/schema-explorer";

describe("SchemaExplorer", () => {
  describe("when the schema does not have dependencies", () => {
    const schema = {
      type: "object",
      properties: {
        user: {
          type: "object",
          title: "User",
          properties: {
            organisation: {
              $ref: "#/definitions/organisation"
            },
            firstname: {
              title: "First Name",
              type: "string"
            },
            lastname: {
              title: "Last Name",
              type: "string"
            },
            email: {
              title: "Email",
              type: "string",
              format: "email"
            },
            phoneNumber: {
              type: "string"
            },
            age: {
              type: "number"
            },
            bio: {
              type: "text"
            },
            role: {
              type: "string",
              enum: ["administrator", "subscriber", "editor"]
            },
            startDate: {
              type: "string",
              format: "date"
            },
            endDate: {
              title: "End Date",
              type: "string",
              format: "date"
            },
            bmi: {
              type: "number"
            },
            score: {
              type: "number"
            }
          }
        },
        feed: {
          type: "object"
        },
        buddy: {
          type: "object",
          properties: {
            name: {
              type: "string"
            },
            birthDate: {
              type: "string",
              format: "date"
            },
            dateOfDeath: {
              type: "string",
              format: "date"
            }
          }
        },
        custodian: {
          type: "object",
          title: "Custodian",
          properties: {
            name: {
              type: "string"
            },
            emailAddress: {
              type: "string"
            },
            birthDate: {
              title: "Custodian Birthdate",
              type: "string",
              format: "date-time"
            },
            dateOfDeath: {
              type: "string",
              format: "date-time"
            }
          }
        }
      },
      definitions: {
        organisation: {
          type: "object",
          title: "Organisation",
          properties: {
            id: {
              type: "string"
            },
            name: {
              title: "Organisation Name",
              type: "string"
            }
          }
        }
      }
    };
    describe("#getTitle", () => {
      describe("when the property exists", () => {
        it("should return the title", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.name": "Organisation Name",
            "user.firstname": "First Name",
            "user.lastname": "Last Name",
            "user.email": "Email",
            "user.endDate": "End Date",
            "custodian.birthDate": "Custodian Birthdate"
          };
          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute];

            expect(explorer.getTitle(attribute)).toEqual(title);
          });

          done();
        });
      });

      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getTitle("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getTitles", () => {
      describe("when the property exists", () => {
        it("should return titles", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.name": ["User", "Organisation", "Organisation Name"],
            "user.firstname": ["User", "First Name"],
            "user.lastname": ["User", "Last Name"],
            "user.email": ["User", "Email"],
            "user.endDate": ["User", "End Date"],
            "custodian.birthDate": ["Custodian", "Custodian Birthdate"]
          };
          Object.keys(verify).forEach(attribute => {
            const titles = verify[attribute];

            expect(explorer.getTitles(attribute)).toEqual(titles);
          });

          done();
        });
      });
      describe("when the property does not exist", () => {
        it("should return null  ", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getTitles("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getType", () => {
      describe("when the property exists", () => {
        it("should return type", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.name": "string",
            "user.firstname": "string",
            "user.lastname": "string",
            "user.email": "string",
            "user.endDate": "string",
            "custodian.birthDate": "string"
          };
          Object.keys(verify).forEach(attribute => {
            const type = verify[attribute];

            expect(explorer.getType(attribute)).toEqual(type);
          });

          done();
        });
      });
      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getType("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getFormat", () => {
      describe("when the property exists", () => {
        describe("when the format attribute exists", () => {
          it("should return format", done => {
            const explorer = new SchemaExplorer(schema);

            const withFormat = {
              "user.email": "email",
              "user.endDate": "date",
              "custodian.birthDate": "date-time"
            };

            Object.keys(withFormat).forEach(attribute => {
              const format = withFormat[attribute];

              expect(explorer.getFormat(attribute)).toEqual(format);
            });

            done();
          });
        });
        describe("when the format attribute does not exist", () => {
          it("should return null", done => {
            const explorer = new SchemaExplorer(schema);

            const withoutFormat = ["user.organisation.name", "user.firstname", "user.lastname"];

            withoutFormat.forEach(attribute => {
              expect(explorer.getFormat(attribute)).toBeNull();
            });

            done();
          });
        });
      });
      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getFormat("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getMapBy", () => {
      describe("when mapping title", () => {
        it("should return a map of path to title", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy("title");

          const verify = {
            "user.organisation.name": "Organisation Name",
            "user.firstname": "First Name",
            "user.lastname": "Last Name",
            "user.email": "Email",
            "user.endDate": "End Date",
            "custodian.birthDate": "Custodian Birthdate"
          };

          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute];

            expect(map[attribute]).toEqual(title);
          });

          done();
        });
      });
      describe("when mapping titles", () => {
        it("should return a map of path to title", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy("titles");

          const verify = {
            "user.organisation.name": ["User", "Organisation", "Organisation Name"],
            "user.firstname": ["User", "First Name"],
            "user.lastname": ["User", "Last Name"],
            "user.email": ["User", "Email"],
            "user.endDate": ["User", "End Date"],
            "custodian.birthDate": ["Custodian", "Custodian Birthdate"]
          };

          Object.keys(verify).forEach(attribute => {
            const titles = verify[attribute];

            expect(map[attribute]).toEqual(titles);
          });

          done();
        });
      });
      describe("when mapping multiple attributes", () => {
        it("should return a map of path to object", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy(["title", "titles"]);

          const verify = {
            "user.organisation.name": {
              title: "Organisation Name",
              titles: ["User", "Organisation", "Organisation Name"]
            },
            "user.firstname": {
              title: "First Name",
              titles: ["User", "First Name"]
            },
            "user.lastname": {
              title: "Last Name",
              titles: ["User", "Last Name"]
            },
            "user.email": {
              title: "Email",
              titles: ["User", "Email"]
            },
            "user.endDate": {
              title: "End Date",
              titles: ["User", "End Date"]
            },
            "custodian.birthDate": {
              title: "Custodian Birthdate",
              titles: ["Custodian", "Custodian Birthdate"]
            }
          };

          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute].title;
            const titles = verify[attribute].titles;

            expect(map[attribute]).toHaveProperty("title", title);
            expect(map[attribute]).toHaveProperty("titles", titles);
          });

          done();
        });
      });
    });
    describe("#getAttributes", () => {
      describe("when schema contains primitives", () => {
        it("should return paths for primitive definitions", done => {
          const schema = {
            type: "object",
            properties: {
              firstname: {
                type: "string"
              },
              lastname: {
                type: "string"
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          const paths = explorer.getAttributes();

          expect(paths.includes("firstname")).toEqual(true);
          expect(paths.includes("lastname")).toEqual(true);

          done();
        });
      });
      describe("when schema contains objects", () => {
        let paths = null;
        beforeEach(done => {
          const schema = {
            type: "object",
            properties: {
              user: {
                type: "object",
                properties: {
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          paths = explorer.getAttributes();

          done();
        });
        it("should return paths for nested definitions", done => {
          expect(paths.includes("user.firstname")).toEqual(true);
          expect(paths.includes("user.lastname")).toEqual(true);

          done();
        });
        it("should return paths for parent definitions", done => {
          expect(paths.includes("user")).toEqual(true);

          done();
        });
      });
      describe("when schema contains objects using references", () => {
        let paths = null;
        beforeEach(done => {
          const schema = {
            definitions: {
              user: {
                type: "object",
                properties: {
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            },
            type: "object",
            properties: {
              user: {
                $ref: "#/definitions/user"
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          paths = explorer.getAttributes();

          done();
        });
        it("should return paths for nested definitions", done => {
          expect(paths.includes("user.firstname")).toEqual(true);
          expect(paths.includes("user.lastname")).toEqual(true);

          done();
        });
      });
      describe("when schema contains arrays", () => {
        let paths = null;
        beforeEach(done => {
          const schema = {
            type: "object",
            properties: {
              users: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    }
                  }
                }
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          paths = explorer.getAttributes();

          done();
        });
        it("should return paths for nested definitions", done => {
          expect(paths.includes("users.firstname")).toEqual(true);
          expect(paths.includes("users.lastname")).toEqual(true);

          done();
        });
        it("should return paths for parent definitions", done => {
          expect(paths.includes("users")).toEqual(true);

          done();
        });
      });
      describe("when schema contains arrays using references", () => {
        let paths = null;
        beforeEach(done => {
          const schema = {
            definitions: {
              user: {
                type: "object",
                properties: {
                  firstname: {
                    type: "string"
                  },
                  lastname: {
                    type: "string"
                  }
                }
              }
            },
            type: "object",
            properties: {
              users: {
                type: "array",
                items: {
                  $ref: "#/definitions/user"
                }
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          paths = explorer.getAttributes();

          done();
        });
        it("should return paths for nested definitions", done => {
          expect(paths.includes("users.firstname")).toEqual(true);
          expect(paths.includes("users.lastname")).toEqual(true);

          done();
        });
        it("should return paths for parent definitions", done => {
          expect(paths.includes("users")).toEqual(true);

          done();
        });
      });
      describe("when schema contains arrays using oneOf", () => {
        let paths = null;
        beforeEach(done => {
          const schema = {
            definitions: {
              user: {
                oneOf: [
                  {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      }
                    }
                  },
                  {
                    type: "object",
                    properties: {
                      email: {
                        type: "string"
                      }
                    }
                  }
                ]
              }
            },
            type: "object",
            properties: {
              users: {
                type: "array",
                items: {
                  $ref: "#/definitions/user"
                }
              }
            }
          };
          const explorer = new SchemaExplorer(schema);
          paths = explorer.getAttributes();

          done();
        });
        it("should return paths for nested definitions", done => {
          expect(paths.includes("users.firstname")).toEqual(true);
          expect(paths.includes("users.lastname")).toEqual(true);
          expect(paths.includes("users.email")).toEqual(true);

          done();
        });
        it("should return paths for parent definitions", done => {
          expect(paths.includes("users")).toEqual(true);

          done();
        });
      });
    });
    describe("#getAttributeBy", () => {
      describe("when the attribute exists", () => {
        describe("when schema contains primitives", () => {
          it("should return the attribute", done => {
            const schema = {
              type: "object",
              properties: {
                firstname: {
                  type: "string"
                },
                lastname: {
                  type: "string"
                }
              }
            };
            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
        describe("when schema contains objects", () => {
          it("should return the value", done => {
            const schema = {
              type: "object",
              properties: {
                user: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    }
                  }
                }
              }
            };
            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("user.firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
        describe("when schema contains objects using references", () => {
          it("should return paths for nested definitions", done => {
            const schema = {
              definitions: {
                user: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    }
                  }
                }
              },
              type: "object",
              properties: {
                user: {
                  $ref: "#/definitions/user"
                }
              }
            };
            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("user.firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
        describe("when schema contains arrays", () => {
          it("should return a value", done => {
            const schema = {
              type: "object",
              properties: {
                users: {
                  type: "array",
                  items: {
                    type: "object",
                    properties: {
                      firstname: {
                        type: "string"
                      },
                      lastname: {
                        type: "string"
                      }
                    }
                  }
                }
              }
            };
            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("users.firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
        describe("when schema contains arrays using references", () => {
          it("should return the value", done => {
            const schema = {
              definitions: {
                user: {
                  type: "object",
                  properties: {
                    firstname: {
                      type: "string"
                    },
                    lastname: {
                      type: "string"
                    }
                  }
                }
              },
              type: "object",
              properties: {
                users: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/user"
                  }
                }
              }
            };

            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("users.firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
        describe("when schema contains arrays using oneOf", () => {
          it("should return the value", done => {
            const schema = {
              definitions: {
                user: {
                  oneOf: [
                    {
                      type: "object",
                      properties: {
                        firstname: {
                          type: "string"
                        },
                        lastname: {
                          type: "string"
                        }
                      }
                    },
                    {
                      type: "object",
                      properties: {
                        email: {
                          type: "string"
                        }
                      }
                    }
                  ]
                }
              },
              type: "object",
              properties: {
                users: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/user"
                  }
                }
              }
            };
            const explorer = new SchemaExplorer(schema);
            const value = explorer.getAttributeBy("users.firstname", "type");

            expect(value).toEqual("string");

            done();
          });
        });
      });
    });
  });
  describe("when the schema has dependencies", () => {
    const schema = {
      type: "object",
      properties: {
        user: {
          type: "object",
          title: "User",
          properties: {
            organisation: {
              $ref: "#/definitions/organisation"
            },
            firstname: {
              title: "First Name",
              type: "string"
            },
            lastname: {
              title: "Last Name",
              type: "string"
            },
            email: {
              title: "Email",
              type: "string",
              format: "email"
            },
            phoneNumber: {
              type: "string"
            },
            age: {
              type: "number"
            },
            bio: {
              type: "text"
            },
            role: {
              type: "string",
              enum: ["administrator", "subscriber", "editor"]
            },
            startDate: {
              type: "string",
              format: "date"
            },
            endDate: {
              title: "End Date",
              type: "string",
              format: "date"
            },
            bmi: {
              type: "number"
            },
            score: {
              type: "number"
            }
          }
        },
        feed: {
          type: "object"
        },
        buddy: {
          type: "object",
          properties: {
            name: {
              type: "string"
            },
            birthDate: {
              type: "string",
              format: "date"
            },
            dateOfDeath: {
              type: "string",
              format: "date"
            }
          }
        },
        custodian: {
          type: "object",
          title: "Custodian",
          properties: {
            name: {
              type: "string"
            },
            emailAddress: {
              type: "string"
            },
            birthDate: {
              title: "Custodian Birthdate",
              type: "string",
              format: "date-time"
            },
            dateOfDeath: {
              type: "string",
              format: "date-time"
            }
          }
        }
      },
      definitions: {
        organisation: {
          type: "object",
          title: "Organisation",
          properties: {
            id: {
              type: "string"
            },
            name: {
              title: "Organisation Name",
              type: "string"
            },
            type: {
              title: "Organisation Type",
              type: "string",
              enum: ["National", "International"]
            }
          },
          dependencies: {
            type: {
              properties: {
                type: {
                  enum: ["International"]
                },
                headquartersCountry: {
                  title: "Organisation Headquarters Country",
                  type: "string"
                },
                companyWebsite: {
                  title: "Website",
                  type: "string",
                  format: "uri"
                }
              }
            }
          }
        }
      }
    };
    describe("#getTitle", () => {
      describe("when the property exists", () => {
        it("should return the title", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.type": "Organisation Type",
            "user.organisation.headquartersCountry": "Organisation Headquarters Country",
            "user.organisation.companyWebsite": "Website"
          };
          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute];

            expect(explorer.getTitle(attribute)).toEqual(title);
          });

          done();
        });
      });

      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getTitle("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getTitles", () => {
      describe("when the property exists", () => {
        it("should return titles", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.type": ["User", "Organisation", "Organisation Type"],
            "user.organisation.headquartersCountry": [
              "User",
              "Organisation",
              "Organisation Headquarters Country"
            ],
            "user.organisation.companyWebsite": ["User", "Organisation", "Website"]
          };
          Object.keys(verify).forEach(attribute => {
            const titles = verify[attribute];

            expect(explorer.getTitles(attribute)).toEqual(titles);
          });

          done();
        });
      });
      describe("when the property does not exist", () => {
        it("should return null  ", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getTitles("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getType", () => {
      describe("when the property exists", () => {
        it("should return type", done => {
          const explorer = new SchemaExplorer(schema);

          const verify = {
            "user.organisation.type": "string",
            "user.organisation.headquartersCountry": "string",
            "user.organisation.companyWebsite": "string"
          };
          Object.keys(verify).forEach(attribute => {
            const type = verify[attribute];

            expect(explorer.getType(attribute)).toEqual(type);
          });

          done();
        });
      });
      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getType("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getFormat", () => {
      describe("when the property exists", () => {
        describe("when the format attribute exists", () => {
          it("should return format", done => {
            const explorer = new SchemaExplorer(schema);

            const withFormat = {
              "user.organisation.companyWebsite": "uri"
            };

            Object.keys(withFormat).forEach(attribute => {
              const format = withFormat[attribute];

              expect(explorer.getFormat(attribute)).toEqual(format);
            });

            done();
          });
        });
        describe("when the format attribute does not exist", () => {
          it("should return null", done => {
            const explorer = new SchemaExplorer(schema);

            const withoutFormat = [
              "user.organisation.type",
              "user.organisation.headquartersCountry"
            ];

            withoutFormat.forEach(attribute => {
              expect(explorer.getFormat(attribute)).toBeNull();
            });

            done();
          });
        });
      });
      describe("when the property does not exist", () => {
        it("should return null", done => {
          const explorer = new SchemaExplorer(schema);

          expect(explorer.getFormat("user.pets")).toBeNull();

          done();
        });
      });
    });
    describe("#getMapBy", () => {
      describe("when mapping title", () => {
        it("should return a map of path to title", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy("title");

          const verify = {
            "user.organisation.type": "Organisation Type",
            "user.organisation.headquartersCountry": "Organisation Headquarters Country",
            "user.organisation.companyWebsite": "Website"
          };

          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute];

            expect(map[attribute]).toEqual(title);
          });

          done();
        });
      });
      describe("when mapping titles", () => {
        it("should return a map of path to title", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy("titles");

          const verify = {
            "user.organisation.type": ["User", "Organisation", "Organisation Type"],
            "user.organisation.headquartersCountry": [
              "User",
              "Organisation",
              "Organisation Headquarters Country"
            ],
            "user.organisation.companyWebsite": ["User", "Organisation", "Website"]
          };

          Object.keys(verify).forEach(attribute => {
            const titles = verify[attribute];

            expect(map[attribute]).toEqual(titles);
          });

          done();
        });
      });
      describe("when mapping multiple attributes", () => {
        it("should return a map of path to object", done => {
          const explorer = new SchemaExplorer(schema);
          const map = explorer.getMapBy(["title", "titles"]);

          const verify = {
            "user.organisation.type": {
              title: "Organisation Type",
              titles: ["User", "Organisation", "Organisation Type"]
            },
            "user.organisation.headquartersCountry": {
              title: "Organisation Headquarters Country",
              titles: ["User", "Organisation", "Organisation Headquarters Country"]
            },
            "user.organisation.companyWebsite": {
              title: "Website",
              titles: ["User", "Organisation", "Website"]
            }
          };

          Object.keys(verify).forEach(attribute => {
            const title = verify[attribute].title;
            const titles = verify[attribute].titles;

            expect(map[attribute]).toHaveProperty("title", title);
            expect(map[attribute]).toHaveProperty("titles", titles);
          });

          done();
        });
      });
    });
    describe("#getAttributes", () => {
      it("should return paths", () => {
        const explorer = new SchemaExplorer(schema);
        const attributes = explorer.getAttributes();

        expect(attributes.includes("user.organisation.headquartersCountry")).toEqual(true);
        expect(attributes.includes("user.organisation.companyWebsite")).toEqual(true);
      });
    });
    describe("#getAttributeBy", () => {
      it("should return the attribute", () => {
        const explorer = new SchemaExplorer(schema);
        const hqType = explorer.getAttributeBy("user.organisation.headquartersCountry", "type");
        expect(hqType).toEqual("string");
        const websiteType = explorer.getAttributeBy("user.organisation.companyWebsite", "type");
        expect(websiteType).toEqual("string");
      });
    });
  });
  describe("when the schema has complex dependencies", () => {
    const filepath = path.resolve(__dirname, "../../", "fixtures/files/incident-schema.json");
    const incidentSchema = JSON.parse(fs.readFileSync(filepath));

    let explorer = null;
    beforeEach(done => {
      explorer = new SchemaExplorer(incidentSchema);
      done();
    });
    describe("#constructor", () => {
      it("should capture all attributes", done => {
        const attributes = explorer.getAttributes();
        [
          "status",
          "type.type",
          "type.typeDescription",
          "type.category",
          "type.categoryDescription",
          "type.subCategory",
          "type.subCategoryDescription",
          "type",
          "specialty.specialty",
          "specialty",
          "harm.result",
          "harm.actualImpact",
          "harm",
          "when.when",
          "when",
          "where.site",
          "where.siteDescription",
          "where.building",
          "where.buildingDescription",
          "where.locationType",
          "where.locationTypeDescription",
          "where.location",
          "where.locationDescription",
          "where.description",
          "where",
          "involved.personType",
          "involved.role",
          "involved.firstname",
          "involved.lastname",
          "involved.gender",
          "involved.patient.hospitalNumber",
          "involved.patient.mentalIllness",
          "involved.patient",
          "involved.injury.injured",
          "involved.injury.injury",
          "involved.injury.bodyPart",
          "involved.injury",
          "involved",
          "details.description",
          "details.documents",
          "details",
          "action.actionTaken",
          "action.beingOpen",
          "action",
          "reporter.anonymous",
          "reporter.securityTeam",
          "reporter",
          "attachments.id",
          "attachments.action",
          "attachments.filename",
          "attachments.mimetype",
          "attachments.size",
          "attachments.url",
          "attachments.width",
          "attachments.height",
          "attachments.thumbnails.filename",
          "attachments.thumbnails.mimetype",
          "attachments.thumbnails.size",
          "attachments.thumbnails.url",
          "attachments.thumbnails.width",
          "attachments.thumbnails.height",
          "attachments.thumbnails",
          "attachments",
          "medicationDetails.stage",
          "medicationDetails.stageOther",
          "medicationDetails.error",
          "medicationDetails.otherError",
          "medicationDetails.involved",
          "medicationDetails",
          "pressureUlcer.admission",
          "pressureUlcer.deviceRelated",
          "pressureUlcer.contributingFactors",
          "pressureUlcer.location",
          "pressureUlcer.grade",
          "pressureUlcer.medicalDevices",
          "pressureUlcer",
          "equipment.type",
          "equipment.brandName",
          "equipment.serialNumber",
          "equipment",
          "safeguardingInformation.children",
          "safeguardingInformation.safeguardingForm",
          "safeguardingInformation"
        ].forEach(path => {
          expect(attributes.includes(path)).toEqual(true);
          const attributeSchema = explorer.getSchema(path);
          expect(attributeSchema).toBeTruthy();
        });
        done();
      });
    });
  });
});
