import coercer from "../../../src/modules/jsonschema/coercer";

const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        firstname: {
          type: "string"
        },
        lastname: {
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        created: {
          type: "string",
          format: "date-time"
        },
        comment: {
          type: "object",
          properties: {
            content: {
              type: "string"
            },
            addedOn: {
              type: "string",
              format: "date"
            }
          }
        },
        retired: {
          type: "boolean"
        }
      }
    },
    buddies: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string"
          },
          birthDate: {
            type: "string",
            format: "date"
          }
        }
      }
    }
  }
};

const dynamicSchema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        department: {
          type: "string",
          enum: ["Sales", "Marketing", "HR"]
        },
        firstname: {
          type: "string"
        },
        lastname: {
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        created: {
          type: "string",
          format: "date-time"
        },
        comment: {
          type: "object",
          properties: {
            content: {
              type: "string"
            },
            addedOn: {
              type: "string",
              format: "date"
            },
            commentType: {
              type: "string",
              enum: ["Plain", "Rich"]
            }
          },
          dependencies: {
            commentType: {
              properties: {
                commentType: {
                  enum: "Rich"
                },
                editor: {
                  type: "string"
                },
                modified: {
                  type: "string",
                  format: "date"
                },
                formatOnLoad: {
                  type: "boolean",
                  title: "Format on load"
                }
              }
            }
          }
        },
        retired: {
          type: "boolean"
        }
      },
      dependencies: {
        category: {
          properties: {
            category: {
              enum: ["Marketing"]
            },
            budget: {
              title: "Budget",
              type: "number"
            },
            budgetExpires: {
              title: "Budget expires",
              type: "string",
              format: "date"
            },
            division: {
              title: "Division",
              type: "string"
            }
          }
        }
      }
    },
    buddies: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string"
          },
          birthDate: {
            type: "string",
            format: "date"
          }
        }
      }
    }
  }
};

describe("jsonschema/coercer", () => {
  describe("when schema has no dependencies", () => {
    it("should not coerce if no coercer is found", () => {
      const object = {
        user: {
          firstname: "Alan",
          lastname: "Thoma",
          startDate: "2018-11-26T02:45:12+05:00",
          email: "invalid"
        },
        buddies: []
      };
      coercer.coerce(schema, object);
      expect(object.user.email).toEqual("invalid");
    });
    describe("when handling dates", () => {
      it("should coerce dates from date times", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00"
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(object.user.startDate).toEqual("2018-11-25");
        expect(object.buddies[0].birthDate).toEqual("1988-04-11");
        expect(object.buddies[1].birthDate).toEqual("2000-12-11");
      });
      it("should coerce dates in nested objects", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            comment: {
              content: "this is a comment",
              addedOn: "2018-10-25T02:45:12"
            }
          },
          buddies: []
        };
        coercer.coerce(schema, object);
        expect(object.user.startDate).toEqual("2018-11-25");
        expect(object.user.comment.addedOn).toEqual("2018-10-25");
      });
      it("should coerce dates from javascript Dates", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00"
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: Date.parse("1988-04-11")
            },
            {
              name: "Richard Mike",
              birthDate: Date.parse("2000-12-12T02:45:12+05:00")
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(object.user.startDate).toEqual("2018-11-25");
        expect(typeof object.buddies[0].birthDate).toEqual("string");
        expect(object.buddies[0].birthDate).toEqual("1988-04-11");
        expect(typeof object.buddies[1].birthDate).toEqual("string");
        expect(object.buddies[1].birthDate).toEqual("2000-12-11");
      });
      it("should coerce datetimes from javascript Dates", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            created: Date.parse("2018-11-26T02:45:12+05:00")
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(typeof object.user.created).toEqual("string");
        expect(object.user.created).toEqual("2018-11-25T21:45:12.000Z");
      });
      it("should not coerce a date if invalid", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "invalid_date"
          },
          buddies: []
        };
        coercer.coerce(schema, object);
        expect(object.user.startDate).toEqual("invalid_date");
      });
      it("should coerce string value of dates typed objects", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: `${new Date("2018-11-06")}`,
            comment: {
              content: "this is a comment",
              addedOn: `${new Date("2018-11-15")}`
            }
          },
          buddies: []
        };
        coercer.coerce(schema, object);
        expect(object.user.startDate).toEqual("2018-11-06");
        expect(object.user.comment.addedOn).toEqual("2018-11-15");
      });
      describe("when in a future timezone", () => {
        it("should resolve timezoned dates into utc", () => {
          const object = {
            user: {
              firstname: "Alan",
              lastname: "Thoma",
              startDate: "2018-11-26T02:45:12+05:00"
            },
            buddies: [
              {
                name: "Steven Ola",
                birthDate: "1988-04-11"
              },
              {
                name: "Richard Mike",
                birthDate: "2000-12-12T02:45:12+05:00"
              }
            ]
          };
          coercer.coerce(schema, object);
          expect(object.user.startDate).toEqual("2018-11-25");
        });
      });
    });
    describe("when handling boolean", () => {
      it("should coerce booleans from string booleans", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            retired: "true"
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(typeof object.user.retired).toEqual("boolean");
        expect(object.user.retired).toEqual(true);
      });
      it("should coerce booleans from numbers", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            retired: 1
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(typeof object.user.retired).toEqual("boolean");
        expect(object.user.retired).toEqual(true);
      });
      it("should coerce booleans from booleans", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            retired: true
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(schema, object);
        expect(typeof object.user.retired).toEqual("boolean");
        expect(object.user.retired).toEqual(true);
      });
    });
  });
  describe("when schema has dependencies", () => {
    describe("when handling dates", () => {
      it("should coerce dates from date times", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            budgetExpires: "2018-11-26T02:45:12+05:00"
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(dynamicSchema, object);
        expect(object.user.startDate).toEqual("2018-11-25");
        expect(object.user.budgetExpires).toEqual("2018-11-25");
        expect(object.buddies[0].birthDate).toEqual("1988-04-11");
        expect(object.buddies[1].birthDate).toEqual("2000-12-11");
      });
      it("should coerce dates in nested objects", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            comment: {
              content: "this is a comment",
              addedOn: "2018-10-25T02:45:12",
              commentType: "Rich",
              modified: "2018-10-25T02:45:12"
            }
          },
          buddies: []
        };
        coercer.coerce(dynamicSchema, object);
        expect(object.user.startDate).toEqual("2018-11-25");
        expect(object.user.comment.addedOn).toEqual("2018-10-25");
        expect(object.user.comment.modified).toEqual("2018-10-25");
      });
    });
    describe("when handling boolean", () => {
      it("should coerce booleans from string booleans", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            retired: "true",
            comment: {
              content: "this is a comment",
              addedOn: "2018-10-25T02:45:12",
              commentType: "Rich",
              modified: "2018-10-25T02:45:12",
              formatOnLoad: "true"
            }
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(dynamicSchema, object);
        expect(typeof object.user.retired).toEqual("boolean");
        expect(object.user.retired).toEqual(true);
        expect(typeof object.user.comment.formatOnLoad).toEqual("boolean");
        expect(object.user.comment.formatOnLoad).toEqual(true);
      });
      it("should coerce booleans from numbers", () => {
        const object = {
          user: {
            firstname: "Alan",
            lastname: "Thoma",
            startDate: "2018-11-26T02:45:12+05:00",
            retired: 1,
            comment: {
              content: "this is a comment",
              addedOn: "2018-10-25T02:45:12",
              commentType: "Rich",
              modified: "2018-10-25T02:45:12",
              formatOnLoad: 1
            }
          },
          buddies: [
            {
              name: "Steven Ola",
              birthDate: "1988-04-11"
            },
            {
              name: "Richard Mike",
              birthDate: "2000-12-12T02:45:12+05:00"
            }
          ]
        };
        coercer.coerce(dynamicSchema, object);
        expect(typeof object.user.retired).toEqual("boolean");
        expect(object.user.retired).toEqual(true);
        expect(typeof object.user.comment.formatOnLoad).toEqual("boolean");
        expect(object.user.comment.formatOnLoad).toEqual(true);
      });
    });
  });
});
