import util from "../../../../src/modules/jsonschema/util";

const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "text"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        }
      }
    },
    feed: {
      type: "object"
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};

describe("jsonschema/util", () => {
  describe("#schemaTitles", () => {
    it("should generate titles from a schema", done => {
      const titles = util.schemaTitles(schema);

      expect(titles["user.organisation.name"].title).toEqual("Organisation Name");
      expect(titles["user.firstname"].title).toEqual("First Name");
      expect(titles["user.lastname"].title).toEqual("Last Name");
      expect(titles["user.email"].title).toEqual("Email");
      expect(titles["user.endDate"].title).toEqual("End Date");
      expect(titles["custodian.birthDate"].title).toEqual("Custodian Birthdate");

      expect(titles["user.organisation.name"].titles).toEqual([
        "User",
        "Organisation",
        "Organisation Name"
      ]);
      expect(titles["user.firstname"].titles).toEqual(["User", "First Name"]);
      expect(titles["user.lastname"].titles).toEqual(["User", "Last Name"]);
      expect(titles["user.email"].titles).toEqual(["User", "Email"]);
      expect(titles["user.endDate"].titles).toEqual(["User", "End Date"]);
      expect(titles["custodian.birthDate"].titles).toEqual(["Custodian", "Custodian Birthdate"]);

      done();
    });
    it("should return null when titles are missing", done => {
      const titles = util.schemaTitles(schema);

      expect(titles["user.phoneNumber"]).toHaveProperty("title", null);
      expect(titles["user.phoneNumber"]).toHaveProperty("titles", null);
      expect(titles["user.age"]).toHaveProperty("title", null);
      expect(titles["user.age"]).toHaveProperty("titles", null);
      expect(titles["custodian.name"]).toHaveProperty("title", null);
      expect(titles["custodian.name"]).toHaveProperty("titles", null);

      done();
    });
    describe("when using sub-schemas with titles", () => {
      const schemaWithSubschemas = {
        $id: "https://api.atlas-dev.makeandship.com/schemas/experiment.json",
        type: "object",
        properties: {
          Treatment: {
            title: "Treatment",
            type: "object",
            properties: {
              outsideStandardPhaseEthambutol: {
                title: "Ethambutol",
                $ref: "#/definitions/DrugPhase"
              }
            }
          }
        },
        definitions: {
          DrugPhase: {
            type: "object",
            title: "Drug Phase",
            properties: {
              start: {
                title: "Date started",
                type: "string",
                format: "date-time"
              },
              stop: {
                title: "Date stopped",
                type: "string",
                format: "date-time"
              }
            }
          }
        }
      };
      it("should use parent schema titles", done => {
        const titles = util.schemaTitles(schemaWithSubschemas);

        const start = titles["Treatment.outsideStandardPhaseEthambutol.start"];
        const stop = titles["Treatment.outsideStandardPhaseEthambutol.stop"];

        expect(start.titles).toEqual(["Treatment", "Ethambutol", "Date started"]);
        expect(stop.titles).toEqual(["Treatment", "Ethambutol", "Date stopped"]);

        done();
      });
    });
    describe("when using array references", () => {
      const schemaWithArrayReferences = {
        $id: "https://api.atlas-dev.makeandship.com/schemas/experiment.json",
        type: "object",
        properties: {
          results: {
            type: "array",
            title: "Results",
            items: {
              $ref: "#/definitions/Result"
            }
          }
        },
        definitions: {
          Result: {
            type: "object",
            title: "Result",
            properties: {
              type: {
                title: "Type",
                type: "string"
              },
              analysed: {
                title: "Analysed",
                type: "string",
                format: "date-time"
              },
              kmer: {
                title: "K-mer",
                type: "number"
              }
            }
          }
        }
      };
      it("should use parent schema titles", done => {
        const titles = util.schemaTitles(schemaWithArrayReferences);

        const type = titles["results.type"];
        const analysed = titles["results.analysed"];
        const kmer = titles["results.kmer"];

        expect(type.titles).toEqual(["Results", "Type"]);
        expect(analysed.titles).toEqual(["Results", "Analysed"]);
        expect(kmer.titles).toEqual(["Results", "K-mer"]);

        done();
      });
    });
  });
  describe("#getPath", () => {
    describe("when the path has no dot notation", () => {
      it("should return the path untouched", () => {
        expect(util.getPath("user")).toEqual("user");
      });
    });
    describe("when the path has no arrays", () => {
      it("should return the path untouched", () => {
        expect(util.getPath("user.firstname")).toEqual("user.firstname");
      });
    });
    describe("when the has arrays", () => {
      describe("with a single element", () => {
        it("should strip array entries", () => {
          expect(util.getPath("user.0.firstname")).toEqual("user.firstname");
        });
      });
      describe("with a multiple element", () => {
        it("should strip array entries", () => {
          expect(util.getPath("user.0.buddies.0.firstname")).toEqual("user.buddies.firstname");
        });
      });
    });
  });
  describe("#dereference", () => {
    const derefSchema = {
      $schema: "http://json-schema.org/draft-04/hyper-schema",
      type: ["object"],
      definitions: {
        comment: {
          $schema: "http://json-schema.org/draft-04/hyper-schema",
          title: "Comment",
          type: ["object"],
          definitions: {
            id: {
              description: "unique identifier of comment",
              example: "01234567-89ab-cdef-0123-456789abcdef",
              readOnly: true,
              format: "uuid",
              type: ["string"]
            },
            body: {
              description: "unique name of body",
              example: "Is the order a rabbit?",
              readOnly: false,
              type: ["string"]
            },
            created_at: {
              description: "when comment was created",
              example: "2012-01-01T12:00:00Z",
              format: "date-time",
              type: ["string"]
            },
            updated_at: {
              description: "when comment was updated",
              example: "2012-01-01T12:00:00Z",
              format: "date-time",
              type: ["string"]
            }
          },
          properties: {
            created_at: {
              $ref: "#/definitions/comment/definitions/created_at"
            },
            id: {
              $ref: "#/definitions/comment/definitions/id"
            },
            body: {
              $ref: "#/definitions/comment/definitions/body"
            },
            updated_at: {
              $ref: "#/definitions/comment/definitions/updated_at"
            },
            user: {
              $ref: "#/definitions/user"
            }
          }
        },
        post: {
          $schema: "http://json-schema.org/draft-04/hyper-schema",
          title: "Post",
          type: ["object"],
          definitions: {
            id: {
              description: "unique identifier of post",
              example: "01234567-89ab-cdef-0123-456789abcdef",
              readOnly: true,
              format: "uuid",
              type: ["string"]
            },
            body: {
              description: "unique body of post",
              example: "Ah^~ My heart will be hopping^~",
              readOnly: false,
              type: ["string"]
            },
            created_at: {
              description: "when post was created",
              example: "2012-01-01T12:00:00Z",
              format: "date-time",
              type: ["string"]
            },
            updated_at: {
              description: "when post was updated",
              example: "2012-01-01T12:00:00Z",
              format: "date-time",
              type: ["string"]
            }
          },
          links: [
            {
              description: "Create a new post.",
              href: "/posts",
              method: "POST",
              rel: "create",
              schema: {
                properties: {},
                type: ["object"]
              },
              title: "Create"
            },
            {
              description: "Delete an existing post.",
              href: "/posts/{(%23%2Fdefinitions%2Fpost%2Fdefinitions%2Fid)}",
              method: "DELETE",
              rel: "destroy",
              title: "Delete"
            },
            {
              description: "Info for existing post.",
              href: "/posts/{(%23%2Fdefinitions%2Fpost%2Fdefinitions%2Fid)}",
              method: "GET",
              rel: "self",
              title: "Info"
            },
            {
              description: "List existing posts.",
              href: "/posts",
              method: "GET",
              rel: "instances",
              title: "List"
            },
            {
              description: "Update an existing post.",
              href: "/posts/{(%23%2Fdefinitions%2Fpost%2Fdefinitions%2Fid)}",
              method: "PATCH",
              rel: "update",
              schema: {
                properties: {},
                type: ["object"]
              },
              title: "Update"
            }
          ],
          properties: {
            created_at: {
              $ref: "#/definitions/post/definitions/created_at"
            },
            id: {
              $ref: "#/definitions/post/definitions/id"
            },
            body: {
              $ref: "#/definitions/post/definitions/body"
            },
            updated_at: {
              $ref: "#/definitions/post/definitions/updated_at"
            },
            user: {
              $ref: "#/definitions/user"
            },
            comments: {
              type: ["array"],
              items: {
                $ref: "#/definitions/comment"
              }
            }
          }
        },
        user: {
          $schema: "http://json-schema.org/draft-04/hyper-schema",
          title: "User",
          type: ["object"],
          definitions: {
            id: {
              description: "unique identifier of user",
              example: "01234567-89ab-cdef-0123-456789abcdef",
              readOnly: true,
              format: "uuid",
              type: ["string"]
            },
            name: {
              description: "unique name of user",
              example: "Syaro",
              readOnly: false,
              type: ["string"]
            }
          },
          properties: {
            id: {
              $ref: "#/definitions/user/definitions/id"
            },
            name: {
              $ref: "#/definitions/user/definitions/name"
            }
          }
        }
      },
      properties: {
        comment: {
          $ref: "#/definitions/comment"
        },
        post: {
          $ref: "#/definitions/post"
        },
        user: {
          $ref: "#/definitions/user"
        }
      },
      description: "A schema for an example API",
      links: [
        {
          href: "https://api.example.com",
          rel: "self"
        }
      ],
      title: "Example API"
    };
    describe("with references", () => {
      it("should replace $ref", () => {
        const replacementSchema = Object.assign({}, derefSchema);
        util.dereference(replacementSchema);

        const comment = replacementSchema.properties.comment;
        expect(comment.$ref).toBeUndefined();
        expect(comment.properties.created_at.$ref).toBeUndefined();
        expect(comment.properties.id.$ref).toBeUndefined();
        expect(comment.properties.body.$ref).toBeUndefined();
        expect(comment.properties.updated_at.$ref).toBeUndefined();
        expect(comment.properties.user.$ref).toBeUndefined();
        expect(comment.properties.user.properties.name.$ref).toBeUndefined();

        const post = replacementSchema.properties.post;
        expect(post.properties.created_at.$ref).toBeUndefined();
        expect(post.properties.id.$ref).toBeUndefined();
        expect(post.properties.body.$ref).toBeUndefined();
        expect(post.properties.updated_at.$ref).toBeUndefined();
        expect(post.properties.user.$ref).toBeUndefined();
        expect(post.properties.user.properties.name.$ref).toBeUndefined();
        expect(post.properties.comments.items.$ref).toBeUndefined();

        const user = replacementSchema.properties.user;
        expect(user.properties.id.$ref).toBeUndefined();
        expect(user.properties.name.$ref).toBeUndefined();
      });
    });
  });
  describe("#schemaFormats", () => {
    it("should generate formats from a schema", done => {
      const formats = util.schemaFormats(schema);

      expect(formats["user.startDate"].format).toEqual("date");
      expect(formats["user.endDate"].format).toEqual("date");
      expect(formats["user.email"].format).toEqual("email");
      expect(formats["custodian.dateOfDeath"].format).toEqual("date-time");

      done();
    });
    it("should return null when formats are missing", done => {
      const formats = util.schemaFormats(schema);

      expect(formats["user.organisation.name"]).toHaveProperty("format", null);
      expect(formats["user.firstname"]).toHaveProperty("format", null);
      expect(formats["user.phoneNumber"]).toHaveProperty("format", null);

      done();
    });
    describe("when using sub-schemas with formats", () => {
      const schemaWithSubschemas = {
        $id: "https://api.atlas-dev.makeandship.com/schemas/experiment.json",
        type: "object",
        properties: {
          Treatment: {
            title: "Treatment",
            type: "object",
            properties: {
              outsideStandardPhaseEthambutol: {
                title: "Ethambutol",
                $ref: "#/definitions/DrugPhase"
              }
            }
          }
        },
        definitions: {
          DrugPhase: {
            type: "object",
            title: "Drug Phase",
            properties: {
              start: {
                title: "Date started",
                type: "string",
                format: "date-time"
              },
              stop: {
                title: "Date stopped",
                type: "string",
                format: "date-time"
              }
            }
          }
        }
      };
      it("should use parent schema formats", done => {
        const formats = util.schemaFormats(schemaWithSubschemas);

        expect(formats["Treatment.outsideStandardPhaseEthambutol.start"]).toHaveProperty(
          "format",
          "date-time"
        );
        expect(formats["Treatment.outsideStandardPhaseEthambutol.stop"]).toHaveProperty(
          "format",
          "date-time"
        );

        done();
      });
    });
    describe("when using array references", () => {
      const schemaWithArrayReferences = {
        $id: "https://api.atlas-dev.makeandship.com/schemas/experiment.json",
        type: "object",
        properties: {
          results: {
            type: "array",
            title: "Results",
            items: {
              $ref: "#/definitions/Result"
            }
          }
        },
        definitions: {
          Result: {
            type: "object",
            title: "Result",
            properties: {
              type: {
                title: "Type",
                type: "string"
              },
              analysed: {
                title: "Analysed",
                type: "string",
                format: "date-time"
              },
              kmer: {
                title: "K-mer",
                type: "number"
              }
            }
          }
        }
      };
      it("should use parent schema formats", done => {
        const formats = util.schemaFormats(schemaWithArrayReferences);

        expect(formats["results.analysed"]).toHaveProperty("format", "date-time");

        done();
      });
    });
  });
});
