import Search from "../../../../src/modules/jsonschema/util/search";
const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "string"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        gender: {
          type: "string",
          enum: ["male", "female"],
          enumNames: ["Male", "Female"],
          enumNamesSearchAttribute: "genderDescription"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        verified: {
          type: "boolean"
        }
      }
    },
    feed: {
      type: "object"
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};

const dynamicSchema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "string"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        gender: {
          type: "string",
          enum: ["male", "female"],
          enumNames: ["Male", "Female"],
          enumNamesSearchAttribute: "genderDescription"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        verified: {
          type: "boolean"
        }
      }
    },
    feed: {
      type: "object"
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        },
        buddyType: {
          type: "string",
          enum: ["friend", "family"]
        }
      },
      dependencies: {
        buddyType: {
          oneOf: [
            {
              properties: {
                buddyType: {
                  enum: ["family"]
                },
                relationship: {
                  type: "string"
                },
                yearsKnown: {
                  type: "integer"
                },
                favouriteColour: {
                  type: "string",
                  enum: ["Red", "Yellow", "Blue"]
                }
              }
            }
          ]
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};
describe("Search", () => {
  describe("#getSortable", () => {
    describe("with standard schema", () => {
      let sortable = null;
      beforeEach(done => {
        const search = new Search(schema);
        sortable = search.getSortable();
        done();
      });
      it("should return sortable attributes", done => {
        expect(sortable.length).toEqual(14);
        [
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "custodian.birthDate",
          "custodian.dateOfDeath",
          "user.age",
          "user.bmi",
          "user.endDate",
          "user.gender",
          "user.genderDescription",
          "user.role",
          "user.score",
          "user.startDate",
          "modified",
          "created"
        ].forEach(path => {
          expect(sortable.includes(path));
        });
        done();
      });
    });
    describe("with dynamic schema", () => {
      let sortable = null;
      beforeEach(done => {
        const search = new Search(dynamicSchema);
        sortable = search.getSortable();
        done();
      });
      it("should return sortable attributes", done => {
        expect(sortable.length).toEqual(17);
        const expectedAttributes = [
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "buddy.buddyType",
          "buddy.yearsKnown",
          "buddy.favouriteColour",
          "custodian.birthDate",
          "custodian.dateOfDeath",
          "user.age",
          "user.bmi",
          "user.endDate",
          "user.gender",
          "user.genderDescription",
          "user.role",
          "user.score",
          "user.startDate",
          "modified",
          "created"
        ];
        expect(expectedAttributes.length).toEqual(17);
        expectedAttributes.forEach(path => {
          expect(sortable.includes(path));
        });
        done();
      });
    });
  });
  describe("#getRangeFilters", () => {
    describe("with standard schema", () => {
      let filters = null;
      beforeEach(done => {
        const search = new Search(schema);
        filters = search.getRangeFilters();
        done();
      });
      it("should return filters", done => {
        expect(filters).toHaveProperty("choices");
        expect(filters.choices.length).toEqual(3);
        ["user.role", "user.gender", "user.verified"].forEach(path => {
          expect(filters.choices.includes(path));
        });
        expect(filters).toHaveProperty("ranges");
        expect(filters.ranges.length).toEqual(9);
        [
          "user.age",
          "user.startDate",
          "user.endDate",
          "user.bmi",
          "user.score",
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "custodian.birthDate",
          "custodian.dateOfDeath"
        ].forEach(path => {
          expect(filters.ranges.includes(path));
        });
        done();
      });
    });
    describe("with dynamic schema", () => {
      let filters = null;
      beforeEach(done => {
        const search = new Search(dynamicSchema);
        filters = search.getRangeFilters();
        done();
      });
      it("should return filters", done => {
        expect(filters).toHaveProperty("choices");
        expect(filters.choices.length).toEqual(5);
        [
          "user.role",
          "user.gender",
          "user.verified",
          "buddy.buddyType",
          "buddy.favouriteColour"
        ].forEach(path => {
          expect(filters.choices.includes(path));
        });
        expect(filters).toHaveProperty("ranges");
        expect(filters.ranges.length).toEqual(10);
        [
          "user.age",
          "user.startDate",
          "user.endDate",
          "user.bmi",
          "user.score",
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "buddy.yearsKnown",
          "custodian.birthDate",
          "custodian.dateOfDeath"
        ].forEach(path => {
          expect(filters.ranges.includes(path));
        });
        done();
      });
    });
  });
  describe("#getRanges", () => {
    describe("with standard schema", () => {
      let ranges = null;
      beforeEach(done => {
        const search = new Search(schema);
        ranges = search.getRanges();
        done();
      });
      it("should return ranges", done => {
        expect(ranges.length).toEqual(9);
        [
          "user.age",
          "user.startDate",
          "user.endDate",
          "user.bmi",
          "user.score",
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "custodian.birthDate",
          "custodian.dateOfDeath"
        ].forEach(path => {
          expect(ranges.includes(path));
        });
        done();
      });
    });
    describe("with dynamic schema", () => {
      let ranges = null;
      beforeEach(done => {
        const search = new Search(dynamicSchema);
        ranges = search.getRanges();
        done();
      });
      it("should return ranges", done => {
        expect(ranges.length).toEqual(10);
        [
          "user.age",
          "user.startDate",
          "user.endDate",
          "user.bmi",
          "user.score",
          "buddy.birthDate",
          "buddy.dateOfDeath",
          "buddy.yearsKnown",
          "custodian.birthDate",
          "custodian.dateOfDeath"
        ].forEach(path => {
          expect(ranges.includes(path));
        });
        done();
      });
    });
  });
  describe("#getChoices", () => {
    describe("with standard schema", () => {
      let choices = null;
      beforeEach(done => {
        const search = new Search(schema);
        choices = search.getChoices();
        done();
      });
      it("should return choices", done => {
        expect(choices.length).toEqual(3);
        ["user.role", "user.gender", "user.verified"].forEach(path => {
          expect(choices.includes(path));
        });
        done();
      });
    });
    describe("with dynamic schema", () => {
      let choices = null;
      beforeEach(done => {
        const search = new Search(dynamicSchema);
        choices = search.getChoices();
        done();
      });
      it("should return choices", done => {
        expect(choices.length).toEqual(5);
        [
          "user.role",
          "user.gender",
          "user.verified",
          "buddy.buddyType",
          "buddy.favouriteColour"
        ].forEach(path => {
          expect(choices.includes(path));
        });
        done();
      });
    });
  });
  describe("#getEnums", () => {
    describe("when valid", () => {
      describe("when the attribute is a choice", () => {
        it("should return enums", () => {
          const search = new Search(schema);
          const enums = search.getEnums("user.role");
          expect(enums.length).toEqual(3);
          ["administrator", "subscriber", "editor"].forEach(item => {
            expect(enums.includes(item)).toEqual(true);
          });
        });
      });
    });
    describe("when not valid", () => {
      describe("when the attribute doesnt exist", () => {
        it("should return null", () => {
          const search = new Search(schema);
          const enums = search.getEnums("user.jobTitles");
          expect(enums).toBeNull();
        });
      });
      describe("when the attribute is not a choice", () => {
        it("should return null", () => {
          const search = new Search(schema);
          const enums = search.getEnums("user.firstname");
          expect(enums).toBeNull();
        });
      });
    });
  });
  describe("#getEnumSize", () => {
    describe("when valid", () => {
      describe("when the attribute is a choice", () => {
        it("should return the number of enums", () => {
          const search = new Search(schema);
          const size = search.getEnumSize("user.role");
          expect(size).toEqual(3);
        });
      });
    });
    describe("when not valid", () => {
      describe("when the attribute doesnt exist", () => {
        it("should return 0", () => {
          const search = new Search(schema);
          const size = search.getEnumSize("user.jobTitle");
          expect(size).toEqual(null);
        });
      });
      describe("when the attribute is a choice", () => {
        it("should return 0", () => {
          const search = new Search(schema);
          const size = search.getEnumSize("user.firstname");
          expect(size).toEqual(null);
        });
      });
    });
  });
});
