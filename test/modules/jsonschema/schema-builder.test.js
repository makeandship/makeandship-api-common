import moment from "moment";

import schemaBuilder from "../../../src/modules/jsonschema/mongoose/schema-builder";

const schema = {
  type: "object",
  properties: {
    category: {
      type: "string",
      enum: ["National", "International"]
    },
    firstname: {
      type: "string"
    },
    lastname: {
      type: "string"
    },
    points: {
      type: "integer"
    },
    dob: {
      type: "string",
      format: "date"
    },
    wokeUp: {
      type: "string",
      format: "date-time"
    },
    isAdmin: {
      type: "boolean",
      default: false
    },
    meaningOfLife: {
      type: ["integer", "string", "boolean"]
    },
    comment: {
      type: "object",
      properties: {
        commentType: {
          type: "string",
          enum: ["Simple", "Rich"]
        },
        content: {
          type: "string",
          default: "No content"
        },
        addedOn: {
          type: "string"
        }
      }
    },
    buddies: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string",
            index: true
          },
          age: {
            type: "number"
          }
        }
      }
    },
    countriesLived: {
      type: "array",
      items: {
        type: "string"
      }
    },
    countriesInformation: {
      type: "array",
      items: {
        type: "object"
      }
    }
  }
};

describe("jsonschema/schemaBuilder", () => {
  describe("when no extensions used", () => {
    describe("with no dependencies", () => {
      const model = schemaBuilder.generateModel(schema);
      it("should generate a model without extensions", () => {
        expect(model).toBeTruthy();
      });
      it("should generate string attributes", () => {
        expect(model.category.type).toEqual("String");
        expect(model.firstname.type).toEqual("String");
        expect(model.lastname.type).toEqual("String");
      });
      it("should generate date attributes", () => {
        expect(model.dob.type).toEqual("Date");
        expect(model.dob.type).toEqual("Date");
      });
      it("should generate date-time attributes", () => {
        expect(model.wokeUp.type).toEqual("Date");
        expect(model.wokeUp.type).toEqual("Date");
      });
      it("should generate number attributes", () => {
        expect(model.points.type).toEqual("Number");
      });
      it("should generate boolean attributes", () => {
        expect(model.isAdmin.type).toEqual("Boolean");
      });
      it("should generate Mixed attributes", () => {
        expect(model.meaningOfLife.type).toEqual("Mixed");
      });
      it("should generate default values", () => {
        expect(model.isAdmin.default).toEqual(false);
        expect(model.comment.content.default).toEqual("No content");
      });
      it("should generate nested objects", () => {
        expect(model.comment.content.type).toEqual("String");
        expect(model.comment.addedOn.type).toEqual("String");
      });
      it("should generate nested arrays", () => {
        expect(model.buddies[0].name.type).toEqual("String");
        expect(model.buddies[0].age.type).toEqual("Number");
      });
      it("should generate indexed fields", () => {
        expect(model.buddies[0].name.index).toEqual(true);
      });
      describe("when there is an array of items", () => {
        describe("when items are objects with no properties", () => {
          it("should generate an array of Mixed type", done => {
            expect(Array.isArray(model.countriesInformation)).toEqual(true);

            const definition = model.countriesInformation[0];
            expect(definition.type).toEqual("Mixed");

            done();
          });
        });
        describe("when items are objects with a basic type", () => {
          it("should generate an array of that basic type", done => {
            expect(Array.isArray(model.countriesLived)).toEqual(true);

            const definition = model.countriesLived[0];
            expect(definition.type).toEqual("String");

            done();
          });
        });
      });
      describe("when there is a default value", () => {
        describe("which is an empty string", () => {
          it("should not set a default", done => {
            const defaultSchema = Object.assign({}, schema);
            defaultSchema.properties.firstname.default = "";

            const model = schemaBuilder.generateModel(schema);
            expect(model.firstname.default).toBeUndefined();

            done();
          });
        });
        describe("which is not an empty string", () => {
          it("should set a default", done => {
            const defaultSchema = Object.assign({}, schema);
            defaultSchema.properties.firstname.default = "Mark";

            const model = schemaBuilder.generateModel(schema);
            expect(model.firstname.default).toEqual("Mark");

            done();
          });
        });
      });
      describe("when there is an index value", () => {
        it("should not set an index", done => {
          const defaultSchema = Object.assign({}, schema);
          defaultSchema.properties.firstname.index = true;

          const model = schemaBuilder.generateModel(schema);
          expect(model.firstname.index).toEqual(true);
          done();
        });
      });
    });
    describe("when dependencies", () => {
      it("should generate dependency attributes", done => {
        const dependencySchema = Object.assign({}, schema);
        dependencySchema.dependencies = {
          comment: {
            oneOf: [
              {
                properties: {
                  category: {
                    enum: ["International"]
                  },
                  countryOfBirth: {
                    type: "string"
                  },
                  countryOfResidence: {
                    type: "string"
                  }
                }
              }
            ]
          }
        };
        const model = schemaBuilder.generateModel(dependencySchema);
        expect(model.countryOfBirth.type).toEqual("String");
        expect(model.countryOfResidence.type).toEqual("String");
        done();
      });
      describe("with nested dependcies", () => {
        it("should generate nested properties", done => {
          const dependencySchema = Object.assign({}, schema);
          dependencySchema.properties.comment.dependencies = {
            commentType: {
              oneOf: [
                {
                  properties: {
                    commentType: {
                      enum: ["Rich"]
                    },
                    html: {
                      type: "string"
                    }
                  }
                }
              ]
            }
          };
          const model = schemaBuilder.generateModel(dependencySchema);
          expect(model.comment.html.type).toEqual("String");
          done();
        });
      });
    });
  });
  describe("when using extensions", () => {
    describe("when using relationship extensions", () => {
      const model = schemaBuilder.generateModel(schema, {
        likes: [
          {
            type: "ObjectId",
            ref: "Like"
          }
        ],
        avatar: {
          ref: "Avatar"
        }
      });
      it("should generate a model with extensions", () => {
        expect(model).toBeTruthy();
      });
      it("should generate array references", () => {
        expect(model.likes.length).toEqual(1);

        const like = model.likes.shift();
        expect(like.type).toEqual("ObjectId");
        expect(like.ref).toEqual("Like");
      });
      it("should generate object references", () => {
        expect(model.avatar.type).toEqual("ObjectId");
        expect(model.avatar.ref).toEqual("Avatar");
      });
    });
    describe("when using default extensions", () => {
      describe("when using a static default", () => {
        it("should apply the default", () => {
          const model = schemaBuilder.generateModel(schema, {
            dob: {
              default: "1975-10-01"
            }
          });
          expect(model.dob).toHaveProperty("default", "1975-10-01");
        });
        it("should not override the default type", () => {
          const model = schemaBuilder.generateModel(schema, {
            dob: {
              default: "1975-10-01"
            }
          });
          expect(model.dob).toHaveProperty("type", "Date");
        });
      });
      describe("when using a function as a default", () => {
        it("should apply the default", () => {
          const model = schemaBuilder.generateModel(schema, {
            dob: {
              default: () => {
                return moment([2018, 9, 18]).toISOString();
              }
            }
          });
          expect(model.dob).toHaveProperty("default");
          expect(typeof model.dob.default).toEqual("function");
        });
        it("should not override the default type", () => {
          const model = schemaBuilder.generateModel(schema, {
            dob: {
              default: () => {
                return moment([2018, 9, 18]).toISOString();
              }
            }
          });
          expect(model.dob).toHaveProperty("type", "Date");
        });
      });
    });
  });
});
