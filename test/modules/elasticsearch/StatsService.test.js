import SearchQuery from "../../../src/modules/elasticsearch/queries/SearchQuery";
import ElasticService from "../../../src/modules/elasticsearch/ElasticService";
import StatsService from "../../../src/modules/elasticsearch/StatsService";

const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "string"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        gender: {
          type: "string",
          enum: ["male", "female"],
          enumNames: ["Male", "Female"],
          enumNamesSearchAttribute: "genderDescription"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        verified: {
          type: "boolean"
        }
      }
    },
    feed: {
      type: "array",
      items: {
        type: "object"
      }
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};

const sleep = ms => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

describe.skip("StatsService", () => {
  describe("constructor", () => {
    it("should initialise attributes", () => {
      const service = new StatsService({ type: "profile" }, schema);
      expect(service.config).toBeTruthy();
      expect(service.client).toBeTruthy();
      expect(service.options).toBeTruthy();
    });
  });
  describe("buildSearch", () => {
    describe("when only index is set", () => {
      it("should generate a query", done => {
        const service = new StatsService(
          { host: "http://localhost:9200", index: "user", type: "profile" },
          schema
        );
        const query = service.buildSearch();
        expect(query).toHaveProperty("index", "user-profile");
        expect(query).toHaveProperty("type", "_doc");
        done();
      });
    });
    describe("when using a search", () => {
      it("should generate a query", done => {
        const service = new StatsService(
          { host: "http://localhost:9200", index: "user", type: "profile" },
          schema
        );
        const query = service.buildSearch({
          query: {
            bool: {
              must: {
                query_string: {
                  query: "Argentina"
                }
              }
            }
          }
        });
        expect(query).toHaveProperty("index", "user-profile");
        expect(query).toHaveProperty("type", "_doc");
        expect(query).toHaveProperty("body", {
          query: {
            bool: {
              must: {
                query_string: {
                  query: "Argentina"
                }
              }
            }
          }
        });
        done();
      });
    });
    describe("when using options", () => {
      it("should generate a query", done => {
        const service = new StatsService(
          { host: "http://localhost:9200", index: "user", type: "profile" },
          schema
        );
        const query = service.buildSearch(null, { "settings.max_window_size": 20000 });
        expect(query).toHaveProperty("index", "user-profile");
        expect(query).toHaveProperty("type", "_doc");
        expect(query["settings.max_window_size"]).toEqual(20000);
        done();
      });
    });
  });
  describe("count", () => {
    let elasticService = null;
    const config = {
      type: "profile",
      index: "user",
      log: "error",
      resultsPerPage: 10,
      settings: {
        "index.mapping.total_fields.limit": 2000
      },
      indexSizeLimit: 1000,
      host: "http://localhost:9200"
    };
    beforeEach(async done => {
      elasticService = new ElasticService(config, schema);
      // make sure an elastic index is available
      await elasticService.deleteIndex();
      await elasticService.createIndex();

      await elasticService.indexDocuments([
        {
          email: "mark@makeandship.com",
          verified: true
        },
        {
          email: "yassire@makeandship.com",
          verified: false
        }
      ]);

      // wait for indexing to complete
      let count = await elasticService.count();
      while (count < 2) {
        await sleep(500);
        count = await elasticService.count();
      }

      done();
    });
    describe("when no query is given", () => {
      it("should return a count of all documents", async done => {
        const count = await elasticService.count();
        expect(count).toEqual(2);

        done();
      });
      it("should return a count of all documents matching a search", async done => {
        const query = new SearchQuery(
          {
            verified: true
          },
          schema,
          "person"
        );
        const count = await elasticService.count(query);
        expect(count).toEqual(1);

        done();
      });
    });
  });
});
