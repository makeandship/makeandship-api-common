import SearchQuery from "../../../../src/modules/elasticsearch/queries/SearchQuery";
import CountSearchQuery from "../../../../src/modules/elasticsearch/queries/CountSearchQuery";

const schema = {
  type: "object",
  properties: {
    firstname: {
      type: "string"
    },
    lastname: {
      type: "string"
    },
    points: {
      type: "integer"
    },
    dob: {
      type: "string",
      format: "date"
    },
    wokeUp: {
      type: "string",
      format: "date-time"
    },
    isAdmin: {
      type: "boolean",
      default: false
    },
    meaningOfLife: {
      type: ["integer", "string", "boolean"]
    },
    metadata: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string",
            index: true
          },
          patient: {
            type: "object",
            properties: {
              smoker: {
                type: "number"
              }
            }
          }
        }
      }
    }
  }
};

describe("CountSearchQuery", () => {
  describe("#constructor", () => {
    it("should initialise the search query", () => {
      const filters = { firstname: "Mark" };
      const query = new SearchQuery(filters, schema, "person");
      const countQuery = new CountSearchQuery(query);

      expect(countQuery.searchQuery).toBeTruthy();
    });
  });
  describe("#toJSON", () => {
    describe("#constructor", () => {
      it("should initialise remove size", () => {
        const filters = { firstname: "Mark" };
        const query = new SearchQuery(filters, schema, "person");
        const countQuery = new CountSearchQuery(query);
        const json = countQuery.toJSON();

        expect(json).not.toHaveProperty("from");
      });
      it("should initialise remove from", () => {
        const filters = { firstname: "Mark" };
        const query = new SearchQuery(filters, schema, "person");
        const countQuery = new CountSearchQuery(query);
        const json = countQuery.toJSON();

        expect(json).not.toHaveProperty("from");
      });
      it("should initialise remove sort", () => {
        const filters = { firstname: "Mark" };
        const query = new SearchQuery(filters, schema, "person");
        query.setSort("dob");
        query.setOrder("asc");
        const countQuery = new CountSearchQuery(query);
        const json = countQuery.toJSON();

        expect(json).not.toHaveProperty("sort");
      });
    });
  });
});
