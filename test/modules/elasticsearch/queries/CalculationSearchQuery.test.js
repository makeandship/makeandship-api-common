import CalculationSearchQuery from "../../../../src/modules/elasticsearch/queries/CalculationSearchQuery";

const schema = {
  type: "object",
  properties: {
    firstname: {
      type: "string"
    },
    lastname: {
      type: "string"
    },
    city: {
      type: "string",
      enum: ["London", "Manchester", "Birmingham"]
    },
    points: {
      type: "integer"
    },
    dob: {
      type: "string",
      format: "date"
    },
    wokeUp: {
      type: "string",
      format: "date-time"
    },
    isAdmin: {
      type: "boolean",
      default: false
    },
    meaningOfLife: {
      type: ["integer", "string", "boolean"]
    },
    metadata: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string",
            index: true
          },
          patient: {
            type: "object",
            properties: {
              cigarettesPerDay: {
                type: "number"
              }
            }
          }
        }
      }
    }
  }
};

describe("CalculationSearchQuery", () => {
  describe("#buildSearchQuery", () => {
    describe("when no filters were set", () => {
      let json = null;
      beforeEach(() => {
        const filters = {};
        const query = new CalculationSearchQuery(filters, schema, "person");
        json = query.toJSON();
      });

      it("should provide a single json response", () => {
        expect(json).not.toHaveProperty("core");
        expect(json).toHaveProperty("from");
        expect(json).toHaveProperty("size");
        expect(json).toHaveProperty("query");
      });
      it("should default size to 0", () => {
        expect(json).toHaveProperty("size", 0);
      });
      it("should have aggregations", () => {
        expect(json).toHaveProperty("aggs");
        expect(Object.keys(json.aggs).length).toEqual(9);
      });
      it("should request sum and avg for integers", () => {
        expect(json.aggs).toHaveProperty("sum_points");
        expect(json.aggs.sum_points).toHaveProperty("sum");
        expect(json.aggs.sum_points.sum).toHaveProperty("field", "points");

        expect(json.aggs).toHaveProperty("avg_points");
        expect(json.aggs.avg_points).toHaveProperty("avg");
        expect(json.aggs.avg_points.avg).toHaveProperty("field", "points");
      });
      it("should request sum and avg for numbers", () => {
        expect(json.aggs["sum_metadata.patient.cigarettesPerDay"]).toBeDefined();
        expect(json.aggs["sum_metadata.patient.cigarettesPerDay"]).toHaveProperty("sum");
        expect(json.aggs["sum_metadata.patient.cigarettesPerDay"].sum).toHaveProperty(
          "field",
          "metadata.patient.cigarettesPerDay"
        );

        expect(json.aggs["avg_metadata.patient.cigarettesPerDay"]).toBeDefined();
        expect(json.aggs["avg_metadata.patient.cigarettesPerDay"]).toHaveProperty("avg");
        expect(json.aggs["avg_metadata.patient.cigarettesPerDay"].avg).toHaveProperty(
          "field",
          "metadata.patient.cigarettesPerDay"
        );
      });
      it("should request sum and average for enumerations", () => {
        expect(json.aggs).toHaveProperty("city");
        const city = json.aggs.city;
        expect(city).toHaveProperty("aggs");

        expect(city.aggs).toHaveProperty("sum_points");
        const sumPoints = city.aggs.sum_points;
        expect(sumPoints).toHaveProperty("sum", { field: "points" });

        expect(city.aggs).toHaveProperty("avg_points");
        const avgPoints = city.aggs.avg_points;
        expect(avgPoints).toHaveProperty("avg", { field: "points" });

        expect(city.aggs["sum_metadata.patient.cigarettesPerDay"]).toBeDefined();
        const sumCigarettes = city.aggs["sum_metadata.patient.cigarettesPerDay"];
        expect(sumCigarettes).toHaveProperty("sum", { field: "metadata.patient.cigarettesPerDay" });

        expect(city.aggs["avg_metadata.patient.cigarettesPerDay"]).toBeDefined();
        const avgCigarettes = city.aggs["avg_metadata.patient.cigarettesPerDay"];
        expect(avgCigarettes).toHaveProperty("avg", { field: "metadata.patient.cigarettesPerDay" });
      });
    });
    describe("when filters were set", () => {
      describe("when a single filter is set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { isAdmin: true };
          const query = new CalculationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });

        it("should provide multiple json responses", () => {
          expect(Object.keys(json).length).toEqual(2);
        });
        it("should provide a core response", () => {
          expect(Object.keys(json).includes("core"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("isAdmin"));
        });
        it("should apply filters to the core", () => {
          const core = json.core;
          expect(core).toHaveProperty("query");
          expect(core.query).toHaveProperty("bool");
          expect(core.query.bool).toHaveProperty("filter");
          expect(core.query.bool.filter).toHaveProperty("match");
          expect(core.query.bool.filter.match).toHaveProperty("isAdmin", true);
        });
        it("should apply remove the in-scope filter from its own filter query", () => {
          const isAdmin = json.isAdmin;
          expect(isAdmin).toHaveProperty("query");
          expect(isAdmin.query).toHaveProperty("bool", {});
        });
      });
      describe("when multiple filters are set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { isAdmin: true, meaningOfLife: true };
          const query = new CalculationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });

        it("should provide multiple json responses", () => {
          expect(Object.keys(json).length).toEqual(3);
        });
        it("should provide a core response", () => {
          expect(Object.keys(json).includes("core"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("isAdmin"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("meaningOfLife"));
        });
        it("should apply filters to the core", done => {
          const core = json.core;
          expect(core).toHaveProperty("query");
          expect(core.query).toHaveProperty("bool");
          expect(core.query.bool).toHaveProperty("filter");
          expect(core.query.bool.filter.length).toEqual(2);
          for (const filter of core.query.bool.filter) {
            expect(filter).toHaveProperty("match");
            for (const key of Object.keys(filter.match)) {
              switch (key) {
                case "isAdmin":
                  expect(filter.match[key]).toEqual(true);
                  break;
                case "meaningOfLife":
                  expect(filter.match[key]).toEqual(true);
                  break;
                default:
                  done.fail("Matches should be isAdmin and meaningOfLife");
              }
            }
          }
          done();
        });
        it("should apply remove the in-scope filter from its own filter query", () => {
          const isAdmin = json.isAdmin;
          expect(isAdmin).toHaveProperty("query");
          expect(isAdmin.query).toHaveProperty("bool");
          expect(isAdmin.query.bool).toHaveProperty("filter");
          expect(isAdmin.query.bool.filter).toHaveProperty("match");
          expect(isAdmin.query.bool.filter.match).not.toHaveProperty("isAdmin");
          expect(isAdmin.query.bool.filter.match).toHaveProperty("meaningOfLife", true);

          const meaningOfLife = json.meaningOfLife;
          expect(meaningOfLife).toHaveProperty("query");
          expect(meaningOfLife.query).toHaveProperty("bool");
          expect(meaningOfLife.query.bool).toHaveProperty("filter");
          expect(meaningOfLife.query.bool.filter).toHaveProperty("match");
          expect(meaningOfLife.query.bool.filter.match).not.toHaveProperty("meaningOfLife");
          expect(meaningOfLife.query.bool.filter.match).toHaveProperty("isAdmin", true);
        });
      });
    });
    describe("when pagination attributes are set", () => {
      describe("when per is set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { page: 1, per: 10 };
          const query = new CalculationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });
        it("should override the default", () => {
          expect(json).toHaveProperty("size", 10);
        });
      });
    });
    describe("when options are set", () => {
      describe("when excluding an aggregation", () => {
        describe("when a single filter is set", () => {
          let json = null;
          beforeEach(() => {
            const filters = { isAdmin: true };
            const query = new CalculationSearchQuery(filters, schema, "person", {
              aggs: { city: { exclude: true } }
            });
            json = query.toJSON();
          });
          it("should not have the excluded aggregation", () => {
            const core = json.core;
            expect(core).toHaveProperty("aggs");
            expect(Object.keys(core.aggs).includes("city")).toEqual(false);
          });
        });
        describe("when multiple filters are set", () => {
          let json = null;
          beforeEach(() => {
            const filters = { isAdmin: true, meaningOfLife: true };
            const query = new CalculationSearchQuery(filters, schema, "person", {
              aggs: { city: { exclude: true } }
            });
            json = query.toJSON();
          });

          it("should not have the excluded aggregation", () => {
            const core = json.core;
            expect(core).toHaveProperty("aggs");
            expect(Object.keys(core.aggs).includes("city")).toEqual(false);
          });
        });
      });
      describe("when setting an aggregation size", () => {
        describe("when size is not set", () => {
          let json = null;
          beforeEach(() => {
            const query = new CalculationSearchQuery({}, schema, "person", {});
            json = query.toJSON();
          });
          it("should not set a term aggregation size", () => {
            expect(json).toHaveProperty("aggs");
            expect(json.aggs).toHaveProperty("city");
            expect(json.aggs.city).toHaveProperty("terms");
            expect(json.aggs.city.terms).toHaveProperty("field", "city");
          });
        });
        describe("when size is set to a positive number", () => {
          let json = null;
          beforeEach(() => {
            const query = new CalculationSearchQuery({}, schema, "person", {
              aggs: { city: { size: 200 } }
            });
            json = query.toJSON();
          });
          it("should not set a term aggregation size", () => {
            expect(json).toHaveProperty("aggs");
            expect(json.aggs).toHaveProperty("city");
            expect(json.aggs.city).toHaveProperty("terms");
            expect(json.aggs.city.terms).toHaveProperty("field", "city");
            expect(json.aggs.city.terms).toHaveProperty("size", 200);
          });
        });
        describe("when size is set to 0", () => {});
      });
    });
  });
});
