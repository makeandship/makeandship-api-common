import MetricSearchQuery from "../../../../src/modules/elasticsearch/queries/MetricSearchQuery";

const schema = {
  type: "object",
  properties: {
    firstname: {
      type: "string"
    },
    lastname: {
      type: "string"
    },
    points: {
      type: "integer"
    },
    dob: {
      type: "string",
      format: "date"
    },
    wokeUp: {
      type: "string",
      format: "date-time"
    },
    isAdmin: {
      type: "boolean",
      default: false
    },
    meaningOfLife: {
      type: ["integer", "string", "boolean"]
    },
    country: {
      type: "string",
      enum: ["UK", "IN", "CH"]
    },
    metadata: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string",
            index: true
          },
          patient: {
            type: "object",
            properties: {
              smoker: {
                type: "number"
              }
            }
          }
        }
      }
    }
  }
};

describe("MetricSearchQuery", () => {
  describe("#constructor", () => {
    it("should initialise filters", () => {
      const filters = { firstname: "Mark" };
      const query = new MetricSearchQuery(filters, schema, "person");
      expect(Object.keys(query.filters).includes("firstname")).toEqual(true);
    });
    it("should initialise schema", () => {
      const filters = { firstname: "Mark" };
      const query = new MetricSearchQuery(filters, schema, "person");
      expect(query.schema).toEqual(schema);
    });
    it("should initialise type", () => {
      const filters = { firstname: "Mark" };
      const query = new MetricSearchQuery(filters, schema, "person");
      expect(query.type).toEqual("person");
    });
  });
  describe("#isMulti", () => {
    describe("when filters are not passed", () => {
      it("should return false", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        expect(query.isMulti()).toEqual(false);
      });
    });
    describe("when filters are null", () => {
      it("should return false", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        expect(query.isMulti(null)).toEqual(false);
      });
    });
    describe("when filters are empty", () => {
      it("should return false", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        expect(query.isMulti({})).toEqual(false);
      });
    });
    describe("when filters are set", () => {
      it("should return true", () => {
        const filters = { "metadata.patient.smoker": true };
        const query = new MetricSearchQuery(filters, schema, "person");
        expect(query.isMulti(filters)).toEqual(true);
      });
    });
  });
  describe("#getExcludeTerm", () => {
    describe("when no aggs are captured in options", () => {
      it("should return true", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        expect(query.getExcludeTerm("division")).toEqual(false);
      });
    });
    describe("when aggs are captured in options without exclude", () => {
      it("should return true", () => {
        const query = new MetricSearchQuery({}, schema, "person", { aggs: { division: {} } });
        expect(query.getExcludeTerm("division")).toEqual(false);
      });
    });
    describe("when aggs are captured in options with exclude", () => {
      it("should return false", () => {
        const query = new MetricSearchQuery({}, schema, "person", {
          aggs: { division: { exclude: true } }
        });
        expect(query.getExcludeTerm("division")).toEqual(true);
      });
    });
  });
  describe("#getTermSize", () => {
    describe("when no aggs are captured in options", () => {
      it("should return null", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        expect(query.getTermSize("division")).toEqual(null);
      });
    });
    describe("when aggs are captured in options without size", () => {
      it("should return null", () => {
        const query = new MetricSearchQuery({}, schema, "person", { aggs: { division: {} } });
        expect(query.getTermSize("division")).toEqual(null);
      });
    });
    describe("when aggs are captured in options with size", () => {
      it("should return size", () => {
        const query = new MetricSearchQuery({}, schema, "person", {
          aggs: { division: { size: 200 } }
        });
        expect(query.getTermSize("division")).toEqual(200);
      });
    });
  });
  describe("#getDefaultTermSize", () => {
    describe("when invalid", () => {
      describe("when no term is passed", () => {
        it("should return null", () => {
          const query = new MetricSearchQuery({}, schema, "person");
          expect(query.getDefaultTermSize()).toEqual(null);
        });
      });
    });
    describe("when valid", () => {
      describe("when a no enum choice term is passed", () => {
        it("should return null", () => {
          const query = new MetricSearchQuery({}, schema, "person", { aggs: { division: {} } });
          expect(query.getDefaultTermSize("city")).toEqual(null);
        });
      });
      describe("when a choice term is passed", () => {
        it("should return 3", () => {
          const query = new MetricSearchQuery({}, schema, "person", { aggs: { division: {} } });
          expect(query.getDefaultTermSize("country")).toEqual(3);
        });
      });
    });
  });
  describe("#processResults", () => {
    describe("when results are not provided", () => {
      it("should return null", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        const results = query.processResults();
        expect(results).toEqual(null);
      });
    });
    describe("when results are empty", () => {
      it("should return null", () => {
        const query = new MetricSearchQuery({}, schema, "person");
        const results = query.processResults({});
        expect(results).toEqual(null);
      });
    });
    describe("when results are provided", () => {
      it("should replace overall aggregations with filtered ones", () => {
        const aggregations = {
          core: {
            aggregations: {
              isAdmin: {
                buckets: [
                  {
                    doc_count: 192,
                    key: true
                  },
                  {
                    doc_count: 192,
                    key: false
                  }
                ]
              }
            }
          },
          isAdmin: {
            aggregations: {
              isAdmin: {
                buckets: [
                  {
                    doc_count: 402,
                    key: true
                  },
                  {
                    doc_count: 362,
                    key: false
                  }
                ]
              }
            }
          }
        };
        const query = new MetricSearchQuery({}, schema, "person");
        const results = query.processResults(aggregations);
        expect(results).toHaveProperty("aggregations");
        expect(results.aggregations).toHaveProperty("isAdmin");
        expect(results.aggregations.isAdmin).toHaveProperty("buckets");
        const buckets = results.aggregations.isAdmin.buckets;

        expect(buckets.length).toEqual(2);
        for (const bucket of buckets) {
          const count = bucket.doc_count;
          const key = bucket.key;

          if (key) {
            expect(count).toEqual(402);
          } else {
            expect(count).toEqual(362);
          }
        }
      });
    });
  });
});
