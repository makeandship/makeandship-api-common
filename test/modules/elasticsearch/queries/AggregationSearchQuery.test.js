import AggregationSearchQuery from "../../../../src/modules/elasticsearch/queries/AggregationSearchQuery";

const schema = {
  type: "object",
  properties: {
    firstname: {
      type: "string"
    },
    lastname: {
      type: "string"
    },
    city: {
      type: ["string", "null"],
      enum: [null, "London", "Manchester", "Birmingham"]
    },
    points: {
      type: "integer"
    },
    dob: {
      type: "string",
      format: "date"
    },
    wokeUp: {
      type: "string",
      format: "date-time"
    },
    isAdmin: {
      type: "boolean",
      default: false
    },
    meaningOfLife: {
      type: ["integer", "string", "boolean"]
    },
    metadata: {
      type: "array",
      items: {
        type: "object",
        properties: {
          name: {
            type: "string",
            index: true
          },
          patient: {
            type: "object",
            properties: {
              cigarettesPerDay: {
                type: "number"
              }
            }
          }
        }
      }
    }
  }
};

describe("AggregationSearchQuery", () => {
  describe("#buildSearchQuery", () => {
    describe("when no filters were set", () => {
      let json = null;
      beforeEach(() => {
        const filters = {};
        const query = new AggregationSearchQuery(filters, schema, "person");
        json = query.toJSON();
      });

      it("should provide a single json response", () => {
        expect(json).not.toHaveProperty("core");
        expect(json).toHaveProperty("from");
        expect(json).toHaveProperty("size");
        expect(json).toHaveProperty("query");
      });
      it("should default size to 0", () => {
        expect(json).toHaveProperty("size", 0);
      });
      it("should have aggregations", () => {
        expect(json).toHaveProperty("aggs");
        expect(Object.keys(json.aggs).length).toEqual(13);
      });
      it("should request min and max for integers", () => {
        expect(json.aggs).toHaveProperty("min_points");
        expect(json.aggs.min_points).toHaveProperty("min");
        expect(json.aggs.min_points.min).toHaveProperty("field", "points");

        expect(json.aggs).toHaveProperty("max_points");
        expect(json.aggs.max_points).toHaveProperty("max");
        expect(json.aggs.max_points.max).toHaveProperty("field", "points");
      });
      it("should request min and max for numbers", () => {
        expect(json.aggs["min_metadata.patient.cigarettesPerDay"]).toBeDefined();
        expect(json.aggs["min_metadata.patient.cigarettesPerDay"]).toHaveProperty("min");
        expect(json.aggs["min_metadata.patient.cigarettesPerDay"].min).toHaveProperty(
          "field",
          "metadata.patient.cigarettesPerDay"
        );

        expect(json.aggs["max_metadata.patient.cigarettesPerDay"]).toBeDefined();
        expect(json.aggs["max_metadata.patient.cigarettesPerDay"]).toHaveProperty("max");
        expect(json.aggs["max_metadata.patient.cigarettesPerDay"].max).toHaveProperty(
          "field",
          "metadata.patient.cigarettesPerDay"
        );
      });
      it("should request min and max for dates", () => {
        expect(json.aggs).toHaveProperty("min_dob");
        expect(json.aggs.min_dob).toHaveProperty("min");
        expect(json.aggs.min_dob.min).toHaveProperty("field", "dob");

        expect(json.aggs).toHaveProperty("max_dob");
        expect(json.aggs.max_dob).toHaveProperty("max");
        expect(json.aggs.max_dob.max).toHaveProperty("field", "dob");
      });
      it("should request min and max for date-times", () => {
        expect(json.aggs).toHaveProperty("min_wokeUp");
        expect(json.aggs.min_wokeUp).toHaveProperty("min");
        expect(json.aggs.min_wokeUp.min).toHaveProperty("field", "wokeUp");

        expect(json.aggs).toHaveProperty("max_wokeUp");
        expect(json.aggs.max_wokeUp).toHaveProperty("max");
        expect(json.aggs.max_wokeUp.max).toHaveProperty("field", "wokeUp");
      });
      it("should set a default term size for enums", () => {
        expect(json.aggs.city.terms).toHaveProperty("size", 4);
      });
      it("should not set a default term size for non enums", () => {
        expect(json.aggs.isAdmin.terms).not.toHaveProperty("size");
      });
    });
    describe("when filters were set", () => {
      describe("when a single filter is set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { isAdmin: true };
          const query = new AggregationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });

        it("should provide multiple json responses", () => {
          expect(Object.keys(json).length).toEqual(2);
        });
        it("should provide a core response", () => {
          expect(Object.keys(json).includes("core"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("isAdmin"));
        });
        it("should apply filters to the core", () => {
          const core = json.core;
          expect(core).toHaveProperty("query");
          expect(core.query).toHaveProperty("bool");
          expect(core.query.bool).toHaveProperty("filter");
          expect(core.query.bool.filter).toHaveProperty("match");
          expect(core.query.bool.filter.match).toHaveProperty("isAdmin", true);
        });
        it("should apply remove the in-scope filter from its own filter query", () => {
          const isAdmin = json.isAdmin;
          expect(isAdmin).toHaveProperty("query");
          expect(isAdmin.query).toHaveProperty("bool", {});
        });
      });
      describe("when multiple filters are set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { isAdmin: true, meaningOfLife: true };
          const query = new AggregationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });

        it("should provide multiple json responses", () => {
          expect(Object.keys(json).length).toEqual(3);
        });
        it("should provide a core response", () => {
          expect(Object.keys(json).includes("core"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("isAdmin"));
        });
        it("should provide a response for filters", () => {
          expect(Object.keys(json).includes("meaningOfLife"));
        });
        it("should apply filters to the core", done => {
          const core = json.core;
          expect(core).toHaveProperty("query");
          expect(core.query).toHaveProperty("bool");
          expect(core.query.bool).toHaveProperty("filter");
          expect(core.query.bool.filter.length).toEqual(2);
          for (const filter of core.query.bool.filter) {
            expect(filter).toHaveProperty("match");
            for (const key of Object.keys(filter.match)) {
              switch (key) {
                case "isAdmin":
                  expect(filter.match[key]).toEqual(true);
                  break;
                case "meaningOfLife":
                  expect(filter.match[key]).toEqual(true);
                  break;
                default:
                  done.fail("Matches should be isAdmin and meaningOfLife");
              }
            }
          }
          done();
        });
        it("should apply remove the in-scope filter from its own filter query", () => {
          const isAdmin = json.isAdmin;
          expect(isAdmin).toHaveProperty("query");
          expect(isAdmin.query).toHaveProperty("bool");
          expect(isAdmin.query.bool).toHaveProperty("filter");
          expect(isAdmin.query.bool.filter).toHaveProperty("match");
          expect(isAdmin.query.bool.filter.match).not.toHaveProperty("isAdmin");
          expect(isAdmin.query.bool.filter.match).toHaveProperty("meaningOfLife", true);

          const meaningOfLife = json.meaningOfLife;
          expect(meaningOfLife).toHaveProperty("query");
          expect(meaningOfLife.query).toHaveProperty("bool");
          expect(meaningOfLife.query.bool).toHaveProperty("filter");
          expect(meaningOfLife.query.bool.filter).toHaveProperty("match");
          expect(meaningOfLife.query.bool.filter.match).not.toHaveProperty("meaningOfLife");
          expect(meaningOfLife.query.bool.filter.match).toHaveProperty("isAdmin", true);
        });
      });
    });
    describe("when pagination attributes are set", () => {
      describe("when per is set", () => {
        let json = null;
        beforeEach(() => {
          const filters = { page: 1, per: 10 };
          const query = new AggregationSearchQuery(filters, schema, "person");
          json = query.toJSON();
        });
        it("should override the default", () => {
          expect(json).toHaveProperty("size", 10);
        });
      });
    });
    describe("when options are set", () => {
      describe("when excluding an aggregation", () => {
        describe("when a single filter is set", () => {
          let json = null;
          beforeEach(() => {
            const filters = { isAdmin: true };
            const query = new AggregationSearchQuery(filters, schema, "person", {
              aggs: { city: { exclude: true } }
            });
            json = query.toJSON();
          });
          it("should not have the excluded aggregation", () => {
            const core = json.core;
            expect(core).toHaveProperty("aggs");
            expect(Object.keys(core.aggs).includes("city")).toEqual(false);
          });
        });
        describe("when multiple filters are set", () => {
          let json = null;
          beforeEach(() => {
            const filters = { isAdmin: true, meaningOfLife: true };
            const query = new AggregationSearchQuery(filters, schema, "person", {
              aggs: { city: { exclude: true } }
            });
            json = query.toJSON();
          });

          it("should not have the excluded aggregation", () => {
            const core = json.core;
            expect(core).toHaveProperty("aggs");
            expect(Object.keys(core.aggs).includes("city")).toEqual(false);
          });
        });
      });
      describe("when setting an aggregation size", () => {
        describe("when size is not set", () => {
          let json = null;
          beforeEach(() => {
            const query = new AggregationSearchQuery({}, schema, "person", {});
            json = query.toJSON();
          });
          it("should not set a term aggregation size", () => {
            expect(json).toHaveProperty("aggs");
            expect(json.aggs).toHaveProperty("city");
            expect(json.aggs.city).toHaveProperty("terms");
            expect(json.aggs.city.terms).toHaveProperty("field", "city");
          });
        });
        describe("when size is set to a positive number", () => {
          let json = null;
          beforeEach(() => {
            const query = new AggregationSearchQuery({}, schema, "person", {
              aggs: { city: { size: 200 } }
            });
            json = query.toJSON();
          });
          it("should not set a term aggregation size", () => {
            expect(json).toHaveProperty("aggs");
            expect(json.aggs).toHaveProperty("city");
            expect(json.aggs.city).toHaveProperty("terms");
            expect(json.aggs.city.terms).toHaveProperty("field", "city");
            expect(json.aggs.city.terms).toHaveProperty("size", 200);
          });
        });
        describe("when size is set to 0", () => {});
      });
    });
  });
});
