import MappingService from "../../../src/modules/elasticsearch/MappingService";

const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "string"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        gender: {
          title: "Gender",
          type: "string",
          enum: ["male", "female"],
          enumNames: ["Male", "Female"],
          enumNamesSearchAttribute: "genderDescription"
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        verified: {
          type: "boolean"
        }
      }
    },
    feed: {
      type: "array",
      items: {
        type: "object"
      }
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};

const dynamicSchema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      title: "User",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          title: "First Name",
          type: "string"
        },
        lastname: {
          title: "Last Name",
          type: "string"
        },
        email: {
          title: "Email",
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "string"
        },
        role: {
          title: "Role",
          type: "string",
          enum: ["administrator", "subscriber", "editor"],
          synonyms: ["adm, administrator", "sub, subscriber", "ed, editor"]
        },
        gender: {
          title: "Gender",
          type: "string",
          enum: ["male", "female"],
          enumNames: ["Male", "Female"],
          enumNamesSearchAttribute: "genderDescription",
          useSynonyms: true
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          title: "End Date",
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        verified: {
          type: "boolean"
        }
      }
    },
    feed: {
      type: "array",
      items: {
        type: "object"
      }
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        },
        buddyType: {
          type: "string",
          enum: ["friend", "family"]
        }
      },
      dependencies: {
        buddyType: {
          oneOf: [
            {
              properties: {
                buddyType: {
                  enum: ["family"]
                },
                relationship: {
                  type: "string"
                }
              }
            }
          ]
        }
      }
    },
    custodian: {
      type: "object",
      title: "Custodian",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          title: "Custodian Birthdate",
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      title: "Organisation",
      properties: {
        id: {
          type: "string"
        },
        name: {
          title: "Organisation Name",
          type: "string"
        }
      }
    }
  }
};

describe("MappingService", () => {
  describe("constructor", () => {
    it("should initialise attributes", () => {
      const service = new MappingService({ type: "profile" }, schema);
      expect(service.type).toBeTruthy();
      expect(service.schema).toBeTruthy();
      expect(service.explorer).toBeTruthy();
      expect(service.mappings).toBeTruthy();
    });
  });
  describe("getMapping", () => {
    describe("without dependencies", () => {
      it("should create a mapping", () => {
        const service = new MappingService({ type: "profile" }, schema);
        const mapping = service.getMapping();
        expect(mapping).toBeTruthy();
      });
      it("should create an ngrams all property", () => {
        const service = new MappingService({ type: "profile" }, schema);
        const mapping = service.getMapping();
        const profile = { properties: mapping.properties };
        expect(profile.properties.copied_ngrams).toBeTruthy();
        expect(profile.properties.copied_ngrams.type).toEqual("text");
        expect(profile.properties.copied_ngrams.analyzer).toEqual("ngrams_analyzer");
        expect(profile.properties.copied_ngrams.index).toEqual(true);
      });
      it("should create an all property", () => {
        const service = new MappingService({ type: "profile" }, schema);
        const mapping = service.getMapping();
        const profile = { properties: mapping.properties };
        expect(profile.properties.copied).toBeTruthy();
        expect(profile.properties.copied.type).toEqual("text");
        expect(profile.properties.copied.analyzer).toEqual("lowercase_analyzer");
        expect(profile.properties.copied.index).toEqual(true);
      });
      describe("with properties", () => {
        it("should capture property mappings", () => {
          const service = new MappingService({ type: "profile" }, schema);
          const mapping = service.getMapping();
          expect(mapping).toHaveProperty("mapping");
          expect(mapping).toHaveProperty("properties");

          const profileProperties = mapping.properties;
          expect(profileProperties).toBeTruthy();
          expect(profileProperties).toHaveProperty("user");
          expect(profileProperties.user).toHaveProperty("properties");
          expect(profileProperties).toHaveProperty("feed");
          expect(profileProperties).toHaveProperty("buddy");
          expect(profileProperties.buddy).toHaveProperty("properties");
          expect(profileProperties).toHaveProperty("custodian");
          expect(profileProperties.custodian).toHaveProperty("properties");
        });
      });
      describe("with nested properties", () => {
        let profile = null;

        beforeEach(done => {
          const service = new MappingService({ type: "profile" }, schema);
          const mapping = service.getMapping();
          expect(mapping).toHaveProperty("mapping");
          expect(mapping).toHaveProperty("properties");

          profile = { properties: mapping.properties };

          done();
        });
        it("should capture property mappings", () => {
          ["id", "name"].forEach(attribute => {
            expect(
              profile.properties.user.properties.organisation.properties[attribute].type
            ).toBeTruthy();
          });
          [
            "firstname",
            "lastname",
            "email",
            "phoneNumber",
            "age",
            "role",
            "startDate",
            "endDate",
            "bmi",
            "score",
            "verified",
            "gender"
          ].forEach(attribute => {
            expect(profile.properties.user.properties[attribute].type).toBeTruthy();
          });
          ["name", "birthDate", "dateOfDeath"].forEach(attribute => {
            expect(profile.properties.buddy.properties[attribute].type).toBeTruthy();
          });
          ["name", "emailAddress", "birthDate", "dateOfDeath"].forEach(attribute => {
            expect(profile.properties.custodian.properties[attribute].type).toBeTruthy();
          });
        });
        it("should setup mappings for strings", done => {
          [
            "profile.properties.user.properties.organisation.properties.id",
            "profile.properties.user.properties.organisation.properties.name",
            "profile.properties.user.properties.firstname",
            "profile.properties.user.properties.lastname",
            "profile.properties.user.properties.email",
            "profile.properties.user.properties.phoneNumber",
            "profile.properties.user.properties.bio",
            "profile.properties.buddy.properties.name",
            "profile.properties.custodian.properties.name",
            "profile.properties.custodian.properties.emailAddress"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("text");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for string enums", done => {
          ["profile.properties.user.properties.role"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("keyword");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for string enums with enumNames", done => {
          [
            "profile.properties.user.properties.gender",
            "profile.properties.user.properties.genderDescription"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("keyword");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for integers", done => {
          [
            "profile.properties.user.properties.bmi",
            "profile.properties.user.properties.score"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("double");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for booleans", done => {
          ["profile.properties.user.properties.verified"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("boolean");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for dates", done => {
          [
            "profile.properties.user.properties.startDate",
            "profile.properties.user.properties.endDate",
            "profile.properties.buddy.properties.birthDate",
            "profile.properties.buddy.properties.dateOfDeath",
            "profile.properties.custodian.properties.birthDate",
            "profile.properties.custodian.properties.dateOfDeath"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("date");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
      });
    });
    describe("with dependencies", () => {
      it("should create a mapping", () => {
        const service = new MappingService({ type: "profile" }, dynamicSchema);
        const mapping = service.getMapping();
        expect(mapping).toBeTruthy();
      });
      it("should create an ngrams all property", () => {
        const service = new MappingService({ type: "profile" }, dynamicSchema);
        const mapping = service.getMapping();
        const profile = { properties: mapping.properties };
        expect(profile.properties.copied_ngrams).toBeTruthy();
        expect(profile.properties.copied_ngrams.type).toEqual("text");
        expect(profile.properties.copied_ngrams.analyzer).toEqual("ngrams_analyzer");
        expect(profile.properties.copied_ngrams.index).toEqual(true);
      });
      it("should create an all property", () => {
        const service = new MappingService({ type: "profile" }, dynamicSchema);
        const mapping = service.getMapping();
        const profile = { properties: mapping.properties };
        expect(profile.properties.copied).toBeTruthy();
        expect(profile.properties.copied.type).toEqual("text");
        expect(profile.properties.copied.analyzer).toEqual("lowercase_analyzer");
        expect(profile.properties.copied.index).toEqual(true);
      });
      describe("with properties", () => {
        it("should capture property mappings", () => {
          const service = new MappingService({ type: "profile" }, dynamicSchema);
          const mapping = service.getMapping();
          expect(mapping).toHaveProperty("mapping");
          expect(mapping).toHaveProperty("properties");

          const profileProperties = mapping.properties;
          expect(profileProperties).toBeTruthy();
          expect(profileProperties).toHaveProperty("user");
          expect(profileProperties.user).toHaveProperty("properties");
          expect(profileProperties).toHaveProperty("feed");
          expect(profileProperties).toHaveProperty("buddy");
          expect(profileProperties.buddy).toHaveProperty("properties");
          expect(profileProperties).toHaveProperty("custodian");
          expect(profileProperties.custodian).toHaveProperty("properties");
        });
      });
      describe("with nested properties", () => {
        let profile = null;

        beforeEach(done => {
          const service = new MappingService({ type: "profile" }, dynamicSchema);
          const mapping = service.getMapping();
          expect(mapping).toHaveProperty("mapping");
          expect(mapping).toHaveProperty("properties");

          profile = { properties: mapping.properties };

          done();
        });
        it("should capture property mappings", () => {
          ["id", "name"].forEach(attribute => {
            expect(
              profile.properties.user.properties.organisation.properties[attribute].type
            ).toBeTruthy();
          });
          [
            "firstname",
            "lastname",
            "email",
            "phoneNumber",
            "age",
            "role",
            "startDate",
            "endDate",
            "bmi",
            "score",
            "verified",
            "gender"
          ].forEach(attribute => {
            expect(profile.properties.user.properties[attribute].type).toBeTruthy();
          });
          ["name", "birthDate", "dateOfDeath"].forEach(attribute => {
            expect(profile.properties.buddy.properties[attribute].type).toBeTruthy();
          });
          ["name", "emailAddress", "birthDate", "dateOfDeath"].forEach(attribute => {
            expect(profile.properties.custodian.properties[attribute].type).toBeTruthy();
          });
        });
        it("should setup mappings for strings", done => {
          [
            "profile.properties.user.properties.organisation.properties.id",
            "profile.properties.user.properties.organisation.properties.name",
            "profile.properties.user.properties.firstname",
            "profile.properties.user.properties.lastname",
            "profile.properties.user.properties.email",
            "profile.properties.user.properties.phoneNumber",
            "profile.properties.user.properties.bio",
            "profile.properties.buddy.properties.name",
            "profile.properties.custodian.properties.name",
            "profile.properties.custodian.properties.emailAddress"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("text");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for string enums", done => {
          ["profile.properties.user.properties.role"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("keyword");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for string enums with enumNames", done => {
          [
            "profile.properties.user.properties.gender",
            "profile.properties.user.properties.genderDescription"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("keyword");
            expect(attribute).toHaveProperty("copy_to");
            expect(attribute.copy_to).toEqual(["copied", "copied_ngrams"]);
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for integers", done => {
          [
            "profile.properties.user.properties.bmi",
            "profile.properties.user.properties.score"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("double");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for booleans", done => {
          ["profile.properties.user.properties.verified"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("boolean");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should setup mappings for dates", done => {
          [
            "profile.properties.user.properties.startDate",
            "profile.properties.user.properties.endDate",
            "profile.properties.buddy.properties.birthDate",
            "profile.properties.buddy.properties.dateOfDeath",
            "profile.properties.custodian.properties.birthDate",
            "profile.properties.custodian.properties.dateOfDeath"
          ].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("date");
            expect(attribute.index).toEqual(true);
          });
          done();
        });
        it("should add mappings for dynamic attributes", () => {
          ["profile.properties.buddy.properties.buddyType"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("keyword");
            expect(attribute.index).toEqual(true);
          });
          ["profile.properties.buddy.properties.relationship"].forEach(path => {
            const attribute = eval(path);

            expect(attribute.type).toEqual("text");
            expect(attribute.index).toEqual(true);
          });
        });
        it("should add mappings for array of objects", () => {
          expect(profile.properties.feed.type).toEqual("object");
        });
      });
    });
    describe("when using synonyms", () => {
      it("should create synonyms filter", () => {
        const service = new MappingService({ type: "profile" }, dynamicSchema);
        const mapping = service.getMapping();
        expect(mapping).toHaveProperty("mapping");
        expect(mapping).toHaveProperty("properties");

        expect(mapping.mapping).toHaveProperty("settings");
        const settings = mapping.mapping.settings;

        expect(settings).toHaveProperty("analysis");
        const analysis = settings.analysis;

        expect(analysis).toHaveProperty("filter");
        const filter = analysis.filter;

        expect(analysis).toHaveProperty("analyzer");
        const analyzer = analysis.analyzer;

        expect(filter).toHaveProperty("gender_syn");
        const genderSynonym = filter.gender_syn;

        expect(filter).toHaveProperty("role_syn");
        const roleSynonym = filter.role_syn;

        expect(analyzer).toHaveProperty("gender_text");
        const genderAnalyzer = analyzer.gender_text;

        expect(analyzer).toHaveProperty("role_text");
        const roleAnalyzer = analyzer.role_text;

        expect(genderSynonym.type).toEqual("synonym");
        expect(genderSynonym.synonyms).toEqual(
          expect.arrayContaining(["male, Male", "female, Female"])
        );

        expect(roleSynonym.type).toEqual("synonym");
        expect(roleSynonym.synonyms).toEqual(
          expect.arrayContaining(["adm, administrator", "sub, subscriber", "ed, editor"])
        );

        expect(genderAnalyzer.tokenizer).toEqual("standard");
        expect(genderAnalyzer.filter).toEqual(expect.arrayContaining(["gender_syn"]));

        expect(roleAnalyzer.tokenizer).toEqual("standard");
        expect(roleAnalyzer.filter).toEqual(expect.arrayContaining(["role_syn"]));
      });

      it("should create raw text fields", () => {
        const service = new MappingService({ type: "profile" }, dynamicSchema);
        const mapping = service.getMapping();
        expect(mapping).toHaveProperty("mapping");
        expect(mapping).toHaveProperty("properties");

        expect(mapping.properties).toHaveProperty("user");
        const user = mapping.properties.user;

        const gender = user.properties.gender;
        const role = user.properties.role;

        expect(gender).toHaveProperty("fields");
        const genderFileds = gender.fields;
        expect(genderFileds.raw.type).toEqual("text");
        expect(genderFileds.raw.analyzer).toEqual("gender_text");

        expect(role).toHaveProperty("fields");
        const roleFileds = role.fields;
        expect(roleFileds.raw.type).toEqual("text");
        expect(roleFileds.raw.analyzer).toEqual("role_text");
      });
    });
  });
});
