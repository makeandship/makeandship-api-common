import ChoicesJSONTransformer from "../../../../src/modules/elasticsearch/transformers/ChoicesJSONTransformer";
import results from "../../../fixtures/results";
import { redisService } from "../../../../src/modules/cache/services";

const titles = {
  "involved.type": {
    title: "Type involved",
    titles: ["Involved", "Type involved"]
  },
  "involved.role": {
    title: "Role involved",
    titles: ["Involved", "Role involved"]
  },
  "when.when": {
    title: "When",
    titles: ["When", "When"]
  },
  "slipsTripsFallsAndCollisions.fallsLastYear": {
    title: "Falls last year",
    titles: ["Slips,trips,Falls and collisions", "Falls last year"]
  }
};

beforeAll(async () => {
  redisService.set("involved.type:Patient", "Patient");
  redisService.set("results.predictor.mdr:true", "Yes");
});

afterAll(async () => {
  await redisService.disconnect();
});

describe("ChoicesJSONTransformer", () => {
  describe("#transform", () => {
    describe("when there is no jsonschema", () => {
      it("should transform aggregations into choices", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.ranges);

        const type = result["involved.type"];
        const role = result["involved.role"];
        const when = result["when.when"];
        const falls = result["slipsTripsFallsAndCollisions.fallsLastYear"];

        // keywords
        const patient = type.choices.filter(item => item.key === "Patient")[0];
        const employee = type.choices.filter(item => item.key === "Employee/Member of staff")[0];

        expect(patient.count).toEqual(6);
        expect(employee.count).toEqual(3);

        const victim = role.choices.filter(
          item => item.key === "Victim (Violence and Agression)"
        )[0];
        expect(victim.count).toEqual(9);

        // ranges - date
        expect(when.min).toEqual("2018-04-03T14:03:00.036Z");
        expect(when.max).toEqual("2018-05-03T12:09:57.322Z");

        // ranges - number
        expect(falls.min).toEqual(1);
        expect(falls.max).toEqual(8);

        done();
      });
      it("should transform choices with no buckets", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.rangesNoBuckets);

        const role = result["involved.role"];

        expect(role).toEqual({ choices: [] });

        done();
      });
      it("should transform boolean choices", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.ranges);

        const mdr = result["results.predictor.mdr"];
        const xdr = result["results.predictor.xdr"];

        expect(mdr.choices[0].key).toEqual(true);
        expect(xdr.choices[0].key).toEqual(false);

        done();
      });
      it("should enrich the choices with descriptions", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.ranges);

        const type = result["involved.type"];
        const mdr = result["results.predictor.mdr"];

        const patient = type.choices.filter(item => item.key === "Patient")[0];
        expect(patient.description).toEqual("Patient");
        expect(mdr.choices[0].description).toEqual("Yes");

        done();
      });
    });
    describe("when titles are provided", () => {
      it("should transform aggregations into choices with title", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.ranges, { titles });

        const type = result["involved.type"];
        const role = result["involved.role"];
        const when = result["when.when"];
        const falls = result["slipsTripsFallsAndCollisions.fallsLastYear"];

        expect(type.title).toEqual("Type involved");
        expect(role.title).toEqual("Role involved");
        expect(when.title).toEqual("When");
        expect(falls.title).toEqual("Falls last year");

        expect(type.titles).toEqual(["Involved", "Type involved"]);
        expect(role.titles).toEqual(["Involved", "Role involved"]);
        expect(when.titles).toEqual(["When", "When"]);
        expect(falls.titles).toEqual(["Slips,trips,Falls and collisions", "Falls last year"]);

        done();
      });
      it("should add title to choices", async done => {
        const transformer = new ChoicesJSONTransformer();
        const result = await transformer.transform(results.rangesNoBuckets, {
          titles
        });

        const type = result["involved.type"];
        const role = result["involved.role"];
        const when = result["when.when"];
        const falls = result["slipsTripsFallsAndCollisions.fallsLastYear"];

        expect(type.title).toEqual("Type involved");
        expect(role.title).toEqual("Role involved");
        expect(when.title).toEqual("When");
        expect(falls.title).toEqual("Falls last year");

        expect(type.titles).toEqual(["Involved", "Type involved"]);
        expect(role.titles).toEqual(["Involved", "Role involved"]);
        expect(when.titles).toEqual(["When", "When"]);
        expect(falls.titles).toEqual(["Slips,trips,Falls and collisions", "Falls last year"]);

        done();
      });
    });
  });
});
