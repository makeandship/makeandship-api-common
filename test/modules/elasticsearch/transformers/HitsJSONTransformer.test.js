import HitsJSONTransformer from "../../../../src/modules/elasticsearch/transformers/HitsJSONTransformer";
import results from "../../../fixtures/results";

describe("HitsJSONTransformer", () => {
  describe("#transform", () => {
    it("should transform results into hits", done => {
      const transformer = new HitsJSONTransformer();
      const result = transformer.transform(results.ranges);

      expect(result.length).toEqual(1);

      done();
    });
  });
});
