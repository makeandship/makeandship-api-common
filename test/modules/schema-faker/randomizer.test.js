import moment from "moment";
import nhsNumberValidator from "nhs-number-validator";
import Randomizer from "../../../src/modules/schema-faker/Randomizer";

let randomData;
const schema = {
  type: "object",
  properties: {
    user: {
      type: "object",
      properties: {
        organisation: {
          $ref: "#/definitions/organisation"
        },
        firstname: {
          type: "string"
        },
        lastname: {
          type: "string"
        },
        email: {
          type: "string",
          format: "email"
        },
        phoneNumber: {
          type: "string"
        },
        dateOfBirth: {
          type: "string",
          format: "date"
        },
        age: {
          type: "number"
        },
        bio: {
          type: "text"
        },
        nhsNumber: {
          type: "string",
          format: "nhsNumber"
        },
        role: {
          type: "string",
          enum: ["administrator", "subscriber", "editor"]
        },
        startDate: {
          type: "string",
          format: "date"
        },
        endDate: {
          type: "string",
          format: "date"
        },
        bmi: {
          type: "number"
        },
        score: {
          type: "number"
        },
        interests: {
          type: "array",
          items: {
            type: "string"
          }
        },
        references: {
          type: "array",
          items: {
            type: "string",
            format: "url"
          }
        },
        firstTravelAsAdult: {
          type: "number",
          minimum: 18
        },
        firstHolidayAsChild: {
          type: "number",
          maximum: 18
        },
        retirement: {
          type: "number",
          minimum: 65,
          maximum: 75
        },
        homeLongitude: {
          type: "number",
          minimum: -180,
          maximum: 180
        },
        homeLatitude: {
          type: "number",
          minimum: -90,
          maximum: 90
        }
      }
    },
    surgeries: {
      type: "array",
      items: {
        type: "object",
        properties: {
          code: {
            type: "string",
            enum: ["69896004", "426396005"],
            enumNames: ["Rheumatoid arthritis", "Cardiac chest pain"]
          },
          content: {
            type: "string"
          },
          file: {
            type: "string"
          },
          startDate: {
            type: "string",
            format: "date-time"
          },
          stopDate: {
            type: "string",
            format: "date-time"
          },
          registrationDate: {
            type: "string",
            format: "date"
          },
          validationDate: {
            type: "string",
            format: "date-time"
          },
          successPercentage: {
            type: "number"
          }
        }
      }
    },
    feed: {
      type: "object"
    },
    buddy: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date"
        },
        dateOfDeath: {
          type: "string",
          format: "date"
        }
      }
    },
    custodian: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        emailAddress: {
          type: "string"
        },
        birthDate: {
          type: "string",
          format: "date-time"
        },
        dateOfDeath: {
          type: "string",
          format: "date-time"
        }
      }
    }
  },
  definitions: {
    organisation: {
      type: "object",
      properties: {
        id: {
          type: "string"
        },
        name: {
          type: "string"
        }
      }
    }
  }
};

beforeAll(() => {
  const randomizer = new Randomizer(schema, {});
  randomData = randomizer.sample();
});

describe("Randomizer", () => {
  describe("#constructor", () => {
    describe("when providing invalid schema", () => {
      it("should throw an error when schema is undefined", done => {
        try {
          new Randomizer(null, {});
          done.fail("schema is undefined");
        } catch (e) {
          expect(e.message).toEqual("Please provide a valid schema");
          done();
        }
      });
    });
  });
  describe("#sample", () => {
    it("should generate names", () => {
      expect(randomData.user.firstname).toBeTruthy();
      expect(randomData.user.lastname).toBeTruthy();
    });
    it("should generate nested objects", () => {
      expect(randomData.user.organisation.name).toBeTruthy();
    });
    it("should generate valid emails", () => {
      expect(randomData.user.email).toEqual(expect.stringContaining("@"));
    });
    it("should generate arrays", () => {
      expect(randomData.surgeries.length).toBeGreaterThan(0);
    });
    it("should generate valid ages", () => {
      expect(randomData.user.age).toBeGreaterThan(1);
      expect(randomData.user.age).toBeLessThan(100);
    });
    it("should generate empty objects", () => {
      expect(randomData.feed).toBeTruthy();
      expect(randomData.feed).toEqual({});
    });
    it("should generate random text", () => {
      expect(randomData.user.bio).toBeTruthy();
    });
    it("should generate valid emums", () => {
      expect(["administrator", "subscriber", "editor"]).toEqual(
        expect.arrayContaining([randomData.user.role])
      );
    });
    it("should generate date-time in the past", () => {
      expect(randomData.surgeries[0].startDate).toBeTruthy();
    });
    it("should generate date-time in the future", () => {
      expect(randomData.surgeries[0].stopDate).toBeTruthy();
    });
    it("should generate simple date", () => {
      expect(randomData.surgeries[0].registrationDate).toBeTruthy();
    });
    it("should generate birth date", () => {
      expect(randomData.buddy.birthDate).toBeTruthy();
    });
    it("should generate date of death", () => {
      expect(randomData.buddy.dateOfDeath).toBeTruthy();
    });
    it("should generate date in the past", () => {
      expect(randomData.user.startDate).toBeTruthy();
    });
    it("should generate date in the future", () => {
      expect(randomData.user.endDate).toBeTruthy();
    });
    it("should generate birth date-time", () => {
      expect(randomData.custodian.birthDate).toBeTruthy();
    });
    it("should generate date-time of death", () => {
      expect(randomData.custodian.dateOfDeath).toBeTruthy();
    });
    it("should generate simple date-time", () => {
      expect(randomData.surgeries[0].validationDate).toBeTruthy();
    });
    it("should generate valid bmi", () => {
      expect(randomData.user.bmi).toBeGreaterThan(15);
      expect(randomData.user.bmi).toBeLessThan(33);
    });
    it("should generate valid percentage", () => {
      expect(randomData.surgeries[0].successPercentage).toBeGreaterThan(0);
      expect(randomData.surgeries[0].successPercentage).toBeLessThan(100);
    });
    it("should generate valid number", () => {
      expect(randomData.user.score).toBeTruthy();
    });
    it("should generate valid filename", () => {
      expect(randomData.surgeries[0].file).toEqual(expect.stringContaining("."));
    });
    it("should generate valid email address", () => {
      expect(randomData.custodian.emailAddress).toEqual(expect.stringContaining("@"));
    });
    it("should generate valid phone number", () => {
      expect(randomData.user.phoneNumber).toBeTruthy();
    });
    it("should generate valid nhs numbers", () => {
      const nhsNumber = randomData.user.nhsNumber;
      expect(nhsNumberValidator.validate(nhsNumber)).toBeTruthy();
    });
    it("should generate above a minimum", () => {
      expect(randomData.user.firstTravelAsAdult).toBeGreaterThan(17);
    });
    it("should generate below a maximum", () => {
      expect(randomData.user.firstHolidayAsChild).toBeLessThan(19);
    });
    it("should generate between a minimum and maximum", () => {
      expect(randomData.user.retirement).toBeGreaterThan(64);
      expect(randomData.user.retirement).toBeLessThan(76);
    });
    it("should generate a longitude", () => {
      expect(randomData.user.homeLongitude).toBeGreaterThan(-180);
      expect(randomData.user.homeLongitude).toBeLessThan(180);
    });
    it("should generate a latitude", () => {
      expect(randomData.user.homeLatitude).toBeGreaterThan(-90);
      expect(randomData.user.homeLatitude).toBeLessThan(90);
    });

    describe("when overriding defaults", () => {
      let overrideRandomData = null;
      const fortyYearOldDOB = moment()
        .subtract("40", "years")
        .subtract("10", "days")
        .format("YYYY-MM-DD");

      beforeEach(() => {
        const overrideSchema = JSON.parse(JSON.stringify(schema));
        const overrideRandomizer = new Randomizer(overrideSchema, {
          overrides: {
            user: {
              dateOfBirth: fortyYearOldDOB,
              age: function(schema, property, data) {
                if (data && data.user && data.user.dateOfBirth) {
                  const dob = data.user.dateOfBirth;
                  const age = moment().diff(moment(dob), "years");
                  return age;
                }
              },
              references: function() {
                return [
                  "https://www.makeandship.com/references/acme-reference",
                  "https://www.makeandship.com/references/nhs-reference"
                ];
              },
              interests: function(schema, property, data, value) {
                const updated = value.map(value => {
                  return value + " - updated";
                });
                return updated;
              }
            },
            surgeries: {
              code: "69896004",
              content: "Knee surgery"
            }
          }
        });
        overrideRandomData = overrideRandomizer.sample();
      });
      afterEach(() => {
        overrideRandomData = null;
      });
      it("should apply override values", () => {
        expect(overrideRandomData).toHaveProperty("user");
        expect(overrideRandomData.user).toHaveProperty("dateOfBirth", fortyYearOldDOB);
      });
      it("should apply override values within arrays", () => {
        expect(overrideRandomData).toHaveProperty("surgeries");
        const surgeries = overrideRandomData.surgeries;
        surgeries.forEach(surgery => {
          expect(surgery).toHaveProperty("code", "69896004");
        });
      });
      it("should apply an override functions", () => {
        expect(overrideRandomData).toHaveProperty("user");
        expect(overrideRandomData.user).toHaveProperty("age", 40);
      });
      it("should apply an override for the arrays", () => {
        expect(overrideRandomData).toHaveProperty("surgeries");
        const surgeries = overrideRandomData.surgeries;
        surgeries.forEach(surgery => {
          expect(surgery).toHaveProperty("content");
          expect(surgery.content).toEqual("Knee surgery");
        });
      });
      it("should apply an override for string arrays", () => {
        expect(overrideRandomData).toHaveProperty("user");
        const user = overrideRandomData.user;
        expect(user).toHaveProperty("references", [
          "https://www.makeandship.com/references/acme-reference",
          "https://www.makeandship.com/references/nhs-reference"
        ]);
      });
      it("should apply an override that updates the preset value", () => {
        expect(overrideRandomData).toHaveProperty("user");
        const user = overrideRandomData.user;
        expect(user).toHaveProperty("interests");
        const interests = user.interests;
        interests.forEach(interest => {
          expect(interest.endsWith(" - updated"));
        });
      });
    });
  });
});
