import nhsNumberValidator from "nhs-number-validator";
import { getRandomNHSNumber } from "../../../src/modules/schema-faker/utils";

describe("utils", () => {
  describe("#getRandomNHSNumber", () => {
    it("should generate valid NHS numbers", () => {
      let count = 100;
      while (count > 0) {
        const nhsNumber = getRandomNHSNumber();
        const valid = nhsNumberValidator.validate(nhsNumber);
        expect(valid).toEqual(true);
        count--;
      }
    });
  });
});
