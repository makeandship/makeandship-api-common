import DmdResultJSONTransformer from "../../../../src/modules/dmd/transformers/DmdResultJSONTransformer";
import dmd from "../../../fixtures/dmd.js";

describe("DmdResultJSONTransformer", () => {
  describe("#transform", () => {
    it("should transform vtms", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.vtm);

      expect(results.length).toEqual(1);
      const vtm = results[0];

      expect(vtm.previousVtmId).toEqual("10042111000001101");
      expect(vtm.vtmIdDate).toEqual("2006-11-01");
      expect(vtm.vtmId).toEqual("411106009");
      expect(vtm.name).toEqual("Fluticasone + Salmeterol");

      done();
    });
    it("should transform vmps", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.vmp);

      expect(results.length).toEqual(1);
      const vmp = results[0];

      expect(vmp.vmpId).toEqual("320275008");
      expect(vmp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
      );
      expect(vmp.nameChangeCode).toEqual("0004");
      expect(vmp.dfIndicatorCode).toEqual("1");
      expect(vmp.unitDoseCode).toEqual("3317411000001100");
      expect(vmp.basisOfNamePrev.code).toEqual("0007");
      expect(vmp.basisOfNamePrev.description).toEqual("Other");
      expect(vmp.dfIndicator.code).toEqual("1");
      expect(vmp.dfIndicator.description).toEqual("Discrete");
      expect(vmp.udfsUnit.code).toEqual("3317411000001100");
      expect(vmp.udfsUnit.description).toEqual("dose");

      done();
    });
    it("should transform vmpps", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.vmpp);

      expect(results.length).toEqual(1);
      const vmpp = results[0];

      expect(vmpp.vmppId).toEqual("1282011000001105");
      expect(vmpp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free 120 dose"
      );
      expect(vmpp.vmpId).toEqual("320275008");
      expect(vmpp.quantityValue).toEqual(120);
      expect(vmpp.quantityUnitCode).toEqual("3317411000001100");

      done();
    });
    it("should transform amps", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.amp);

      expect(results.length).toEqual(1);
      const amp = results[0];

      expect(amp.supplierCode).toEqual("2074301000001102");
      expect(amp.name).toEqual("AirFluSal 25micrograms/dose / 125micrograms/dose inhaler");
      expect(amp.description).toEqual(
        "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd)"
      );
      expect(amp.vmpId).toEqual("320275008");
      expect(amp.availabilityRestrictionCode).toEqual("0001");
      expect(amp.ampId).toEqual("34215311000001107");
      expect(amp.licensingAuthority.code).toEqual("0001");
      expect(amp.licensingAuthority.description).toEqual("Medicines - MHRA/EMA");
      expect(amp.availabilityRestriction.code).toEqual("0001");
      expect(amp.availabilityRestriction.description).toEqual("None");
      expect(amp.supplier.code).toEqual("2074301000001102");
      expect(amp.supplier.description).toEqual("Sandoz Ltd");

      done();
    });
    it("should transform ampps", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.ampp);

      expect(results.length).toEqual(1);
      const ampp = results[0];

      expect(ampp.name).toEqual(
        "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd) 120 dose"
      );
      expect(ampp.vmppId).toEqual("1282011000001105");
      expect(ampp.priceInfo.priceDate).toEqual("2017-10-11");
      expect(ampp.priceInfo.previousPrice).toEqual("2350");
      expect(ampp.priceInfo.price).toEqual("1850");
      expect(ampp.priceInfo.amppId).toEqual("34215411000001100");
      expect(ampp.priceInfo.priceBasisCode).toEqual("0001");
      expect(ampp.legalCategoryCode).toEqual("0003");
      expect(ampp.amppId).toEqual("34215411000001100");
      expect(ampp.ampId).toEqual("34215311000001107");
      expect(ampp.legalCategory.description).toEqual("POM");
      expect(ampp.legalCategory.code).toEqual("0003");
      expect(ampp.reimbInfo.pxchrgs).toEqual("1");
      expect(ampp.reimbInfo.speccontcd).toEqual("0001");
      expect(ampp.reimbInfo.dispfees).toEqual("1");
      expect(ampp.reimbInfo.amppId).toEqual("34215411000001100");

      done();
    });
    it("should transform ampps list", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.amppAll);

      expect(results.length).toEqual(9);
      const resultsById = {};
      const resultsByName = {};
      results.forEach(result => {
        resultsById[result.amppId.toString()] = result;
        resultsByName[result.name] = result;
      });

      expect(Object.keys(resultsById).length).toEqual(9);
      expect(resultsById["36005011000001107"].name).toEqual(
        "Amgevita 40mg/0.8ml solution for injection pre-filled syringes (Amgen Ltd) 2 pre-filled disposable injection"
      );

      expect(Object.keys(resultsByName).length).toEqual(9);
      expect(
        resultsByName[
          "Amgevita 40mg/0.8ml solution for injection pre-filled syringes (Amgen Ltd) 2 pre-filled disposable injection"
        ].amppId
      ).toEqual("36005011000001107");

      done();
    });
    it("should not include regimens", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.ampp);

      expect(results.length).toEqual(1);
      const result = results.pop();
      expect(result).not.toHaveProperty("regimens");

      done();
    });
    it("should transform nested objects", done => {
      const transformer = new DmdResultJSONTransformer();
      const results = transformer.transform(dmd.ampp);

      expect(results.length).toEqual(1);
      const ampp = results[0];

      expect(ampp.vmpp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free 120 dose"
      );
      expect(ampp.vmpp.vmppId).toEqual("1282011000001105");
      expect(ampp.vmpp.quantityValue).toEqual(120);
      expect(ampp.amp.supplierCode).toEqual("2074301000001102");
      expect(ampp.amp.ampId).toEqual("34215311000001107");
      expect(ampp.amp.name).toEqual("AirFluSal 25micrograms/dose / 125micrograms/dose inhaler");
      expect(ampp.vtm.name).toEqual("Fluticasone + Salmeterol");
      expect(ampp.legalCategoryCode).toEqual("0003");
      expect(ampp.vmp.vmpId).toEqual("320275008");
      expect(ampp.vmp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
      );

      done();
    });

    describe("when regimens are available", () => {
      it("should add regimens to results", done => {
        const options = {
          regimensByAmppId: {
            "34215411000001100": [
              {
                dose: "1 injection",
                frequency: "Once a month"
              }
            ]
          }
        };
        const transformer = new DmdResultJSONTransformer();
        const results = transformer.transform(dmd.ampp, options);

        expect(results.length).toEqual(1);
        const result = results.pop();

        expect(result).toHaveProperty("regimens");
        expect(result.regimens.length).toEqual(1);

        const regimen = result.regimens.pop();
        expect(regimen).toHaveProperty("dose", "1 injection");
        expect(regimen).toHaveProperty("frequency", "Once a month");

        done();
      });
    });

    describe("when transforming suggest results", () => {
      it("should provide the correct shortName and conditionCode", done => {
        const transformer = new DmdResultJSONTransformer();
        const results = transformer.transform(dmd.suggest);

        expect(results.length).toEqual(1);
        expect(results[0].shortName).toEqual("Humira 40mg/0.8ml");
        expect(results[0].conditionCode).toEqual("69896004");

        done();
      });
    });
  });
});
