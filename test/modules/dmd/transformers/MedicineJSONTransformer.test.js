import MedicineJSONTransformer from "../../../../src/modules/dmd/transformers/MedicineJSONTransformer";

import dmd from "../../../fixtures/dmd.js";

describe("MedicineJSONTransformer", () => {
  describe("#transform", () => {
    it("should transform a vtm", done => {
      const transformer = new MedicineJSONTransformer();
      const vtm = transformer.transform(dmd.vtm.hits.hits[0]);

      expect(vtm.previousVtmId).toEqual("10042111000001101");
      expect(vtm.vtmIdDate).toEqual("2006-11-01");
      expect(vtm.vtmId).toEqual("411106009");
      expect(vtm.name).toEqual("Fluticasone + Salmeterol");

      done();
    });
    it("should transform a vmp", done => {
      const transformer = new MedicineJSONTransformer();
      const vmp = transformer.transform(dmd.vmp.hits.hits[0]);

      expect(vmp.vmpId).toEqual("320275008");
      expect(vmp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
      );
      expect(vmp.nameChangeCode).toEqual("0004");
      expect(vmp.dfIndicatorCode).toEqual("1");
      expect(vmp.unitDoseCode).toEqual("3317411000001100");
      expect(vmp.basisOfNamePrev.code).toEqual("0007");
      expect(vmp.basisOfNamePrev.description).toEqual("Other");
      expect(vmp.dfIndicator.code).toEqual("1");
      expect(vmp.dfIndicator.description).toEqual("Discrete");
      expect(vmp.udfsUnit.code).toEqual("3317411000001100");
      expect(vmp.udfsUnit.description).toEqual("dose");

      done();
    });
    it("should transform a vmpp", done => {
      const transformer = new MedicineJSONTransformer();
      const vmpp = transformer.transform(dmd.vmpp.hits.hits[0]);

      expect(vmpp.vmppId).toEqual("1282011000001105");
      expect(vmpp.name).toEqual(
        "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free 120 dose"
      );
      expect(vmpp.vmpId).toEqual("320275008");
      expect(vmpp.quantityValue).toEqual(120);
      expect(vmpp.quantityUnitCode).toEqual("3317411000001100");

      done();
    });
    it("should transform an amp", done => {
      const transformer = new MedicineJSONTransformer();
      const amp = transformer.transform(dmd.amp.hits.hits[0]);

      expect(amp.supplierCode).toEqual("2074301000001102");
      expect(amp.name).toEqual("AirFluSal 25micrograms/dose / 125micrograms/dose inhaler");
      expect(amp.description).toEqual(
        "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd)"
      );
      expect(amp.vmpId).toEqual("320275008");
      expect(amp.availabilityRestrictionCode).toEqual("0001");
      expect(amp.ampId).toEqual("34215311000001107");
      expect(amp.licensingAuthority.code).toEqual("0001");
      expect(amp.licensingAuthority.description).toEqual("Medicines - MHRA/EMA");
      expect(amp.availabilityRestriction.code).toEqual("0001");
      expect(amp.availabilityRestriction.description).toEqual("None");
      expect(amp.supplier.code).toEqual("2074301000001102");
      expect(amp.supplier.description).toEqual("Sandoz Ltd");

      done();
    });
    it("should transform an ampp", done => {
      const transformer = new MedicineJSONTransformer();
      const ampp = transformer.transform(dmd.ampp.hits.hits[0]);

      expect(ampp.name).toEqual(
        "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd) 120 dose"
      );
      expect(ampp.vmppId).toEqual("1282011000001105");
      expect(ampp.priceInfo.priceDate).toEqual("2017-10-11");
      expect(ampp.priceInfo.previousPrice).toEqual("2350");
      expect(ampp.priceInfo.price).toEqual("1850");
      expect(ampp.priceInfo.amppId).toEqual("34215411000001100");
      expect(ampp.priceInfo.priceBasisCode).toEqual("0001");
      expect(ampp.legalCategoryCode).toEqual("0003");
      expect(ampp.amppId).toEqual("34215411000001100");
      expect(ampp.ampId).toEqual("34215311000001107");
      expect(ampp.legalCategory.description).toEqual("POM");
      expect(ampp.legalCategory.code).toEqual("0003");
      expect(ampp.reimbInfo.pxchrgs).toEqual("1");
      expect(ampp.reimbInfo.speccontcd).toEqual("0001");
      expect(ampp.reimbInfo.dispfees).toEqual("1");
      expect(ampp.reimbInfo.amppId).toEqual("34215411000001100");

      done();
    });
  });
});
