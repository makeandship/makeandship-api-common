import utils from "../../../src/modules/dmd/utils";

const query = {
  vmpId: ["29921611000001109", "2992161100000115"],
  name: "EnergieShake",
  ampId: "123213213",
  vtmId: "123213213",
  abc: "lorem"
};

describe("dmd/utils", () => {
  describe("#when the keys exist in the mapping", () => {
    it("should translate them", () => {
      const translated = utils.translateQuery(query);
      expect(translated.vpid).toEqual(["29921611000001109", "2992161100000115"]);
      expect(translated.apid).toEqual("123213213");
      expect(translated.vtmid).toEqual("123213213");
      expect(translated.vmpId).toBeUndefined();
      expect(translated.ampId).toBeUndefined();
      expect(translated.vtmId).toBeUndefined();
    });
  });
  describe("#when the keys dont exist in the mapping", () => {
    it("should keep the originals", () => {
      const translated = utils.translateQuery(query);
      expect(translated.name).toEqual("EnergieShake");
      expect(translated.abc).toEqual("lorem");
    });
  });
});
