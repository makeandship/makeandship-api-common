export default {
  vtm: {
    took: 5,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 1,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "411106009",
          _score: 0,
          _source: {
            vtmidprev: "10042111000001101",
            vtmiddt: "2006-11-01",
            type: "VTM",
            vtmid: "411106009",
            nm: "Fluticasone + Salmeterol"
          }
        }
      ]
    }
  },
  vmp: {
    took: 5,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 1,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "320275008",
          _score: 0,
          _source: {
            controlDrugInfo: {
              controlDrugCategory: {
                code: "0000",
                description: "No Controlled Drug Status"
              },
              catcd: "0000"
            },
            nmchangecd: "0004",
            udfsuomcd: "3317411000001100",
            dfindcd: "1",
            presstatcd: "0007",
            basisOfNamePrev: {
              code: "0007",
              description: "Other"
            },
            type: "VMP",
            nonAvailability: {
              code: null,
              description: null
            },
            virtualProductIngredient: {
              strengthUnitOfMeasure: {
                code: "258685003",
                description: "microgram"
              },
              strntdnmtrval: 1,
              basisOfStrength: {
                code: "0001",
                description: "Based on Ingredient Substance"
              },
              strntdnmtruomcd: "3317411000001100",
              value: 125
            },
            nameChangeReason: {
              code: "0004",
              description: "Other"
            },
            basisOfName: {
              code: "0007",
              description: "Other"
            },
            nmprev:
              "Fluticasone 125micrograms/actuation / Salmeterol 25micrograms/actuation inhaler CFC free",
            ontDrugForm: {
              formcd: "0004",
              ontDrugForm: {
                code: "0004",
                description: "pressurizedinhalation.inhalation"
              }
            },
            UdfsUnit: {
              code: "3317411000001100",
              description: "dose"
            },
            vpid: "320275008",
            unitdoseuomcd: "3317411000001100",
            cfcf: "0001",
            combinationProd: {
              code: null,
              description: null
            },
            basisprevcd: "0007",
            udfs: 1,
            vtm: {
              vtmidprev: "10042111000001101",
              vtmiddt: "2006-11-01",
              type: "VTM",
              vtmid: "411106009",
              nm: "Fluticasone + Salmeterol"
            },
            basiscd: "0007",
            drugFormType: {
              formcd: "385203008",
              drugForm: {
                code: "385203008",
                description: "Pressurised inhalation"
              }
            },
            dfIndicator: {
              code: "1",
              description: "Discrete"
            },
            nmdt: "2006-08-15",
            drugRoute: {
              drugRoute: {
                code: "18679011000001101",
                description: "Inhalation"
              },
              routecd: "18679011000001101"
            },
            vtmid: "411106009",
            nm: "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free",
            doseUnitOfmeasure: {
              code: "3317411000001100",
              description: "dose"
            }
          }
        }
      ]
    }
  },
  vmpp: {
    took: 6,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 1,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "1282011000001105",
          _score: 0,
          _source: {
            vppid: "1282011000001105",
            vpid: "320275008",
            qtyval: 120.0,
            combinationPack: {
              code: null,
              description: null
            },
            qtyuomcd: "3317411000001100",
            vmp: {
              vpid: "320275008",
              nmchangecd: "0004",
              udfsuomcd: "3317411000001100",
              dfindcd: "1",
              unitdoseuomcd: "3317411000001100",
              cfcf: "0001",
              basisprevcd: "0007",
              presstatcd: "0007",
              udfs: 1.0,
              basiscd: "0007",
              nmprev:
                "Fluticasone 125micrograms/actuation / Salmeterol 25micrograms/actuation inhaler CFC free",
              nmdt: "2006-08-15",
              vtmid: "411106009",
              nm: "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
            },
            quantityUnit: {
              code: null,
              description: null
            },
            type: "VMPP",
            vtm: {
              vtmidprev: "10042111000001101",
              vtmiddt: "2006-11-01",
              type: "VTM",
              vtmid: "411106009",
              nm: "Fluticasone + Salmeterol"
            },
            nm:
              "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free 120 dose"
          }
        }
      ]
    }
  },
  amp: {
    took: 4,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 1,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "34215311000001107",
          _score: 0,
          _source: {
            suppcd: "2074301000001102",
            licensingAuthorityPrev: {
              code: null,
              description: null
            },
            vpid: "320275008",
            availrestrictcd: "0001",
            flavour: {
              code: null,
              description: null
            },
            apid: "34215311000001107",
            combinationProd: {
              code: null,
              description: null
            },
            licensingAuthority: {
              code: "0001",
              description: "Medicines - MHRA/EMA"
            },
            type: "AMP",
            vtm: {
              vtmidprev: "10042111000001101",
              vtmiddt: "2006-11-01",
              type: "VTM",
              vtmid: "411106009",
              nm: "Fluticasone + Salmeterol"
            },
            availabilityRestriction: {
              code: "0001",
              description: "None"
            },
            supplier: {
              code: "2074301000001102",
              description: "Sandoz Ltd"
            },
            vmp: {
              vpid: "320275008",
              nmchangecd: "0004",
              udfsuomcd: "3317411000001100",
              dfindcd: "1",
              unitdoseuomcd: "3317411000001100",
              cfcf: "0001",
              basisprevcd: "0007",
              presstatcd: "0007",
              udfs: 1.0,
              basiscd: "0007",
              nmprev:
                "Fluticasone 125micrograms/actuation / Salmeterol 25micrograms/actuation inhaler CFC free",
              nmdt: "2006-08-15",
              vtmid: "411106009",
              nm: "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
            },
            nm: "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler",
            desc: "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd)",
            licauthcd: "0001",
            licensingAuthorityChange: {
              code: null,
              description: null
            }
          }
        }
      ]
    }
  },
  ampp: {
    took: 4,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 1,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "34215411000001100",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "1282011000001105",
              vpid: "320275008",
              qtyval: 120.0,
              qtyuomcd: "3317411000001100",
              nm:
                "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free 120 dose"
            },
            vppid: "1282011000001105",
            priceinfo: {
              priceprev: "2350",
              price: "1850",
              appid: "34215411000001100",
              pricedt: "2017-10-11",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "2074301000001102",
              vpid: "320275008",
              availrestrictcd: "0001",
              apid: "34215311000001107",
              nm: "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler",
              desc: "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd)",
              licauthcd: "0001"
            },
            apid: "34215311000001107",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: null,
            reimbinfo: {
              pxchrgs: "1",
              speccontcd: "0001",
              dispfees: "1",
              appid: "34215411000001100"
            },
            vtm: {
              vtmidprev: "10042111000001101",
              vtmiddt: "2006-11-01",
              type: "VTM",
              vtmid: "411106009",
              nm: "Fluticasone + Salmeterol"
            },
            appid: "34215411000001100",
            vmp: {
              vpid: "320275008",
              nmchangecd: "0004",
              udfsuomcd: "3317411000001100",
              dfindcd: "1",
              unitdoseuomcd: "3317411000001100",
              cfcf: "0001",
              basisprevcd: "0007",
              presstatcd: "0007",
              udfs: 1.0,
              basiscd: "0007",
              nmprev:
                "Fluticasone 125micrograms/actuation / Salmeterol 25micrograms/actuation inhaler CFC free",
              nmdt: "2006-08-15",
              vtmid: "411106009",
              nm: "Fluticasone 125micrograms/dose / Salmeterol 25micrograms/dose inhaler CFC free"
            },
            nm: "AirFluSal 25micrograms/dose / 125micrograms/dose inhaler (Sandoz Ltd) 120 dose"
          }
        }
      ]
    }
  },
  amppAll: {
    took: 4,
    timed_out: false,
    _shards: {
      total: 1,
      successful: 1,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: 9,
      max_score: 0,
      hits: [
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36005011000001107",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "5007311000001102",
              vpid: "35894411000001100",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes 2 pre-filled disposable injection"
            },
            vppid: "5007311000001102",
            priceinfo: {
              price: "63360",
              appid: "36005011000001107",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "3303701000001100",
              vpid: "35894411000001100",
              abbrevnm: "Amgevita 40mg/0.8ml inj pre-filled syringes",
              availrestrictcd: "0001",
              apid: "36004911000001107",
              ema: "0001",
              nm: "Amgevita 40mg/0.8ml solution for injection pre-filled syringes",
              desc: "Amgevita 40mg/0.8ml solution for injection pre-filled syringes (Amgen Ltd)",
              licauthcd: "0001"
            },
            apid: "36004911000001107",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36005011000001107",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36005011000001107",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36005011000001107",
            vmp: {
              vpid: "35894411000001100",
              nmchangecd: "0004",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled syringes",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              basisprevcd: "0001",
              presstatcd: "0001",
              udfs: 0.8,
              vpidt: "2018-10-17",
              nonavaildt: "2018-10-25",
              vpidprev: "408154002",
              basiscd: "0001",
              vpiddt: {
                month: 10,
                hour: -2147483648,
                year: 2018,
                timezone: -2147483648,
                day: 17,
                minute: -2147483648,
                second: -2147483648
              },
              nmprev: "Adalimumab 50mg/ml injection 0.8ml pre-filled syringes",
              nmdt: "2004-04-29",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes",
              nonavailcd: "0000"
            },
            nm:
              "Amgevita 40mg/0.8ml solution for injection pre-filled syringes (Amgen Ltd) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36005211000001102",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "11187811000001103",
              vpid: "11236911000001103",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices 2 pre-filled disposable injection"
            },
            vppid: "11187811000001103",
            priceinfo: {
              price: "63360",
              appid: "36005211000001102",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "3303701000001100",
              vpid: "11236911000001103",
              availrestrictcd: "0001",
              apid: "36005111000001108",
              ema: "0001",
              nm: "Amgevita 40mg/0.8ml solution for injection pre-filled pen",
              desc: "Amgevita 40mg/0.8ml solution for injection pre-filled pen (Amgen Ltd)",
              licauthcd: "0001"
            },
            apid: "36005111000001108",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36005211000001102",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36005211000001102",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36005211000001102",
            vmp: {
              basiscd: "0001",
              vpid: "11236911000001103",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled disposable devices",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              presstatcd: "0001",
              udfs: 0.8,
              nonavaildt: "2018-10-25",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices",
              nonavailcd: "0000"
            },
            nm:
              "Amgevita 40mg/0.8ml solution for injection pre-filled pen (Amgen Ltd) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36084211000001102",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "11187811000001103",
              vpid: "11236911000001103",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices 2 pre-filled disposable injection"
            },
            vppid: "11187811000001103",
            priceinfo: {
              price: "61625",
              appid: "36084211000001102",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "144301000001108",
              vpid: "11236911000001103",
              availrestrictcd: "0001",
              apid: "36084111000001108",
              ema: "0001",
              nm: "Hulio 40mg/0.8ml solution for injection pre-filled pen",
              desc: "Hulio 40mg/0.8ml solution for injection pre-filled pen (Mylan)",
              licauthcd: "0001"
            },
            apid: "36084111000001108",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36084211000001102",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36084211000001102",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36084211000001102",
            vmp: {
              basiscd: "0001",
              vpid: "11236911000001103",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled disposable devices",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              presstatcd: "0001",
              udfs: 0.8,
              nonavaildt: "2018-10-25",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices",
              nonavailcd: "0000"
            },
            nm:
              "Hulio 40mg/0.8ml solution for injection pre-filled pen (Mylan) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36085211000001101",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "5007311000001102",
              vpid: "35894411000001100",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes 2 pre-filled disposable injection"
            },
            vppid: "5007311000001102",
            priceinfo: {
              price: "61625",
              appid: "36085211000001101",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "144301000001108",
              vpid: "35894411000001100",
              availrestrictcd: "0001",
              apid: "36085111000001107",
              ema: "0001",
              nm: "Hulio 40mg/0.8ml solution for injection pre-filled syringes",
              desc: "Hulio 40mg/0.8ml solution for injection pre-filled syringes (Mylan)",
              licauthcd: "0001"
            },
            apid: "36085111000001107",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36085211000001101",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36085211000001101",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36085211000001101",
            vmp: {
              vpid: "35894411000001100",
              nmchangecd: "0004",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled syringes",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              basisprevcd: "0001",
              presstatcd: "0001",
              udfs: 0.8,
              vpidt: "2018-10-17",
              nonavaildt: "2018-10-25",
              vpidprev: "408154002",
              basiscd: "0001",
              vpiddt: {
                month: 10,
                hour: -2147483648,
                year: 2018,
                timezone: -2147483648,
                day: 17,
                minute: -2147483648,
                second: -2147483648
              },
              nmprev: "Adalimumab 50mg/ml injection 0.8ml pre-filled syringes",
              nmdt: "2004-04-29",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes",
              nonavailcd: "0000"
            },
            nm:
              "Hulio 40mg/0.8ml solution for injection pre-filled syringes (Mylan) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36435711000001107",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "36435511000001102",
              vpid: "36441711000001106",
              qtyval: 1.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 20mg/0.4ml solution for injection pre-filled syringes 1 pre-filled disposable injection"
            },
            vppid: "36435511000001102",
            priceinfo: {
              price: "15840",
              appid: "36435711000001107",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "3303701000001100",
              vpid: "36441711000001106",
              abbrevnm: "Amgevita 20mg/0.4ml inj pre-filled syringes",
              availrestrictcd: "0001",
              apid: "36435611000001103",
              ema: "0001",
              nm: "Amgevita 20mg/0.4ml solution for injection pre-filled syringes",
              desc: "Amgevita 20mg/0.4ml solution for injection pre-filled syringes (Amgen Ltd)",
              licauthcd: "0001"
            },
            apid: "36435611000001103",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36435711000001107",
              padm: "0001"
            },
            reimbinfo: {
              pxchrgs: "1",
              speccontcd: "0001",
              dispfees: "1",
              appid: "36435711000001107",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36435711000001107",
            vmp: {
              basiscd: "0001",
              vpid: "36441711000001106",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 20mg/0.4ml inj pre-filled syringes",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              presstatcd: "0001",
              udfs: 0.4,
              vtmid: "398728003",
              nm: "Adalimumab 20mg/0.4ml solution for injection pre-filled syringes"
            },
            nm:
              "Amgevita 20mg/0.4ml solution for injection pre-filled syringes (Amgen Ltd) 1 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36442111000001100",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "11187811000001103",
              vpid: "11236911000001103",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices 2 pre-filled disposable injection"
            },
            vppid: "11187811000001103",
            priceinfo: {
              price: "63385",
              appid: "36442111000001100",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "2946001000001103",
              vpid: "11236911000001103",
              availrestrictcd: "0001",
              apid: "36442011000001101",
              ema: "0001",
              nm: "Imraldi 40mg/0.8ml solution for injection pre-filled pen",
              desc: "Imraldi 40mg/0.8ml solution for injection pre-filled pen (Biogen Idec Ltd)",
              licauthcd: "0001"
            },
            apid: "36442011000001101",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36442111000001100",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36442111000001100",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36442111000001100",
            vmp: {
              basiscd: "0001",
              vpid: "11236911000001103",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled disposable devices",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              presstatcd: "0001",
              udfs: 0.8,
              nonavaildt: "2018-10-25",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices",
              nonavailcd: "0000"
            },
            nm:
              "Imraldi 40mg/0.8ml solution for injection pre-filled pen (Biogen Idec Ltd) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36460311000001105",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "5007311000001102",
              vpid: "35894411000001100",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes 2 pre-filled disposable injection"
            },
            vppid: "5007311000001102",
            priceinfo: {
              price: "63385",
              appid: "36460311000001105",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "2946001000001103",
              vpid: "35894411000001100",
              abbrevnm: "Imraldi 40mg/0.8ml inj pre-filled syringes",
              availrestrictcd: "0001",
              apid: "36460211000001102",
              ema: "0001",
              nm: "Imraldi 40mg/0.8ml solution for injection pre-filled syringes",
              desc:
                "Imraldi 40mg/0.8ml solution for injection pre-filled syringes (Biogen Idec Ltd)",
              licauthcd: "0001"
            },
            apid: "36460211000001102",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36460311000001105",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36460311000001105",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36460311000001105",
            vmp: {
              vpid: "35894411000001100",
              nmchangecd: "0004",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled syringes",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              basisprevcd: "0001",
              presstatcd: "0001",
              udfs: 0.8,
              vpidt: "2018-10-17",
              nonavaildt: "2018-10-25",
              vpidprev: "408154002",
              basiscd: "0001",
              vpiddt: {
                month: 10,
                hour: -2147483648,
                year: 2018,
                timezone: -2147483648,
                day: 17,
                minute: -2147483648,
                second: -2147483648
              },
              nmprev: "Adalimumab 50mg/ml injection 0.8ml pre-filled syringes",
              nmdt: "2004-04-29",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes",
              nonavailcd: "0000"
            },
            nm:
              "Imraldi 40mg/0.8ml solution for injection pre-filled syringes (Biogen Idec Ltd) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36234411000001103",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "11187811000001103",
              vpid: "11236911000001103",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices 2 pre-filled disposable injection"
            },
            vppid: "11187811000001103",
            priceinfo: {
              price: "64618",
              appid: "36234411000001103",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "2074301000001102",
              vpid: "11236911000001103",
              availrestrictcd: "0001",
              apid: "36234311000001105",
              ema: "0001",
              nm: "Hyrimoz 40mg/0.8ml solution for injection pre-filled pen",
              desc: "Hyrimoz 40mg/0.8ml solution for injection pre-filled pen (Sandoz Ltd)",
              licauthcd: "0001"
            },
            apid: "36234311000001105",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36234411000001103",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36234411000001103",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36234411000001103",
            vmp: {
              basiscd: "0001",
              vpid: "11236911000001103",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled disposable devices",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              presstatcd: "0001",
              udfs: 0.8,
              nonavaildt: "2018-10-25",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled disposable devices",
              nonavailcd: "0000"
            },
            nm:
              "Hyrimoz 40mg/0.8ml solution for injection pre-filled pen (Sandoz Ltd) 2 pre-filled disposable injection"
          }
        },
        {
          _index: "dmd-data",
          _type: "kafka-connect",
          _id: "36234611000001100",
          _score: 0,
          _source: {
            vmpp: {
              vppid: "5007311000001102",
              vpid: "35894411000001100",
              qtyval: 2.0,
              qtyuomcd: "3318611000001103",
              nm:
                "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes 2 pre-filled disposable injection"
            },
            vppid: "5007311000001102",
            priceinfo: {
              price: "64618",
              appid: "36234611000001100",
              pricebasiscd: "0001"
            },
            legalcatcd: "0003",
            amp: {
              suppcd: "2074301000001102",
              vpid: "35894411000001100",
              abbrevnm: "Hyrimoz 40mg/0.8ml inj pre-filled syringes",
              availrestrictcd: "0001",
              apid: "36234511000001104",
              ema: "0001",
              nm: "Hyrimoz 40mg/0.8ml solution for injection pre-filled syringes",
              desc: "Hyrimoz 40mg/0.8ml solution for injection pre-filled syringes (Sandoz Ltd)",
              licauthcd: "0001"
            },
            apid: "36234511000001104",
            legalCategory: {
              code: "0003",
              description: "POM"
            },
            type: "AMPP",
            prescinfo: {
              appid: "36234611000001100",
              padm: "0001"
            },
            reimbinfo: {
              bb: "0001",
              pxchrgs: "1",
              dispfees: "1",
              appid: "36234611000001100",
              dnd: "0001"
            },
            vtm: {
              type: "VTM",
              vtmid: "398728003",
              nm: "Adalimumab"
            },
            appid: "36234611000001100",
            vmp: {
              vpid: "35894411000001100",
              nmchangecd: "0004",
              udfsuomcd: "258773002",
              abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled syringes",
              dfindcd: "1",
              unitdoseuomcd: "3318611000001103",
              basisprevcd: "0001",
              presstatcd: "0001",
              udfs: 0.8,
              vpidt: "2018-10-17",
              nonavaildt: "2018-10-25",
              vpidprev: "408154002",
              basiscd: "0001",
              vpiddt: {
                month: 10,
                hour: -2147483648,
                year: 2018,
                timezone: -2147483648,
                day: 17,
                minute: -2147483648,
                second: -2147483648
              },
              nmprev: "Adalimumab 50mg/ml injection 0.8ml pre-filled syringes",
              nmdt: "2004-04-29",
              vtmid: "398728003",
              nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes",
              nonavailcd: "0000"
            },
            nm:
              "Hyrimoz 40mg/0.8ml solution for injection pre-filled syringes (Sandoz Ltd) 2 pre-filled disposable injection"
          }
        }
      ]
    }
  },
  suggest: {
    took: 5644,
    timed_out: false,
    _shards: {
      total: 1,
      successful: 1,
      skipped: 0,
      failed: 0
    },
    hits: {
      total: {
        value: 0,
        relation: "eq"
      },
      max_score: null,
      hits: []
    },
    suggest: {
      dmd: [
        {
          text: "Hum",
          offset: 0,
          length: 3,
          options: [
            {
              text: "Humira 40mg/0.8ml solution for injection pre-fille",
              _index: "dmd-data",
              _type: "_doc",
              _id: "5007611000001107",
              _score: 1,
              _source: {
                vmpp: {
                  vppid: "5007311000001102",
                  vpid: "35894411000001100",
                  qtyval: 2,
                  qtyuomcd: "3318611000001103",
                  nm:
                    "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes 2 pre-filled disposable injection"
                },
                type_vmp: "AMPP_35894411000001100",
                vppid: "5007311000001102",
                priceinfo: {
                  pricedt: "2011-01-01",
                  priceprev: "71500",
                  price: "70428",
                  appid: "5007611000001107",
                  pricebasiscd: "0001"
                },
                shortNameAndSupplier: "Humira 40mg/0.8ml (AbbVie Ltd)",
                legalcatcd: "0003",
                amp: {
                  suppcd: "20741511000001103",
                  licensingAuthorityPrev: {
                    code: null,
                    description: null
                  },
                  vpid: "35894411000001100",
                  availrestrictcd: "0009",
                  flavour: {
                    code: null,
                    description: null
                  },
                  apid: "5007511000001108",
                  combinationProd: {
                    code: null,
                    description: null
                  },
                  licensingAuthority: {
                    code: "0001",
                    description: "Medicines - MHRA/EMA"
                  },
                  type: "AMP",
                  availabilityRestriction: {
                    code: "0009",
                    description: "Not available"
                  },
                  supplier: {
                    code: "20741511000001103",
                    description: "AbbVie Ltd"
                  },
                  nmprev: "Humira 40mg/0.8ml injection 0.8ml pre-filled syringes",
                  bnf: {
                    chapter: "10",
                    sub_chapter: "01"
                  },
                  nmdt: "2004-07-12",
                  nm: "Humira 40mg/0.8ml solution for injection pre-filled syringes",
                  desc: "Humira 40mg/0.8ml solution for injection pre-filled syringes (AbbVie Ltd)",
                  licauthcd: "0001",
                  licensingAuthorityChange: {
                    code: null,
                    description: null
                  }
                },
                apid: "5007511000001108",
                discdt: "2018-03-13",
                legalCategory: {
                  code: "0003",
                  description: "POM"
                },
                type_vmpp: "AMPP_5007311000001102",
                type_amp: "AMPP_5007511000001108",
                type: "AMPP",
                prescinfo: {
                  appid: "5007611000001107",
                  padm: "0001"
                },
                reimbinfo: {
                  bb: "0001",
                  pxchrgs: "1",
                  dispfees: "1",
                  appid: "5007611000001107",
                  dnd: "0001"
                },
                vtm: {
                  bnf: [
                    {
                      chapter: "10",
                      sub_chapter: "01"
                    }
                  ],
                  type: "VTM",
                  vtmid: "398728003",
                  nm: "Adalimumab"
                },
                tradeName: "Humira",
                disccd: "0001",
                appid: "5007611000001107",
                vmp: {
                  vpid: "35894411000001100",
                  nmchangecd: "0004",
                  udfsuomcd: "258773002",
                  abbrevnm: "Adalimumab 40mg/0.8ml inj pre-filled syringes",
                  dfindcd: "1",
                  unitdoseuomcd: "3318611000001103",
                  basisprevcd: "0001",
                  presstatcd: "0001",
                  udfs: 0.8,
                  vpidt: "2018-10-17",
                  nonavaildt: "2018-10-25",
                  vpidprev: "408154002",
                  basiscd: "0001",
                  nmprev: "Adalimumab 50mg/ml injection 0.8ml pre-filled syringes",
                  nmdt: "2004-04-29",
                  vtmid: "398728003",
                  nm: "Adalimumab 40mg/0.8ml solution for injection pre-filled syringes",
                  nonavailcd: "0000"
                },
                shortName: "Humira 40mg/0.8ml",
                nm:
                  "Humira 40mg/0.8ml solution for injection pre-filled syringes (AbbVie Ltd) 2 pre-filled disposable injection",
                type_vtm: "AMPP_398728003",
                conditionCode: "69896004"
              },
              contexts: {
                conditionCode: ["69896004"]
              }
            }
          ]
        }
      ]
    }
  }
};
