# Make + Ship API Common

A common set of modules and utilities to supporting building RESTful APIs using node.js

## Pre-requisites

Install imagemagick locally

```
brew install imagemagick
```

or

```
brew upgrade imagemagick
```

## Development

### Tests

```
yarn test
```
