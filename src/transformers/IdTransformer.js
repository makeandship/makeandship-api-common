import JSONTransformer from "./JSONTransformer";

/**
 * A class to transform json responses
 * @property response : the response Object from mongoose
 */
class IdTransformer extends JSONTransformer {
  /**
   * The transformation engine
   */
  transform(o, options) {
    const res = super.transform(o, options);

    const id = res._id || res.id;
    if (id) {
      res.id = id.toString();
    }
    if (res._id) {
      delete res._id;
    }

    return res;
  }
}

export default IdTransformer;
