import JSONTransformer from "./JSONTransformer";

/**
 * A class to transform json responses
 * @property response : the response Object from mongoose
 */
class WhitelistTransformer extends JSONTransformer {
  /**
   * The transformation engine
   */
  transform(o, options) {
    if (options.whitelist.length > 0) {
      const res = {};
      options.whitelist.forEach(field => {
        res[field] = o[field];
      });
      return res;
    }
    return o;
  }
}

export default WhitelistTransformer;
