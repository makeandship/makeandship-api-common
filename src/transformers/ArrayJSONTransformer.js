/**
 * A class to transform an array of json responses
 * @property response : the response Object from mongoose
 */
class ArrayJSONTransformer {
  /**
   * Transform an array of results using options.transformer
   * @param {Array} results
   * @return {Array} transformed results
   */
  transform(o, options) {
    const res = [];
    const Transformer = options.transformer;

    if (Array.isArray(o) && o.length) {
      o.forEach(result => {
        if (Transformer) {
          res.push(new Transformer().transform(result, options));
        } else {
          res.push(result);
        }
      });
    }

    return res;
  }
}

export default ArrayJSONTransformer;
