import JSONTransformer from "./JSONTransformer";
import IdTransformer from "./IdTransformer";

/**
 * A class to transform json responses
 * @property response : the response Object from mongoose
 */
class ModelJSONTransformer extends JSONTransformer {
  transform(o, options) {
    let res = super.transform(o, options);
    res = new IdTransformer().transform(res);
    return res;
  }
}

export default ModelJSONTransformer;
