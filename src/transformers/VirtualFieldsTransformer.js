import JSONTransformer from "./JSONTransformer";
/**
 * A class to transform json responses
 * @property response : the response Object from mongoose
 */
class VirtualFieldsTransformer extends JSONTransformer {
  /**
   * The transformation engine
   */
  transform(o, options) {
    const res = super.transform(o, options);
    return Object.assign(res, options.virtualFields);
  }
}

export default VirtualFieldsTransformer;
