export default {
  PASS_THROUGH_SORT_WHITELIST: ["created", "modified"],
  PASS_THROUGH_FILTER_WHITELIST: ["created", "modified"]
};
