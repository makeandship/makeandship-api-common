import Promise from "bluebird";

// use mock redis for tests
let redis = require("redis");
if (process.env.NODE_ENV === "test") {
  redis = require("redis-mock");
}

// promisify
Promise.promisifyAll(redis.RedisClient.prototype);

// constants
const host = process.env.REDIS_HOST;
const port = process.env.REDIS_PORT;
const password = process.env.REDIS_PASSWORD;

/**
 * A singleton service responsible of managing redis connection,
 * Reading and writing data to the cache.
 */
class RedisService {
  constructor(options = {}) {
    this.ready = true;
    this.client = redis.createClient(port, host, options);
    this.client.on("error", err => {
      this.ready = false;
      return err;
    });
  }

  /**
   * Set a key into redis
   *
   * Optionally pass expireSeconds to control how long this value
   * should be kept for if not the default otherwise will be the
   * default
   *
   * @param {*} key
   * @param {*} value
   * @param {*} expireSeconds
   */
  async set(key, value, expireSeconds = null) {
    if (!this.ready) {
      return null;
    }
    await this.client.setAsync(key, value);
    if (expireSeconds && Number.isInteger(expireSeconds)) {
      await this.client.expireAsync(key, parseInt(expireSeconds));
    }
  }

  async get(key) {
    if (!this.ready) {
      return null;
    }
    const value = await this.client.getAsync(key);
    return value;
  }

  async keys(pattern) {
    if (!this.ready) {
      return null;
    }
    if (pattern) {
      const values = await this.client.keysAsync(pattern);
      return values;
    }
    return null;
  }

  async del(key) {
    if (!this.ready) {
      return null;
    }
    if (Array.isArray(key)) {
      const results = [];
      for (const entry of key) {
        const result = await this.delKey(entry);
        results.push(result);
      }
      return results;
    } else {
      const result = await this.delKey(key);
      return result;
    }
  }

  async delKey(key) {
    if (!this.ready) {
      return null;
    }
    if (key) {
      const result = await this.client.delAsync(key);
      return result;
    }
    return null;
  }

  async disconnect() {
    if (!this.ready) {
      return;
    }
    await this.client.quitAsync();
  }
}

export default password ? new RedisService({ password }) : new RedisService();
