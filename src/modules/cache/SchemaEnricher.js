import { redisService } from "./services";
import { utils } from "../json";

/**
 * Enrich an object with descriptions from the cache
 */
class SchemaEnricher {
  static async enrich(object) {
    const flat = utils.flatten(JSON.parse(JSON.stringify(object)));
    const keys = Object.keys(flat);
    for (let key of keys) {
      const code = flat[key];
      const description = await redisService.get(`${key}:${code}`);
      if (description) {
        utils.setValue(`${key}Description`, description, object);
      }
    }
  }

  static async enrichChoices(path, choices) {
    for (let choice of choices) {
      const description = await redisService.get(`${path}:${choice.key}`);
      if (description) {
        choice.description = description;
      }
    }
  }
}

export default SchemaEnricher;
