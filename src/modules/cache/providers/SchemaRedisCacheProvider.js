import SchemaCacheProvider from "./SchemaCacheProvider";

/**
 * A provider to generate the redis cache data from a jsonschema
 */
class SchemaRedisCacheProvider extends SchemaCacheProvider {
  constructor(schema) {
    super(schema);
  }

  getCachePayload() {
    const paths = this.schemaExplorer.getPathsWith("enumNames");
    const cache = {};
    Object.keys(paths).forEach(key => {
      const path = paths[key];
      for (let index = 0; index < path.enum.length; index++) {
        const cacheKey = `${key}:${path.enum[index]}`;
        cache[cacheKey] = path.enumNames[index];
      }
    });

    return cache;
  }
}

export default SchemaRedisCacheProvider;
