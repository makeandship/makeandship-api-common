import SchemaExplorer from "../../jsonschema/schema-explorer";
/**
 * A provider to generate the cache data from a jsonschema
 */
class SchemaCacheProvider {
  constructor(schema) {
    this.schemaExplorer = new SchemaExplorer(schema);
  }

  getCachePayload() {}
}

export default SchemaCacheProvider;
