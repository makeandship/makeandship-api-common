import rmfr from "rmfr";
import mkdirp from "mkdirp-promise";
import Promise from "bluebird";
import path from "path";
import gm from "gm";
import url from "url";
import shortid from "shortid";

class ImageConverter {
  constructor(upload) {
    this.upload = upload;
    this.file = upload.path;
    this.extension = this.getExtension(this.file);
    this.im = gm.subClass({ imageMagick: true });
  }

  async convert(sizes, options, clean = false) {
    const that = this;

    // promises to convert to each size
    if (sizes) {
      const code = shortid.generate();

      if (clean) {
        await this.cleanDirectory(this.getDirectory(options));
      }

      await this.createDirectory(this.getDirectory(options));

      const promises = sizes.map(size => that.convertImage(code, size, options));
      return Promise.all(promises);
    }
  }

  async convertImage(code, size, options) {
    // read and resize the image
    const image = await this.readSourceImage();
    const resizedImage = await image.resize(size, size);

    // write the converted file
    const directory = this.getDirectory(options);
    const extension = this.getExtension(this.upload.originalname);
    const filename = `${code}-${size}${extension}`;
    const destination = `${directory}/${filename}`;

    await this.writeThumbnail(resizedImage, destination);

    // generate a target url
    const url = this.getUrl(filename, options);

    // get the filesize
    const filesize = await this.getFilesize(resizedImage);

    const summary = {
      url,
      height: size,
      width: size,
      filename,
      mimetype: this.upload.mimetype,
      size: filesize
    };

    return summary;
  }

  getExtension(filename) {
    return path.extname(filename);
  }

  getUrl(filename, options) {
    return url.format({
      protocol: options.protocol,
      host: options.host,
      pathname: `${options.route}/${filename}`
    });
  }

  getFilesize(image) {
    return new Promise((resolve, reject) => {
      image.filesize((err, size) => {
        if (err) {
          return reject(err);
        }
        return resolve(parseInt(size));
      });
    });
  }

  readSourceImage() {
    const source = this.file;
    return new Promise((resolve, reject) => {
      try {
        const img = this.im(source);
        return resolve(img);
      } catch (err) {
        return reject(err);
      }
    });
  }

  writeThumbnail(image, destination) {
    return new Promise((resolve, reject) => {
      image.write(destination, err => {
        if (err) {
          return reject(err);
        }
        return resolve(true);
      });
    });
  }

  getDirectory(options) {
    return `${options.uploadDir}/${options.target}`;
  }

  async createDirectory(directory) {
    await mkdirp(directory);
  }

  async cleanDirectory(directory) {
    await rmfr(directory);
  }
}

export default ImageConverter;
