import url from "url";
import mkdirp from "mkdirp-promise";
import isImage from "is-image";
import ImageConverter from "./image-converter";
import config from "../../../config";
import File from "../../../util/file";

/**
 * Upload images and ordinary files
 * Generate thumbs for images
 * Copy files to destination
 * @param {*} file
 * @param {*} options
 */
const upload = async (file, options) => {
  options = {
    uploadDir: config.express.uploadDir,
    filename: file.originalname,
    ...options
  };

  if (file && file.originalname) {
    const result = await uploadFile(file, options);

    if (isImage(file.originalname)) {
      const sizes = options.imageSizes || config.accounts.avatarSizes;
      const converter = new ImageConverter(file);
      result.thumbnails = await converter.convert(sizes, options);
    }
    return result;
  }

  return null;
};

/**
 * Upload a file to a given destination
 * @param {*} file
 * @param {*} options
 */
const uploadFile = async (file, options) => {
  const { originalname, mimetype, size } = file;

  const targetDirectory = `${options.uploadDir}/${options.target}`;
  await mkdirp(`${targetDirectory}`);

  const targetFilepath = `${options.uploadDir}/${options.target}/${options.filename}`;
  await File.copy(file.path, targetFilepath);

  const targetUrl = url.format({
    protocol: options.protocol,
    host: options.host,
    pathname: `${options.route}/${options.filename}`
  });

  return {
    url: targetUrl,
    filename: originalname,
    mimetype,
    size
  };
};

/**
 * Remove a file from a given location
 * @param {*} file
 * @param {*} options
 */
const remove = async (path, filename) => {
  const uploadDir = config.express.uploadDir;
  const targetFilepath = `${uploadDir}/${path}/${filename}`;
  const targetFileExists = await File.exists(targetFilepath);
  if (targetFileExists) {
    return await File.remove(targetFilepath);
  }

  return false;
};

const files = Object.freeze({
  upload,
  remove
});

export default files;
