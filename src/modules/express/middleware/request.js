/**
 * An express middleware to whitelist the request body
 *
 * @param {array} whitelistFields
 *
 * @return {function} next
 */
const whitelist = whitelistFields => {
  return (req, res, next) => {
    if (!whitelistFields || !whitelistFields.length) {
      return next();
    }
    const body = {};
    whitelistFields.forEach(field => {
      body[field] = req.body[field];
    });
    req.body = body;
    return next();
  };
};

const request = Object.freeze({ whitelist });

export default request;
