import Validator from "../../ajv/Validator";

/**
 * An express middleware to validate jsonschema
 * Expects an error to be returned in case the validation failed
 *
 * @param {object} jsonschema
 * @param {object} options
 *
 * @return {function} next
 */
const schemaValidation = (schema, options = {}) => {
  if (!schema) throw new Error("Please provide a validation schema");

  const message = options.message ? options.message : "Validation failed";
  if (options && options.message) {
    delete options.message;
  }

  // create the ajv instance
  const validator = new Validator(schema, options);

  return (req, res, next) => {
    const errors = validator.validate(req.body);
    if (errors) {
      return next({ errors, message });
    }
    return next();
  };
};

/**
 * An express middleware to remove additional fields from the request body
 *
 * @param {object} jsonschema
 *
 * @return {function} next
 */
const trim = schema => {
  if (!schema) throw new Error("Please provide a validation schema");

  // create the ajv instance
  const validator = new Validator(schema, { removeAdditional: "all" });
  const ajv = validator.init({ removeAdditional: "all" });

  return (req, res, next) => {
    const validate = ajv.compile(schema);
    validate(req.body);
    return next();
  };
};

const jsonschema = Object.freeze({
  schemaValidation,
  trim
});
export { schemaValidation, trim };
export default jsonschema;
