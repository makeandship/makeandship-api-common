import cheerio from "cheerio";
import request from "request-promise";
import url from "url";
import KeycloakAccount from "../../accounts/keycloak";
import PhoneHelper from "../../../helpers/PhoneHelper";

const createUUID = () => {
  const s = [];
  const hexDigits = "0123456789abcdef";
  for (let i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = "4";
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
  s[8] = s[13] = s[18] = s[23] = "-";

  const uuid = s.join("");
  return uuid;
};

const isError = body => {
  const $ = typeof body === "function" ? body : cheerio.load(body);
  return $(".alert-error").length > 0;
};

const getErrors = body => {
  const $ = typeof body === "function" ? body : cheerio.load(body);

  const errors = [];

  const errorElements = $(".alert-error span.kc-feedback-text");
  if (errorElements.length > 0) {
    errorElements.each(function() {
      errors.push(cheerio(this).text());
    });
  }

  return errors;
};

class Keycloak {
  constructor(options) {
    /**
     * Core keycloak configuration
     * Core attributes used in various calls
     */
    Object.assign(this, {
      userModel: options.userModel,
      nativeConfig: options.keycloakConfig.native,
      realm: options.keycloakConfig.realm,
      authUrl: options.keycloakConfig.authUrl,
      tokenUrl: options.keycloakConfig.tokenUrl,
      options
    });
  }

  /**
   * process to authentocate by phone
   * @param {*} phone
   */
  async phoneAuthenticationProcess(phone) {
    const rp = request;
    const uri = `${this.nativeConfig.protocol}://${this.nativeConfig.host}/auth/realms/${this.realm}/${this.authUrl}`;

    const qs = {
      client_id: this.nativeConfig.clientId,
      scope: this.nativeConfig.scope,
      response_type: this.nativeConfig.responseType,
      redirect_uri: this.nativeConfig.redirectUri,
      state: createUUID()
    };

    const startAuthenticationOptions = {
      uri,
      qs,
      headers: {
        "User-Agent": "M+S"
      },
      resolveWithFullResponse: true
    };

    const startAuthenticationResponse = await rp(startAuthenticationOptions);

    const $ = cheerio.load(startAuthenticationResponse.body);
    const headers = startAuthenticationResponse.headers;

    const cookies = headers["set-cookie"];

    const jar = rp.jar();
    cookies.forEach(cookie => {
      jar.setCookie(cookie, `${this.nativeConfig.protocol}://${this.nativeConfig.host}`);
    });

    const form = $("#kc-content form");

    if (form) {
      const initialAction = $(form).attr("action");
      const initialMethod = $(form).attr("method");

      const sendPhoneNumberOptions = {
        method: initialMethod,
        uri: initialAction,
        form: {
          mobile_number: phone,
          login: "Submit"
        },
        resolveWithFullResponse: true,
        jar
      };

      const sendPhoneNumberResponse = await rp(sendPhoneNumberOptions);

      const phone$ = cheerio.load(sendPhoneNumberResponse.body);

      const phoneError = isError(phone$);

      if (phoneError) {
        const errors = getErrors(phone$);
        throw new Error(errors);
      } else {
        const phoneForm = phone$("#kc-totp-login-form");
        const phoneAction = phone$(phoneForm).attr("action");
        const phoneMethod = phone$(phoneForm).attr("method");
        await this.saveUser(phone, {
          cookies,
          method: phoneMethod,
          uri: phoneAction
        });
      }
    }
  }

  /**
   * An express middleware to authenticate by phone
   * Expects an error to be returned in case the authentication failed
   *
   * @param {object} req
   * @param {object} res
   * @param {function} next
   *
   * @return {function} next
   */
  authenticateByPhone() {
    return async (req, res, next) => {
      const phone = req.body.username;
      if (phone && PhoneHelper.isValid(phone)) {
        const formattedPhone = PhoneHelper.getPhone(phone);
        try {
          await this.phoneAuthenticationProcess(formattedPhone);
          return res.jsend("SMS sent successfully.");
        } catch (e) {
          return res.jerror(e);
        }
      }
      return next();
    };
  }

  /**
   * An express middleware to validate the sms code
   * Expects an error to be returned in case the validation failed
   *
   * @param {object} req
   * @param {object} res
   * @param {function} next
   *
   * @return {function} next
   */
  validateSMSCode() {
    return async (req, res, next) => {
      const phone = req.body.username;
      const smsCode = req.body.smsCode;
      if (phone && PhoneHelper.isValid(phone) && smsCode) {
        const formattedPhone = PhoneHelper.getPhone(phone);
        try {
          const rp = request;
          let user = await this.userModel.findByPhone(formattedPhone);
          let keycloakRequest = user.keycloakRequest;
          if (!keycloakRequest || keycloakRequest.cookies || keycloakRequest.cookies.length === 0) {
            await this.phoneAuthenticationProcess(formattedPhone);
            user = await this.userModel.findByPhone(formattedPhone);
            keycloakRequest = user.keycloakRequest;
          }
          const jar = rp.jar();
          keycloakRequest.cookies.forEach(cookie => {
            jar.setCookie(cookie, `${this.nativeConfig.protocol}://${this.nativeConfig.host}`);
          });
          const sendSMSCodeOptions = {
            method: keycloakRequest.method,
            uri: keycloakRequest.uri,
            form: {
              smsCode: smsCode,
              login: "Submit"
            },
            resolveWithFullResponse: true,
            jar,
            followRedirect: false,
            simple: false
          };

          const sendSMSCodeResponse = await rp(sendSMSCodeOptions);
          const sendSMSCode$ = cheerio.load(sendSMSCodeResponse.body);
          const sendSMSCodeError = isError(sendSMSCode$);
          if (sendSMSCodeError) {
            const errors = getErrors(sendSMSCode$);
            return res.jerror(errors);
          } else {
            // clear the cookies
            await this.saveUser(formattedPhone, {});

            const sendSMSCodeHeaders = sendSMSCodeResponse.headers;

            // code
            const approvedRedirect = sendSMSCodeHeaders["location"];

            const parsedApprovedRedirect = url.parse(approvedRedirect, true);
            const queryParams = parsedApprovedRedirect.query;

            const getTokensOptions = {
              method: "POST",
              uri: `${this.nativeConfig.protocol}://${this.nativeConfig.host}/auth/realms/${this.realm}/${this.tokenUrl}`,
              form: {
                client_id: this.nativeConfig.clientId,
                grant_type: this.nativeConfig.grantType,
                code: queryParams.code,
                redirect_uri: this.nativeConfig.redirectUri
              },
              resolveWithFullResponse: true
            };

            const getTokensResponse = await rp(getTokensOptions);

            return res.jsend({
              redirect_uri: this.nativeConfig.redirectUri,
              ...JSON.parse(getTokensResponse.body)
            });
          }
        } catch (e) {
          return res.jerror(e);
        }
      }
      return next();
    };
  }

  /**
   * Save user to Mongo
   * @param {string} phone
   * @param {object} keycloakRequest
   */
  async saveUser(phone, keycloakRequest) {
    let user;
    try {
      user = await this.userModel.findByPhone(phone);
    } catch (e) {
      // error in mongo fetch the user from keycloak and save it
      user = await this.syncUser(phone);
    }
    if (user) {
      user.keycloakRequest = keycloakRequest;
      await user.save();
    }
  }

  /**
   * Sync user with keycloak
   * @param {*} phone
   */
  async syncUser(phone) {
    const keycloakAccount = new KeycloakAccount(this.options);
    await keycloakAccount.syncUserByPhone(phone);
  }
}

export default Keycloak;
