import Dot from "dot-prop";
import fs from "fs";
import os from "os";
import is from "type-is";
import Promise from "bluebird";

import SchemaExplorer from "../../jsonschema/schema-explorer";
import File from "../../../util/file";

const processBase64 = async (path, file, options) => {
  const metadata = File.decodeBase64(file);
  if (metadata) {
    const fileMetadata = {
      fieldname: path
    };
    if (metadata.name) {
      fileMetadata.originalname = metadata.name;
    }
    if (metadata.mimetype) {
      fileMetadata.mimetype = metadata.mimetype;
    }

    // directory and filename
    const directory = options.dest || os.tmpdir();
    const filename = await File.getRandomFilename();
    const filepath = `${directory}${filename}`;

    fileMetadata.destination = directory;
    fileMetadata.filename = filename;
    fileMetadata.path = filepath;

    const buffer = metadata.data;
    // process file
    try {
      await uploadBase64(filepath, buffer);
      const size = await File.getSize(filepath);
      fileMetadata.size = size;

      return fileMetadata;
    } catch (e) {
      return null;
    }
  }

  return null;
};
const uploadBase64 = async (filepath, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, data, err => {
      if (err) {
        return reject(err);
      } else {
        return resolve(filepath);
      }
    });
  });
};

/**
 * An express middleware to identify base64 encoded files and
 * add them to req.files (to align with multer)
 *
 * @param {object} jsonschema
 * @param {Error} error
 * @param {string} errorObjectOrName
 *
 * @return {function} next
 */
const base64 = (schema, options) => {
  if (!schema) throw new Error("Please provide a validation schema");

  const explorer = new SchemaExplorer(schema);
  const paths = explorer.getPathsWith("format", "binary");

  // { fieldname: 'attachments',
  //   originalname: 'bailey-zindel-NRQV-hBF10M-unsplash.jpg',
  //   encoding: '7bit',
  //   mimetype: 'image/jpeg',
  //   destination: 'tmp/',
  //   filename: '400096407b4559f848617133f3da04d4',
  //   path: 'tmp/400096407b4559f848617133f3da04d4',
  //   size: 6232691 },
  return async (req, res, next) => {
    if (!is(req, ["multipart"])) return next();

    try {
      if (paths) {
        for (let path of Object.keys(paths)) {
          const body = req.body;
          const value = Dot.get(body, path);

          if (value) {
            const files = [];
            if (Array.isArray(value)) {
              for (let item of value) {
                const uploadedFile = await processBase64(path, item, options);
                if (uploadedFile) {
                  files.push(uploadedFile);
                }
              }
            } else {
              const uploadedFile = await processBase64(value, options);
              if (uploadedFile) {
                files.push(uploadedFile);
              }
            }

            if (files) {
              if (!req.files) {
                req.files = [];
              }
              req.files.push(...files);
            }

            Dot.delete(req.body, path);
          }
        }
      }
      next();
    } catch (e) {
      next(e);
    }
  };
};

const files = Object.freeze({
  base64
});

export default files;
