import httpStatus from "http-status";
import { AuthError, ValidationError, RuntimeError } from "../error";

const response = function(status, data, message, code, statusCode = 200) {
  const response = { status };
  if (code) {
    response.code = code;
  }
  if (message) {
    response.message = message;
  }
  if (data) {
    response.data = data;
  }
  this.status(statusCode).json(response);
};

const jsend = function(data) {
  return response.call(this, "success", data);
};

const jerror = function(error, statusCode) {
  const responseStatusCode = statusCode || error.httpStatus || httpStatus.INTERNAL_SERVER_ERROR;
  if (error && (error instanceof AuthError || error instanceof RuntimeError)) {
    return response.call(this, "error", null, error.message, error.code, responseStatusCode);
  } else if (error && error instanceof ValidationError) {
    return response.call(
      this,
      "error",
      error.data || error,
      error.message,
      error.code,
      responseStatusCode
    );
  } else {
    return response.call(
      this,
      "error",
      error.data,
      error.message,
      error.code || 10000,
      responseStatusCode
    );
  }
};

const jfail = function(error, statusCode) {
  return response.call(this, "fail", error, error.message, statusCode);
};

const jsendExpress = Object.freeze({
  jsend,
  jerror,
  jfail
});

export default jsendExpress;
