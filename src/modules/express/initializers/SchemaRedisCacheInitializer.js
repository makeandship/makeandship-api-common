import { redisService } from "../../cache/services";
import SchemaRedisCacheProvider from "../../cache/providers/SchemaRedisCacheProvider";
import SchemaCacheInitializer from "./SchemaCacheInitializer";

/**
 * A cache initializer to seed cache values to Redis
 */
class SchemaRedisCacheInitializer extends SchemaCacheInitializer {
  constructor(schemas) {
    super(schemas);
  }

  initialize() {
    this.schemas.forEach(schema => {
      const provider = new SchemaRedisCacheProvider(schema);
      const payload = provider.getCachePayload();
      Object.keys(payload).forEach(key => {
        redisService.set(key, payload[key]);
      });
    });
  }
}

export default SchemaRedisCacheInitializer;
