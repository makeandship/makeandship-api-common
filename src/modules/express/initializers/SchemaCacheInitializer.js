/**
 * A cache initializer to seed cache values
 */
class SchemaCacheInitializer {
  constructor(schemas) {
    this.schemas = schemas;
  }

  initialize() {}
}

export default SchemaCacheInitializer;
