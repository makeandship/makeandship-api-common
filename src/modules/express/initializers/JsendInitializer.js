import jsend from "../../express/jsend";

/**
 * A cache initializer to add express jsend
 */
class JsendInitializer {
  initialize(app) {
    Object.assign(app.response, jsend);
  }
}

export default JsendInitializer;
