/**
 * An express initializer
 */
class ExpressInitializer {
  constructor(app) {
    this.initializers = [];
    this.app = app;
  }

  add(initializer) {
    this.initializers.push(initializer);
  }

  async initialize(options = {}) {
    for (const initializer of this.initializers) {
      await initializer.initialize(this.app, options);
    }
  }
}

export default ExpressInitializer;
