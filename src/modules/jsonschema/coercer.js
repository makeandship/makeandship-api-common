import SchemaExplorer from "../jsonschema/schema-explorer";

import moment from "moment";
import { utils as JSONUtils } from "../json";
import JSONSchemaUtils from "../jsonschema/util/index";

/**
 * Coerce an object attributes against a schema formats and types
 * @param {*} object
 * @param {*} schema
 */
const coerce = (schema, object) => {
  const explorer = new SchemaExplorer(schema);

  const flat = JSONUtils.flatten(JSON.parse(JSON.stringify(object)));
  Object.keys(flat).forEach(key => {
    const path = JSONSchemaUtils.getPath(key);
    const definition = explorer.getSchema(path);
    if (definition && Object.prototype.hasOwnProperty.call(definition, "type")) {
      const { type, format } = definition;
      const coercerFunction = getCoercer(type, format);
      if (coercerFunction) {
        const coercedValue = coercerFunction(flat[key]);
        JSONUtils.setValue(key, coercedValue, object);
      }
    }
  });
};

/**
 * Get the coercer function from a given type and format
 * @param {*} type
 * @param {*} format
 */
const getCoercer = (type, format) => {
  if (JSONSchemaUtils.isTypeValid(type, "string") && format === "date") {
    return dateCoercer;
  } else if (JSONSchemaUtils.isTypeValid(type, "string") && format === "date-time") {
    return dateTimeCoercer;
  } else if (JSONSchemaUtils.isTypeValid(type, "boolean")) {
    return booleanCoercer;
  }
  return null;
};

/**
 * Check if a date is valid
 * @param {*} d
 */
const isValidDate = d => d instanceof Date && !isNaN(d);

/**
 * A date coercer to a string with format yyyy-mm-dd
 * @param {*} value
 */
const dateCoercer = value => {
  const isValid = isValidDate(new Date(value));
  if (isValid) {
    const validDate = new Date(value);
    // parse in utc - calling clients will send as utc+offset
    const date = moment.utc(validDate);
    const dateOnly = date.format("YYYY-MM-DD");

    return dateOnly;
  }
  return value;
};

/**
 * A date-time coercer to a string using iso format
 * @param {*} value
 */
const dateTimeCoercer = value => {
  const isValid = isValidDate(new Date(value));

  if (isValid) {
    const validDate = new Date(value);
    // parse in utc - calling clients will send as utc+offset
    const date = moment.utc(validDate);
    return date.toISOString();
  }

  return value;
};

const booleanCoercer = value => {
  if (typeof value === "boolean") {
    return value;
  } else if (typeof value === "string") {
    return value === "true";
  } else if (typeof value === "number") {
    return value !== 0;
  }
  return false;
};

const coercer = Object.freeze({
  coerce
});

export default coercer;
