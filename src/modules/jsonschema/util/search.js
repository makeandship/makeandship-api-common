import SchemaExplorer from "../schema-explorer";
import Constants from "../../../constants";
import JSONSchemaUtils from "../util";

class Search {
  constructor(schema) {
    this.schema = schema;
    this.explorer = new SchemaExplorer(this.schema);
  }

  /**
   * Is this schema sortable.  Sortable attributes
   * - enumerations
   * - dates
   * - date times
   * - integers
   * - numbers
   * - booleans
   * @param {*} schema
   */
  isSortable(schema) {
    if (schema && schema.type) {
      if (JSONSchemaUtils.isTypeValid(schema.type, "string") && (schema.enum || schema.keyword)) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "string") && schema.format === "date") {
        return true;
      } else if (
        JSONSchemaUtils.isTypeValid(schema.type, "string") &&
        schema.format === "date-time"
      ) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "integer")) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "number")) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "boolean")) {
        return true;
      }
    }

    return false;
  }

  /**
   * Is this schema a range.  Range attributes
   * - dates
   * - date times
   * - integers
   * - numbers
   * @param {*} schema
   */
  isRange(schema) {
    if (schema && schema.type) {
      if (JSONSchemaUtils.isTypeValid(schema.type, "string") && schema.format === "date") {
        return true;
      } else if (
        JSONSchemaUtils.isTypeValid(schema.type, "string") &&
        schema.format === "date-time"
      ) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "integer")) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "number")) {
        return true;
      }
    }

    return false;
  }

  /**
   * Is this schema summable i.e. totals and averages.  Summable attributes
   * - integers
   * - numbers
   * @param {*} schema
   */
  isSummable(schema) {
    if (schema && schema.type) {
      if (JSONSchemaUtils.isTypeValid(schema.type, "integer")) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "number")) {
        return true;
      }
    }

    return false;
  }

  /**
   * Is this schema a choice.  Choice attributes
   * - enumerations
   * - booleans
   * @param {*} schema
   */
  isChoice(schema) {
    if (schema && schema.type) {
      if (JSONSchemaUtils.isTypeValid(schema.type, "string") && (schema.enum || schema.keyword)) {
        return true;
      } else if (JSONSchemaUtils.isTypeValid(schema.type, "boolean")) {
        return true;
      }
    }

    return false;
  }

  /**
   * Return all sortable attributes
   *
   * @return {Array} attributes
   */
  getSortable() {
    const attributes = this.explorer.getAttributes();
    const sortable = attributes.filter(path => {
      const attributeSchema = this.explorer.getSchema(path);
      return this.isSortable(attributeSchema);
    });
    const final = sortable
      ? [...sortable, ...Constants.PASS_THROUGH_SORT_WHITELIST]
      : Constants.PASS_THROUGH_SORT_WHITELIST;
    return final;
  }

  /**
   * Get the search filters and ranges
   */
  getRangeFilters() {
    const choices = this.getChoices();
    const ranges = this.getRanges();

    return {
      choices,
      ranges
    };
  }

  /**
   * Get the search filters and summables
   */
  getCalculationFilters() {
    const choices = this.getChoices();
    const summables = this.getSummables();

    return {
      choices,
      summables
    };
  }

  /**
   * Return all choice attributes from a schema
   */
  getChoices() {
    const attributes = this.explorer.getAttributes();
    const choices = attributes.filter(path => {
      const attributeSchema = this.explorer.getSchema(path);
      return this.isChoice(attributeSchema);
    });
    return choices;
  }

  /**
   * Return all range attributes from a schema
   */
  getRanges() {
    const attributes = this.explorer.getAttributes();
    const ranges = attributes.filter(path => {
      const attributeSchema = this.explorer.getSchema(path);
      return this.isRange(attributeSchema);
    });
    return ranges;
  }

  /**
   * Return all summable attributes from a schema
   */
  getSummables() {
    const attributes = this.explorer.getAttributes();
    const summables = attributes.filter(path => {
      const attributeSchema = this.explorer.getSchema(path);
      return this.isSummable(attributeSchema);
    });
    return summables;
  }

  /**
   * Get all enums for a given attribute
   */
  getEnums(path) {
    if (path) {
      const attributeSchema = this.explorer.getSchema(path);
      if (attributeSchema && attributeSchema.enum) {
        return attributeSchema.enum;
      }
    }

    return null;
  }

  /**
   * Get all enum size for a given attribute
   */
  getEnumSize(path) {
    if (path) {
      const enums = this.getEnums(path);
      if (enums) {
        return enums.length;
      }
    }

    return null;
  }
}

export default Search;
