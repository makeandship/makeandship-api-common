import Ajv from "ajv";
import traverse from "traverse";
import SchemaExplorer from "../schema-explorer";

/**
 * Return titles for a schema
 *
 * Note this method does not validate the schema
 *
 * @param {*} schema
 * @return {object} titles keyed by dot notation property name
 */
const schemaTitles = schema => {
  const explorer = new SchemaExplorer(schema);
  return explorer.getMapBy(["title", "titles"]);
};

/**
 * Return formats for a schema
 *
 * Note this method does not validate the schema
 *
 * @param {*} schema
 * @return {object} titles keyed by dot notation property name
 */
const schemaFormats = schema => {
  const explorer = new SchemaExplorer(schema);
  return explorer.getMapBy(["format"]);
};

/**
 * Validate an object against a schema
 * @param {*} object
 * @param {*} schema
 */
const isValid = (object, schema) => {
  const ajv = new Ajv({ allErrors: true, $data: true });
  const validate = ajv.compile(schema);
  return validate(object);
};

/**
 * Get a path to a schema definition from a JSON object path
 * @param {String} path
 * @return {String} path
 */
const getPath = path => {
  // replace indexed attributes e.g. user.0.buddy is user.buddy
  return path.replace(/(\.\d+\.)/g, ".");
};

/**
 * Dereference all $ref in a schema
 * @param {Schema} schema
 * @return {Schema} derefereced schema
 */
const dereference = schema => {
  if (schema) {
    const definitions = {};

    // build definitions
    traverse(schema).forEach(function(value) {
      if (value && value.definitions) {
        const prefix = this.path.length === 0 ? "#" : "#/";
        const path = `${prefix}${this.path.join("/")}`;

        Object.keys(value.definitions).forEach(attribute => {
          const attributePath = `${path}/definitions/${attribute}`;
          definitions[attributePath] = value.definitions[attribute];
        });
      }
    });

    traverse(schema).forEach(function(value) {
      // work from the parent
      if (value && Object.prototype.hasOwnProperty.call(value, "$ref")) {
        const $ref = value["$ref"];
        const definition = definitions[$ref];

        if (definition) {
          // source attributes
          const dereferenced = Object.assign({}, value);
          delete dereferenced.$ref;

          // reference attributes without overriding
          Object.keys(definition).forEach(attribute => {
            if (!dereferenced[attribute]) {
              dereferenced[attribute] = definition[attribute];
            }
          });

          // replace the value
          this.update(dereferenced);
        }
      }
    });
  }
  return schema;
};

/**
 * Return if the current type is value
 */
const isTypeValid = (type, value) => {
  if (type && Array.isArray(type)) {
    return type.indexOf(value) > -1;
  }

  return type === value;
};

const util = Object.freeze({
  schemaTitles,
  schemaFormats,
  isValid,
  getPath,
  dereference,
  isTypeValid
});

export default util;
