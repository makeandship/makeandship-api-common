import deepmerge from "deepmerge";
import SchemaParser from "../../jsonschema/schema-parser";

const generateModel = (schema, extensions = {}) => {
  const parsableSchema = JSON.parse(JSON.stringify(schema));
  const parsedSchema = SchemaParser.parse(parsableSchema);
  const model = buildMongooseAttributes(parsedSchema.properties, parsedSchema.dependencies);
  addExtensions(model, extensions);
  return model;
};

const buildMongooseAttributes = (properties, dependencies) => {
  const obj = {};
  Object.keys(properties).forEach(key => {
    const object = properties[key];
    const defaultValue = object.default;
    const index = object.index;
    if (Array.isArray(object.type)) {
      obj[key] = {
        type: "Mixed"
      };
    } else {
      switch (object.type) {
        case "string":
          if (object.format && (object.format === "date" || object.format === "date-time")) {
            obj[key] = object.unique === true ? { type: "Date", unique: true } : { type: "Date" };
          } else {
            obj[key] =
              object.unique === true ? { type: "String", unique: true } : { type: "String" };
          }
          break;
        case "integer":
        case "number":
          obj[key] = object.unique === true ? { type: "Number", unique: true } : { type: "Number" };
          break;
        case "boolean":
          obj[key] = { type: "Boolean" };
          break;
        case "object":
          if (object.properties) {
            obj[key] = buildMongooseAttributes(object.properties, object.dependencies);
          } else {
            obj[key] = { type: "Mixed" };
          }
          break;
        case "array":
          {
            if (!object.items.properties && object.items) {
              // wrap to simulate a properties object and unpack
              const wrapped = buildMongooseAttributes({ items: object.items }, null);
              const item = wrapped.items;
              obj[key] = [item];
            } else {
              const itemProperties = object.items.properties || object.items;
              const item = buildMongooseAttributes(itemProperties, null);
              obj[key] = [item];
            }
          }
          break;
      }
    }
    if (typeof defaultValue !== "undefined" && defaultValue !== "") {
      obj[key].default = defaultValue;
    }
    if (typeof index !== "undefined") {
      obj[key].index = index;
    }
  });
  const dependencyObj = buildMongooseDependencyAttributes(obj, dependencies);
  return dependencyObj;
};

const buildMongooseDependencyAttributes = (model, dependencies) => {
  if (model && dependencies) {
    const definitions = [];

    Object.keys(dependencies).forEach(attribute => {
      const dependencyAttribute = dependencies[attribute];

      if (dependencyAttribute.anyOf) {
        definitions.push(...dependencyAttribute.anyOf);
      } else if (dependencyAttribute.oneOf) {
        definitions.push(...dependencyAttribute.oneOf);
      } else if (dependencyAttribute.allOf) {
        definitions.push(...dependencyAttribute.allOf);
      } else {
        definitions.push(dependencyAttribute);
      }
    });

    definitions.forEach(definition => {
      if (definition.properties) {
        Object.keys(definition.properties).forEach(property => {
          if (!model.property) {
            const jsonSchema = definition.properties[property];
            const singlePropertySchema = {};
            singlePropertySchema[property] = jsonSchema;
            const propertyMongooseSchema = buildMongooseAttributes(singlePropertySchema);
            if (propertyMongooseSchema[property]) {
              model[property] = propertyMongooseSchema[property];
            }
          }
        });
      }
    });
  }

  return model;
};

const addExtensions = (model, extensions) => {
  Object.keys(extensions).forEach(key => {
    const extension = extensions[key];
    model[key] = deepmerge(model[key], extension);

    if (Array.isArray(model[key])) {
      model[key].forEach(item => {
        if (item.ref && !item.type) {
          item.type = "ObjectId";
        }
      });
    } else {
      if (model[key].ref && !model[key].type) {
        model[key].type = "ObjectId";
      }
    }
  });
};

const schemaBuilder = Object.freeze({
  generateModel
});

export default schemaBuilder;
