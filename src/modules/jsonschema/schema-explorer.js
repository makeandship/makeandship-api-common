import util from "./util/index";

class SchemaExplorer {
  constructor(schema) {
    this.schema = util.dereference(schema);

    this.attributesByPath = this.build(schema.properties, schema.definitions, schema.dependencies);
  }

  /**
   * Build a map of path to attributes
   *
   * e.g
   * {
   *    "field1": { title: "..", ...}
   *    "field1.field2": { title: "..", ...}
   *    "field1.field2.field3": { title: "..", ...}
   * }
   * @param {*} properties
   * @param {*} definitions
   * @param {*} dependencies
   * @param {*} parent
   * @param {*} attributesByPath
   */
  build(properties, definitions, dependencies, parent = null, attributesByPath = {}) {
    const that = this;
    if (properties) {
      const attributes = Object.keys(properties);
      attributes.forEach(attribute => {
        const path = parent ? `${parent}.${attribute}` : attribute;

        // resolve references
        const attributeDefinition = properties[attribute];
        const attributeSchema = attributeDefinition.$ref
          ? that.getDefinition(definitions, attributeDefinition.$ref, attributeDefinition)
          : attributeDefinition;

        if (attributeSchema) {
          switch (attributeSchema.type) {
            case "object":
              attributesByPath = that.build(
                attributeSchema.properties,
                definitions,
                attributeSchema.dependencies,
                path,
                attributesByPath
              );

              // store parent attribute
              attributesByPath[path] = attributeSchema;

              break;
            case "array":
              if (attributeSchema.items) {
                const attributeSchemaItemDefinition = attributeSchema.items.$ref
                  ? that.getDefinition(
                      definitions,
                      attributeSchema.items.$ref,
                      attributeSchema.items
                    )
                  : attributeSchema.items;
                if (attributeSchemaItemDefinition) {
                  if (attributeSchemaItemDefinition.oneOf || attributeSchemaItemDefinition.anyOf) {
                    // definition via oneOf or anyOf
                    const oneOf = attributeSchemaItemDefinition.oneOf
                      ? attributeSchemaItemDefinition.oneOf
                      : [];
                    const anyOf = attributeSchemaItemDefinition.anyOf
                      ? attributeSchemaItemDefinition.anyOf
                      : [];
                    const items = oneOf.concat(anyOf);
                    if (items && items.length) {
                      items.forEach(item => {
                        attributesByPath = that.build(
                          item.properties,
                          definitions,
                          item.dependencies,
                          path,
                          attributesByPath
                        );
                      });
                    }
                  } else {
                    // primitive definition
                    // standard definition
                    const attributeSchemaItemProperties = attributeSchemaItemDefinition.properties;
                    const attributeSchemaItemDependencies =
                      attributeSchemaItemDefinition.dependencies;
                    attributesByPath = that.build(
                      attributeSchemaItemProperties,
                      definitions,
                      attributeSchemaItemDependencies,
                      path,
                      attributesByPath
                    );
                  }

                  // store parent attribute
                  const parentDefinition = Object.assign({}, attributeSchema);
                  parentDefinition.items = attributeSchemaItemDefinition;
                  // store parent attribute
                  attributesByPath[path] = parentDefinition;
                }
              }
              break;
            default:
              // store all attributes
              attributesByPath[path] = attributeSchema;
          }
        }
      });
    }

    // dependencies may contain new definitions
    if (dependencies) {
      const queue = [];

      Object.keys(dependencies).forEach(attribute => {
        const dependencyAttribute = dependencies[attribute];

        if (dependencyAttribute.anyOf) {
          queue.push(...dependencyAttribute.anyOf);
        } else if (dependencyAttribute.oneOf) {
          queue.push(...dependencyAttribute.oneOf);
        } else if (dependencyAttribute.allOf) {
          queue.push(...dependencyAttribute.allOf);
        } else {
          queue.push(dependencyAttribute);
        }
      });

      queue.forEach(definition => {
        if (definition.properties) {
          Object.keys(definition.properties).forEach(property => {
            const path = parent ? `${parent}.${property}` : property;

            // dereference attribute schema if necessary
            const initialSchema = definition.properties[property];
            const attributeSchema = initialSchema.$ref
              ? that.getDefinition(definitions, initialSchema.$ref, initialSchema)
              : initialSchema;

            // processing
            const hasNestedProperties = !!(attributeSchema && attributeSchema.properties);
            const hasBeenProcessed = !!attributesByPath[path];
            const process = hasNestedProperties || !hasBeenProcessed;

            if (process) {
              // hold the original value if processing children but current value exists already
              const currentAttributeSchema = attributesByPath[path];

              const singlePropertySchema = {};
              singlePropertySchema[property] = attributeSchema;

              attributesByPath = that.build(
                singlePropertySchema,
                definitions,
                null,
                parent,
                attributesByPath
              );

              if (hasBeenProcessed) {
                // return parent to original value not a partial schema for an e.g. anyOf
                attributesByPath[path] = currentAttributeSchema;
              }
            }
          });
        }
      });
    }

    return attributesByPath;
  }

  /**
   * Resolve a definition
   *
   * @param {array} definitions all schema definitions
   * @param {string} ref a reference to lookup in definitions
   * @param {object} subSchema the definiton including reference
   *
   * @return {object} schema after resolution
   */
  getDefinition(definitions, ref, subSchema) {
    if (definitions && ref && subSchema && ref.startsWith("#/definitions/")) {
      const key = ref.replace("#/definitions/", "");
      const definition = definitions[key];

      if (definition) {
        const cloned = JSON.parse(JSON.stringify(definition));
        if (subSchema.title) {
          cloned.title = subSchema.title;
        }

        return cloned;
      }
    }
  }

  /**
   * Get the type of a given path
   *
   * @param {string} path
   *
   * @return {string} type
   */
  getType(path) {
    return this.getAttributeBy(path, "type");
  }

  /**
   * Get the title of a given path
   *
   * @param {string} path
   *
   * @return {string} title
   */
  getTitle(path) {
    return this.getAttributeBy(path, "title");
  }

  /**
   * Get all parent titles and the title of a given path
   *
   * @param {string} path
   *
   * @return {array} titles
   */
  getTitles(path) {
    const paths = this.getPaths(path);
    const titles = [];

    paths.forEach(path => {
      const title = this.getTitle(path);
      if (title) {
        titles.push(title);
      }
    });

    if (titles.length === paths.length) {
      return titles;
    }

    return null;
  }

  /**
   * Get the format of a given path
   *
   * @param {string} path
   *
   * @return {string} format
   */
  getFormat(path) {
    return this.getAttributeBy(path, "format");
  }

  /**
   * Get an attribute for a given path
   *
   * @param {string} path
   * @param {string} attribute
   *
   * @return {string} value of the attribute
   */
  getAttributeBy(path, attribute) {
    if (path && attribute && this.attributesByPath) {
      const definition = this.attributesByPath[path];
      if (definition) {
        if (definition[attribute]) {
          return definition[attribute];
        }
      }
    }
    return null;
  }

  /**
   * Get a map of all paths for a given attribute or attributes
   *
   * @param {string} path
   * @param {*} attribute or array of attributes
   *
   * @return {object} map of path to an object of attribute to attribute value
   */
  getMapBy(attribute) {
    if (attribute && this.attributesByPath) {
      const map = {};

      Object.keys(this.attributesByPath).forEach(path => {
        if (Array.isArray(attribute)) {
          attribute.forEach(attributeEntry => {
            if (!map[path]) {
              map[path] = {};
            }
            // use getters where available to support dynamic/virtual attributes e.g. titles
            const fn = `get${attributeEntry
              .substring(0, 1)
              .toUpperCase()}${attributeEntry.substring(1, attributeEntry.length)}`;
            if (typeof this[fn] === "function") {
              map[path][attributeEntry] = this[fn](path);
            } else {
              const value = this.getAttributeBy(path, attribute);
              if (value) {
                map[path][attributeEntry] = value;
              }
            }
          });
        } else {
          // use getters where available to support dynamic/virtual attributes e.g. titles
          const fn = `get${attribute.substring(0, 1).toUpperCase()}${attribute.substring(
            1,
            attribute.length
          )}`;
          if (typeof this[fn] === "function") {
            map[path] = this[fn](path);
          } else {
            const value = this.getAttributeBy(path, attribute);
            if (value) {
              map[path] = value;
            }
          }
        }
      });
      return map;
    }

    return null;
  }

  /**
   * Given a path, return the path hierarchy e.g.
   *
   * user.address.postcode
   *
   * user
   * user.address
   * user.address.postcode
   *
   * @param {string} path
   *
   * @return {array} paths
   */
  getPaths(path) {
    if (path) {
      const paths = [];

      const segments = path.split(".");
      const accumulator = [];
      segments.forEach(segment => {
        accumulator.push(segment);
        paths.push(accumulator.join("."));
      });
      return paths;
    }

    return null;
  }

  getAttributes() {
    return Object.keys(this.attributesByPath);
  }

  getPathsWith(attribute, value) {
    const paths = {};

    Object.keys(this.attributesByPath).forEach(path => {
      const definition = this.attributesByPath[path];
      if (definition) {
        if (definition.items && (definition.items.oneOf || definition.items.anyOf)) {
          const oneOf = definition.items.oneOf || [];
          const anyOf = definition.items.anyOf | [];
          const items = [...oneOf, ...anyOf];
          items.forEach(item => {
            if (
              Object.prototype.hasOwnProperty.call(item, attribute) &&
              (!value || value === item[attribute])
            ) {
              paths[path] = item;
            }
          });
        } else {
          if (
            Object.prototype.hasOwnProperty.call(definition, attribute) &&
            (!value || value === definition[attribute])
          ) {
            paths[path] = definition;
          }
        }
      }
    });

    return paths;
  }

  getSchema(path) {
    if (path && this.attributesByPath) {
      const definition = this.attributesByPath[path];
      if (definition) {
        return definition;
      }
    }
    return null;
  }
}

export default SchemaExplorer;
