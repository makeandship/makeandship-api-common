import pointer from "jsonpointer";

const parse = (schema, path = "") => {
  if (pointer.get(schema, `${path}/$ref`)) {
    resolveReference(schema, path);
    return schema;
  }

  const subschema = pointer.get(schema, path) ?? "";

  Object.keys(subschema).forEach(k => {
    const v = subschema[k];
    switch (typeof v) {
      case "object":
        if (v) {
          parse(schema, `${path}/${k}`);
        }
        break;
      case "array":
        v.forEach((_, i) => parse(schema, `${path}/${i}`));
    }
  });

  return schema;
};

const resolveReference = (schema, path) => {
  const ref = pointer.get(schema, `${path}/$ref`).split("#")[1];
  parse(schema, ref);
  const def = pointer.get(schema, ref);
  pointer.set(schema, path, def);
};

export default { parse };
