import flat from "flat";

import coercer from "./coercer";

const normalizeIndexedAttributes = object => {
  const body = Object.assign({}, object);

  Object.keys(body).forEach(key => {
    const newKey = key.includes("[") ? key.replace(/\[(\d+)\]/, ".$1") : key;
    body[newKey] = body[key];
    if (newKey !== key) {
      delete body[key];
    }
  });

  return body;
};

const normalizeFlatAtributes = object => {
  const body = Object.assign({}, object);

  return flat.unflatten(body);
};

/**
 * Create a standard json object to work with where
 *
 * - flattened attributes are nested
 * - indexed attributes are arrays
 * - formatted objects are coerced into natural form
 *
 * @param {*} schema
 * @param {*} object
 *
 * @return {*} object
 */
const normalize = (schema, object) => {
  const body = Object.assign({}, object);
  const indexed = normalizeIndexedAttributes(body);
  const nested = normalizeFlatAtributes(indexed);

  coercer.coerce(schema, nested);

  return nested;
};

const normalizer = Object.freeze({
  normalize
});

export default normalizer;
