import _ from "lodash";

import Generator from "./generator";

class ChoiceGenerator extends Generator {
  generate(choices = []) {
    if (choices && choices.length) {
      return _.sample(choices);
    } else if (this.definition && this.definition.enum) {
      return _.sample(this.definition.enum);
    }
    return null;
  }
}

export default ChoiceGenerator;
