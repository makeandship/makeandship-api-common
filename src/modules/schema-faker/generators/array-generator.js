import Generator from "./generator";
import DataGeneratorFactory from "./data-generator-factory";

import { getRandomArrayLength } from "../utils";

class ArrayGenerator extends Generator {
  generate() {
    const data = [];

    const factory = new DataGeneratorFactory();
    const size = getRandomArrayLength();

    const property = this.property;
    const definition = this.definition;
    [...Array(size)].forEach(() => {
      const generator = factory.create(property, definition.items);
      data.push(generator.generate());
    });

    return data;
  }
}

export default ArrayGenerator;
