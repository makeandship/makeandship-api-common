import faker from "faker";

import Generator from "./generator";

class IDGenerator extends Generator {
  generate() {
    return faker.random.uuid();
  }
}

export default IDGenerator;
