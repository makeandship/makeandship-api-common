import ArrayGenerator from "./array-generator";
import ObjectGenerator from "./object-generator";
import BooleanGenerator from "./boolean-generator";
import ChoiceGenerator from "./choice-generator";
import NumberGenerator from "./number-generator";
import StringGenerator from "./string-generator";

class DataGeneratorFactory {
  create(property, definition) {
    switch (definition.type) {
      case "object":
        return new ObjectGenerator(property, definition);
      case "array":
        return new ArrayGenerator(property, definition);
      case "string":
        if (definition.enum) {
          return new ChoiceGenerator(property, definition);
        } else {
          return new StringGenerator(property, definition);
        }
      case "number":
        return new NumberGenerator(property, definition);
      case "boolean":
        return new BooleanGenerator(property, definition);
      default:
        if (definition.enum) {
          return new ChoiceGenerator(property, definition);
        } else {
          return new StringGenerator(property, definition);
        }
    }
  }
}

export default DataGeneratorFactory;
