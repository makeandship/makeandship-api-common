import faker from "faker";

import Generator from "./generator";
import { getRandomInt } from "../utils";

class DateGenerator extends Generator {
  generate() {
    const property = this.property;

    // likely future dates - end and stop
    if (this.isLikelyFuture(property)) {
      return this.format(faker.date.past());
    } else if (this.isLikelyPast(property)) {
      // likely future dates - birth, start and death
      if (this.isLikelyRecentPast(property)) {
        return this.format(faker.date.past(getRandomInt(1, 5)));
      } else if (this.isLikelyDistantPast(property)) {
        return this.format(faker.date.past(getRandomInt(12, 80)));
      } else {
        return this.format(faker.date.past(1));
      }
    } else {
      // all others in the past
      return this.format(faker.date.past(1));
    }
  }

  format(date, dateOnly = false) {
    if (date) {
      const iso = date.toISOString();
      if (dateOnly) {
        return iso.substring(0, 10);
      } else {
        return iso;
      }
    }
    return null;
  }

  isLikelyFuture(property) {
    return this.isLikely(property, ["stop", "end"]);
  }
  isLikelyPast(property) {
    return this.isLikely(property, ["start", "birth", "death"]);
  }
  isLikelyDistantPast(property) {
    return this.isLikely(property, ["birth"]);
  }
  isLikelyRecentPast(property) {
    return this.isLikely(property, ["death"]);
  }
}

export default DateGenerator;
