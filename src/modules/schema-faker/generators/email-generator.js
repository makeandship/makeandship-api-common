import faker from "faker";

import Generator from "./generator";

class EmailGenerator extends Generator {
  generate() {
    return faker.internet.email();
  }
}

export default EmailGenerator;
