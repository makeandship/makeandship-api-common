import faker from "faker";

import Generator from "./generator";
import IDGenerator from "./id-generator";
import DateGenerator from "./date-generator";
import FilenameGenerator from "./filename-generator";
import EmailGenerator from "./email-generator";
import FirstnameGenerator from "./firstname-generator";
import LastnameGenerator from "./lastname-generator";
import NHSNumberGenerator from "./nhs-number-generator";
import PhoneGenerator from "./phone-generator";
import URLGenerator from "./url-generator";

class StringGenerator extends Generator {
  generate() {
    const property = this.property;
    const definition = this.definition;
    if (definition) {
      if (definition.format) {
        switch (definition.format) {
          case "date":
            return new DateGenerator(property, definition).generate(true);
          case "date-time":
            return new DateGenerator(property, definition).generate(false);
          case "email":
            return new EmailGenerator(this.property, this.definition).generate();
          case "url":
            return new URLGenerator(property, definition).generate();
          case "nhsNumber":
            return new NHSNumberGenerator(this.property, this.definition).generate();
        }
      } else {
        // non format strings - try and detect from field name
        if (this.isLikelyId(property)) {
          return new IDGenerator(property, definition).generate();
        } else if (this.isLikelyFile(property)) {
          return new FilenameGenerator(this.property, this.definition).generate();
        } else if (this.isLikelyEmail(property)) {
          return new EmailGenerator(property, definition).generate();
        } else if (this.isLikelyFirstname(property)) {
          return new FirstnameGenerator(this.property, this.definition).generate();
        } else if (this.isLikelyLastname(property)) {
          return new LastnameGenerator(this.property, this.definition).generate();
        } else if (this.isLikelyPhone(property)) {
          return new PhoneGenerator(property, definition).generate();
        } else {
          return faker.lorem.word();
        }
      }
    }
    return null;
  }

  isLikelyId(property) {
    return this.isLikelyByEnding(property, ["id"]);
  }
  isLikelyFile(property) {
    return this.isLikelyByEnding(property, ["file"]);
  }
  isLikelyEmail(property) {
    return this.isLikely(property, ["email"]);
  }
  isLikelyFirstname(property) {
    return this.isLikelyByEnding(property, ["firstname"]);
  }
  isLikelyLastname(property) {
    return this.isLikelyByEnding(property, ["lastname"]);
  }
  isLikelyPhone(property) {
    return this.isLikely(property, ["phone"]);
  }
}

export default StringGenerator;
