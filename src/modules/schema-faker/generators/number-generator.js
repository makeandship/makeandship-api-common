import faker from "faker";
import {
  getRandomLongitude,
  getRandomLatitude,
  getRandomAge,
  getRandomBMI,
  getRandomPercentage
} from "../utils";

import Generator from "./generator";

class NumberGenerator extends Generator {
  generate() {
    const property = this.property;
    const definition = this.definition;
    if (this.isLikelyLatitude(property)) {
      return getRandomLatitude();
    } else if (this.isLikelyLongitude(property)) {
      return getRandomLongitude();
    } else if (this.isLikelyPercentage(property)) {
      return getRandomPercentage();
    } else if (this.isLikelyAge(property)) {
      return getRandomAge(definition.minimum, definition.maximum);
    } else if (this.isLikelyBMI(property)) {
      return getRandomBMI();
    } else {
      if (this.definition.minimum || this.definition.maximum) {
        const options = {};
        if (this.definition.minimum) {
          options.min = this.definition.minimum;
        }
        if (this.definition.maximum) {
          options.max = this.definition.maximum;
        }
        const value = faker.random.number(options);
        return value;
      } else {
        return faker.random.number();
      }
    }
  }

  isLikelyLatitude(property) {
    return this.isLikely(property, ["latitude", "lat"]);
  }
  isLikelyLongitude(property) {
    return this.isLikely(property, ["longitude", "lng"]);
  }
  isLikelyPercentage(property) {
    return this.isLikely(property, ["percent"]);
  }
  isLikelyAge(property) {
    return this.isLikely(property, ["age"]);
  }
  isLikelyBMI(property) {
    return this.isLikely(property, ["bmi"]);
  }
}

export default NumberGenerator;
