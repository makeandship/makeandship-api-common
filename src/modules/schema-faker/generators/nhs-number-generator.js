import Generator from "./generator";

import { getRandomNHSNumber } from "../utils";

class NHSNumberGenerator extends Generator {
  generate() {
    return getRandomNHSNumber();
  }
}

export default NHSNumberGenerator;
