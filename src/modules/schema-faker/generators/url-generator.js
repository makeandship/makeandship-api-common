import faker from "faker";

import Generator from "./generator";

class URLGenerator extends Generator {
  generate() {
    return faker.internet.url();
  }
}

export default URLGenerator;
