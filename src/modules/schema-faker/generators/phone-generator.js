import faker from "faker";

import Generator from "./generator";

class PhoneGenerator extends Generator {
  generate() {
    return faker.phone.phoneNumber("078## ######");
  }
}

export default PhoneGenerator;
