import faker from "faker";

import Generator from "./generator";

class LastnameGenerator extends Generator {
  generate() {
    return faker.name.lastName();
  }
}

export default LastnameGenerator;
