class Generator {
  constructor(property, definition) {
    this.property = property;
    this.definition = definition;
  }
  generate() {
    return null;
  }
  isLikely(property, indicators = []) {
    if (property) {
      const lowerCaseProperty = property.toLowerCase();
      return indicators.some(indicator => lowerCaseProperty.includes(indicator));
    }
    return false;
  }
  isLikelyByEnding(property, indicators = []) {
    if (property) {
      const lowerCaseProperty = property.toLowerCase();
      return indicators.some(indicator => lowerCaseProperty.endsWith(indicator));
    }
    return false;
  }
}

export default Generator;
