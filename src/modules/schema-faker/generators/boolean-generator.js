import _ from "lodash";

import Generator from "./generator";

class BooleanGenerator extends Generator {
  generate() {
    return _.sample([true, false]);
  }
}

export default BooleanGenerator;
