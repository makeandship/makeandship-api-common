import faker from "faker";

import Generator from "./generator";

class FilenameGenerator extends Generator {
  generate() {
    return faker.system.fileName();
  }
}

export default FilenameGenerator;
