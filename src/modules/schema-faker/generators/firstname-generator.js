import faker from "faker";

import Generator from "./generator";

class FirstnameGenerator extends Generator {
  generate() {
    return faker.name.firstName();
  }
}

export default FirstnameGenerator;
