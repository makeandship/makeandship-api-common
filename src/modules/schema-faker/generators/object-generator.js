import Generator from "./generator";
import DataGeneratorFactory from "./data-generator-factory";

import SchemaExplorer from "../../jsonschema/schema-explorer";

class ObjectGenerator extends Generator {
  generate() {
    const data = {};

    const explorer = new SchemaExplorer(this.definition);
    const attributes = explorer.getAttributes();

    if (attributes) {
      // properties only i.e. attributes with no nesting
      const properties = attributes.filter(path => !path.includes("."));
      properties.forEach(property => {
        const definition = explorer.getSchema(property);
        const generator = new DataGeneratorFactory().create(property, definition);

        const value = generator.generate();

        data[property] = value;
      });
    }

    return data;
  }
}

export default ObjectGenerator;
