import DataGeneratorFactory from "./generators/data-generator-factory";

const sample = properties => {
  const data = {};

  Object.keys(properties).forEach(property => {
    const definition = properties[property];

    const generator = new DataGeneratorFactory().create(property, definition);
    const value = generator.generate();

    data[property] = value;
  });

  return data;
};

const schemaFaker = Object.freeze({
  sample
});

export default schemaFaker;
