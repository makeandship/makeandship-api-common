import faker from "faker";

/**
 * Generate a valid NHS number
 * https://www.datadictionary.nhs.uk/data_dictionary/attributes/n/nhs/nhs_number_de.asp?shownav=1
 *
 * Note remainder of 10 is invalid and discarded
 */
const getRandomNHSNumber = () => {
  const nhsNumberBase = faker.helpers.replaceSymbolWithNumber("#########");
  const nhsNumberBaseAsArray = nhsNumberBase.split("");

  const remainder =
    nhsNumberBaseAsArray
      .map((digit, index) => {
        return digit * (11 - (index + 1));
      })
      .reduce((previous, current) => {
        return previous + current;
      }, 0) % 11;

  const check = 11 - remainder === 11 ? 0 : 11 - remainder;

  if (check === 10) {
    return getRandomNHSNumber();
  } else {
    return `${nhsNumberBase}${check}`;
  }
};

const getRandomFloat = (min, max) => Math.random() * (max - min) + min;

const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min;

const getRandomAge = (minimum, maximum) => {
  const min = minimum ? minimum : 10;
  const max = maximum ? maximum : 80;

  return getRandomInt(min, max);
};

const getRandomBMI = () => getRandomInt(1600, 3200) / 100;

const getRandomArrayLength = () => getRandomInt(1, 5);

const getRandomPercentage = () => getRandomInt(100, 10000) / 100;

const getRandomLongitude = () => getRandomFloat(-180, 180);

const getRandomLatitude = () => getRandomFloat(-90, 90);

export {
  getRandomFloat,
  getRandomInt,
  getRandomNHSNumber,
  getRandomAge,
  getRandomBMI,
  getRandomArrayLength,
  getRandomPercentage,
  getRandomLongitude,
  getRandomLatitude
};
