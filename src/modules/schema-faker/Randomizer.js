import flatten from "flat";

import DataGeneratorFactory from "./generators/data-generator-factory";

import SchemaExplorer from "../jsonschema/schema-explorer";

class Randomizer {
  constructor(schema, options = {}) {
    if (!schema) {
      throw Error("Please provide a valid schema");
    }

    this.schema = schema;
    this.options = options;
  }

  sample() {
    const data = {};

    const dgf = new DataGeneratorFactory();
    const explorer = new SchemaExplorer(this.schema);
    const attributes = explorer.getAttributes();

    if (attributes) {
      // properties only i.e. attributes with no nesting
      const properties = attributes.filter(path => !path.includes("."));
      properties.forEach(property => {
        const propertySchema = explorer.getSchema(property);
        const generator = dgf.create(property, propertySchema);
        const value = generator.generate();

        data[property] = value;
      });
    }

    if (this.options && this.options.overrides) {
      return this.override(data, this.options.overrides);
    }

    return data;
  }

  override(data, overrides) {
    const flatData = flatten(data);
    const flatDataWithLeafArrays = this.createArraysAtLeafNodes(flatData);

    const flatOverrides = flatten(overrides);
    // preserve order of overrides by running each individually
    Object.keys(flatOverrides).forEach(flatOverride => {
      const orderedOverride = {};
      orderedOverride[flatOverride] = flatOverrides[flatOverride];

      Object.keys(flatDataWithLeafArrays).forEach(key => {
        const value = flatDataWithLeafArrays[key];

        // strip out any array indexes to match in overrides
        const overrideKey = key
          .split(".")
          .filter(part => isNaN(part))
          .join(".");

        const override = orderedOverride[overrideKey];
        if (override) {
          switch (typeof override) {
            case "function":
              {
                const overriden = override.call(
                  this,
                  this.schema,
                  key,
                  flatten.unflatten(flatDataWithLeafArrays),
                  value
                );
                flatDataWithLeafArrays[key] = overriden;
              }
              break;
            default:
              flatDataWithLeafArrays[key] = override;
              break;
          }
        }
      });
    });

    const overridenData = flatten.unflatten(flatDataWithLeafArrays);
    return overridenData;
  }

  /**
   * Given an object with flattened keys i.e. using dot notation, recognise
   * arrays held at leaf nodes and turn them back into arrays
   *
   * e.g.
   *
   * "user.interests.0": "Football",
   * "user.interests.1": "Darts"
   *
   * becomes
   *
   * user.interests: ["Football", "Darts"]
   *
   * @param {*} flatData
   * @return {*} flatData with leaf nodes turned into arrays
   */
  createArraysAtLeafNodes(flatData) {
    const adjusted = {};
    if (flatData) {
      const keys = Object.keys(flatData).sort();
      keys.forEach(key => {
        // match .<number> at the end of a string
        const matches = /(^.*)\.(\d$)/.exec(key);
        if (matches) {
          const base = matches[1];
          if (!adjusted[base]) {
            adjusted[base] = [];
          }
          adjusted[base].push(flatData[key]);
        } else {
          adjusted[key] = flatData[key];
        }
      });
    }
    return adjusted;
  }
}

export default Randomizer;
