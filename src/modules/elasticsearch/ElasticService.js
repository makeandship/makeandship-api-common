import deepmerge from "deepmerge";
import IndexService from "./IndexService";
import MappingGenerator from "./MappingGenerator";
import MappingServiceFactory from "./MappingServiceFactory";
import SearchService from "./SearchService";
import StatsService from "./StatsService";

/**
 * A service to handle elasticsearch calls
 */
class ElasticService {
  /**
   * The constructor
   * @param {*} config
   * @param {*} schema
   * @param {*} options
   */
  constructor(config, schema, options = {}) {
    this.schema = schema;
    this.config = config;
    this.options = options;

    const indexOptions = options.index ? options.index : null;
    this.indexService = new IndexService(config, indexOptions);

    const searchOptions = options.search ? options.search : null;
    this.searchService = new SearchService(config, searchOptions);

    const statsOptions = options.stats ? options.stats : null;
    this.statsService = new StatsService(config, statsOptions);

    const mappingOptions = options.mapping ? options.mapping : null;
    this.mappingGenerator = new MappingGenerator(config, schema, mappingOptions);
  }

  /**
   * Delete the index
   */
  deleteIndex() {
    return this.indexService.deleteIndex();
  }

  /**
   * Create the index with mapping
   * @param {*} options
   */
  async createIndex(options = {}) {
    const mappingOptions = this.options.mapping ? this.options.mapping : null;
    const mappingService = await MappingServiceFactory.get(
      this.config,
      this.schema,
      mappingOptions
    );
    if (mappingService) {
      const mappingJson = await mappingService.getMapping();
      const body = deepmerge(mappingJson, options);
      return this.indexService.createIndex(body);
    }
    return null;
  }

  /**
   * Index one document to ES
   * @param {*} document
   */
  async indexDocument(document) {
    const body = typeof document.toJSON === "function" ? document.toJSON() : document;
    const indexed = await this.indexService.indexDocument({
      index: `${this.config.index}-${this.config.type}`,
      type: "_doc",
      id: document.id,
      body
    });

    return indexed;
  }

  /**
   * Update one document in ES
   * @param {*} document
   */
  async updateDocument(document) {
    const doc = typeof document.toJSON === "function" ? document.toJSON() : document;
    const updated = await this.indexService.updateDocument({
      index: `${this.config.index}-${this.config.type}`,
      id: document.id,
      type: "_doc",
      body: {
        doc
      }
    });

    return updated;
  }

  /**
   * Bulk indexing
   * @param {*} documents
   */
  async indexDocuments(documents) {
    if (documents && documents.length) {
      const body = [];

      documents.map(document =>
        body.push(
          {
            index: {
              _index: `${this.config.index}-${this.config.type}`,
              _type: "_doc",
              _id: document.id
            }
          },
          typeof document.toJSON === "function" ? document.toJSON() : document
        )
      );

      const result = await this.indexService.indexDocuments(body);
      return result;
    }

    return null;
  }

  /**
   * Delete one document from ES
   * @param {*} id
   */
  async deleteDocument(id) {
    const deleted = await this.indexService.deleteDocument({
      index: `${this.config.index}-${this.config.type}`,
      type: "_doc",
      id
    });

    return deleted;
  }

  /**
   * Run the search
   * @param {*} searchQuery
   * @param {*} options
   */
  search(searchQuery, options) {
    return this.searchService.search(searchQuery, options);
  }

  /**
   * Run a scroll
   * @param {*} scrollId
   * @param {*} options
   */
  scroll(searchQuery, options) {
    return this.searchService.scroll(searchQuery, options);
  }

  /**
   * Get document by id
   * @param {*} id
   */
  get(id) {
    return this.searchService.get(id);
  }

  /**
   * Return a count of documents in this index
   */
  async count(query = null, options = null) {
    const count = await this.statsService.count(query, options);
    return count;
  }

  /**
   *
   */
  async getLiveMapping() {
    const mappingOptions = this.options.mapping ? this.options.mapping : null;
    const mappingService = await MappingServiceFactory.get(
      this.config,
      this.schema,
      mappingOptions
    );
    if (mappingService) {
      const mapping = await mappingService.getLiveMapping();
      return mapping;
    }
    return null;
  }
}

export default ElasticService;
