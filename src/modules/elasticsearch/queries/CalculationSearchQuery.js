import esb from "elastic-builder";
import MetricSearchQuery from "./MetricSearchQuery";
import SearchUtil from "../../jsonschema/util/search";

/**
 * Create aggregation search query
 */
class CalculationSearchQuery extends MetricSearchQuery {
  /**
   * The constructor
   * @param {*} filters
   */
  constructor(filters, schema, type, options) {
    super(filters, schema, type, options);
  }

  buildSearchQuery(filters) {
    super.buildSearchQuery(filters); // populates this.body

    if (!this.searchUtil) {
      this.searchUtil = new SearchUtil(this.schema);
    }
    const { choices, summables } = this.searchUtil.getCalculationFilters();

    // aggregations
    for (const field of choices) {
      if (!this.getExcludeTerm(field)) {
        const terms = esb.termsAggregation(field, field);
        const termSize = this.getTermSize(field);
        if (termSize !== null) {
          terms.size(termSize);
        }
        for (const summable of summables) {
          terms.agg(esb.sumAggregation(`sum_${summable}`, summable));
          terms.agg(esb.avgAggregation(`avg_${summable}`, summable));
        }
        this.body.agg(terms);
      }
    }

    // summables
    for (const summable of summables) {
      if (!this.getExcludeTerm(summable)) {
        this.body.agg(esb.sumAggregation(`sum_${summable}`, summable));
        this.body.agg(esb.avgAggregation(`avg_${summable}`, summable));
      }
    }
  }
}

export default CalculationSearchQuery;
