import esb from "elastic-builder";
import MetricSearchQuery from "./MetricSearchQuery";
import SearchUtil from "../../jsonschema/util/search";

/**
 * Create aggregation search query
 */
class AggregationSearchQuery extends MetricSearchQuery {
  /**
   * The constructor
   * @param {*} filters
   */
  constructor(filters, schema, type, options) {
    super(filters, schema, type, options);
  }

  buildSearchQuery(filters) {
    super.buildSearchQuery(filters); // populates this.body

    this.searchUtil = new SearchUtil(this.schema);
    const { choices, ranges } = this.searchUtil.getRangeFilters();

    // aggregate terms
    for (const field of choices) {
      if (!this.getExcludeTerm(field)) {
        const terms = esb.termsAggregation(field, field);
        const termSize = this.getTermSize(field); // from options
        if (termSize !== null) {
          terms.size(termSize);
        } else {
          const defaultTermSize = this.getDefaultTermSize(field); // from schema
          if (defaultTermSize !== null) {
            terms.size(defaultTermSize);
          }
        }
        this.body.agg(terms);
      }
    }

    // add min and max for ranges
    for (const field of ranges) {
      if (!this.getExcludeTerm(field)) {
        this.body.agg(esb.maxAggregation(`max_${field}`, field));
        this.body.agg(esb.minAggregation(`min_${field}`, field));
      }
    }
  }
}

export default AggregationSearchQuery;
