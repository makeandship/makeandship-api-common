class SearchQueryDecorator {
  decorate(query) {
    return query;
  }
}

export default SearchQueryDecorator;
