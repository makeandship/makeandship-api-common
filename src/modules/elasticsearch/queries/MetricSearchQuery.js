import SearchQuery from "./SearchQuery";

import SearchUtil from "../../jsonschema/util/search";

class MetricSearchQuery extends SearchQuery {
  /**
   * The constructor
   * @param {*} filters
   */
  constructor(filters, schema, type, options) {
    super(filters, schema, type, options);
    if (!this.per) {
      this.setSize(0);
    }
  }

  /**
   * This search query requires multiple searches
   * - required if there are filters
   */
  isMulti() {
    return this.hasFilters();
  }

  /**
   * Template method to add a child search
   */
  buildChildSearches(clazz, searches = {}) {
    const keys = Object.keys(this.filters);

    for (const key of keys) {
      if (this.getKeyType(key) !== "range") {
        const subFilters = JSON.parse(JSON.stringify(this.filters));
        delete subFilters[key];

        // inline to avoid circular dependencies caused when using a factory approach
        const Clazz = this.constructor;
        const query = new Clazz(subFilters, this.schema, this.type, this.options);

        query.setQ(this.q);
        query.setPage(this.page);
        query.setSort(this.sort);
        query.setOrder(this.order);
        query.setSize(this.size);
        query.setFilters(subFilters);

        searches[key] = query.toJSON(false);
      }
    }

    return searches;
  }

  /**
   * Check if options have overrides for a given term
   * @param {*} term
   */
  getExcludeTerm(term) {
    if (this.options && this.options.aggs && this.options.aggs[term]) {
      return !!this.options.aggs[term].exclude;
    }
    return false;
  }

  /**
   * Check if there is a specific size set for this term
   * @param {*} agg
   */
  getTermSize(term) {
    if (this.options && this.options.aggs && this.options.aggs[term]) {
      if (Object.keys(this.options.aggs[term]).includes("size")) {
        // incase value is 0 would be falsy
        return this.options.aggs[term].size;
      }
    }
    return null;
  }

  getDefaultTermSize(term) {
    if (term) {
      const searchUtil = new SearchUtil(this.schema);
      if (searchUtil) {
        const size = searchUtil.getEnumSize(term);
        return size;
      }
    }

    return null;
  }

  /**
   * Template method to process results
   * @param {*} children
   */
  processResults(results) {
    if (results && Object.keys(results).length) {
      const calculations = results.core;

      Object.keys(results).forEach(attribute => {
        if (results[attribute].aggregations) {
          calculations.aggregations[attribute] = results[attribute].aggregations[attribute];
        }
      });

      return calculations;
    }

    return null;
  }

  /**
   * Build a json object with keys for filter and values of searches
   */
  toJSON(children = true) {
    const core = super.toJSON();
    if (this.isMulti() && children) {
      const searches = {
        core
      };
      const clazz = this.constructor.name;
      this.buildChildSearches(clazz, searches);

      return searches;
    } else {
      return core;
    }
  }
}

export default MetricSearchQuery;
