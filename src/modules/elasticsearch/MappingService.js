import dot from "dot-prop";
import elasticsearch from "elasticsearch";

import SchemaExplorer from "../jsonschema/schema-explorer";
import SchemaParser from "../jsonschema/schema-parser";
import JSONSchemaUtils from "../jsonschema/util";

/**
 * A service responsible for generating the mapping
 */
class MappingService {
  /**
   * The constructor
   * @param {*} config
   * @param {*} schema
   * @param {*} options
   */
  constructor(config, schema, options) {
    this.type = config.type;
    this.index = `${config.index}-${config.type}`;
    this.schema = schema;
    this.options = options;
    this.explorer = new SchemaExplorer(this.schema);
    this.mappings = {};

    this.client = new elasticsearch.Client({
      host: config.host,
      log: config.log
    });
  }

  /**
   * Check cache and create the mapping if not available
   */
  getMapping() {
    if (this.schema) {
      const id = this.schema["$id"];
      if (this.mappings[id]) {
        return this.mappings[id];
      } else {
        const parsableSchema = JSON.parse(JSON.stringify(this.schema));
        const parsedSchema = SchemaParser.parse(parsableSchema);
        const mapping = this.generateJsonMapping({
          type: this.type,
          jsonschema: parsedSchema,
          additionalFields: ["location"]
        });

        // add overrides where necessary
        if (this.options && this.options.overrides) {
          this.overrideMapping(mapping, this.options.overrides);
        }

        this.mappings[id] = mapping;

        return mapping;
      }
    }
  }

  /**
   * Override mappings with options
   * @param {*} mapping
   * @param {*} overrides
   */
  overrideMapping(mapping, overrides) {
    if (mapping) {
      if (overrides.keyword) {
        this.overrideMappingKeywords(mapping, overrides.keyword);
      }
    }
  }

  /**
   * Override mappings with options
   * @param {*} mapping
   * @param {*} keyword
   */
  overrideMappingKeywords(mapping, keyword) {
    if (mapping && keyword) {
      // apply keywords
      keyword.forEach(item => {
        const propertiesPath = item.includes(".") ? item.split(".").join(".fields.") : item;
        const path = `mappings.properties.${propertiesPath}`;
        const existing = dot.get(mapping, path);
        if (existing) {
          if (existing.type !== "keyword") {
            existing.type = "keyword";
            existing.copy_to = ["copied", "copied_ngrams"];
            existing.index = true;
            dot.set(mapping, path, existing);
          }
        } else {
          dot.set(mapping, path, {
            type: "keyword",
            copy_to: ["copied", "copied_ngrams"],
            index: true
          });
        }
      });
    }
  }

  /**
   * Generate the json mapping
   * @param {*} options
   */
  generateJsonMapping(options) {
    const { filter: synonymFilter, analyzer: synonymAnalyzer } = this.createSynonymAnalysis();
    const mapping = {
      mappings: {},
      settings: {
        analysis: {
          normalizer: {
            lowercase_normalizer: {
              type: "custom",
              filter: ["lowercase"]
            }
          },
          filter: {
            ngrams_filter: {
              type: "ngram",
              min_gram: "4",
              max_gram: "15"
            },
            ...synonymFilter
          },
          analyzer: {
            ngrams_analyzer: {
              filter: ["lowercase", "ngrams_filter"],
              type: "custom",
              tokenizer: "whitespace"
            },
            lowercase_analyzer: {
              filter: ["lowercase"],
              type: "custom",
              tokenizer: "whitespace"
            },
            ...synonymAnalyzer
          }
        }
      }
    };
    const properties = this.createProperties(
      options.jsonschema.properties,
      options.additionalFields
    );
    properties.copied_ngrams = {
      type: "text",
      analyzer: "ngrams_analyzer",
      index: true
    };
    properties.copied = {
      type: "text",
      analyzer: "lowercase_analyzer",
      index: true
    };

    return { mapping, properties };
  }

  /**
   * Create properties based on the object type and format.
   * @param {*} schema
   * @param {*} additionalFields
   */
  createProperties(schema, additionalFields = []) {
    const properties = {};

    const attributes = this.explorer.getAttributes();
    if (attributes) {
      const sorted = attributes.sort();
      sorted.forEach(path => {
        const parts = path.includes(".") ? path.split(".") : [path];
        const propertiesPath = parts.join(".properties.");
        const key = parts.slice(-1).pop();

        // create an empty property if needed
        if (!dot.has(properties, propertiesPath)) {
          dot.set(properties, propertiesPath, {});
        }

        const object = this.explorer.getSchema(path);
        if (
          JSONSchemaUtils.isTypeValid(object.type, "string") &&
          (object.enum || additionalFields.indexOf(key) > -1 || object.keyword)
        ) {
          dot.set(properties, propertiesPath, {
            type: "keyword",
            copy_to: ["copied", "copied_ngrams"],
            index: true
          });
          if (object.enumNames && object.enumNamesSearchAttribute) {
            const enumPropertiesPath = propertiesPath.replace(
              `.${key}`,
              `.${object.enumNamesSearchAttribute}`
            );
            dot.set(properties, enumPropertiesPath, {
              type: "keyword",
              copy_to: ["copied", "copied_ngrams"],
              index: true
            });
          }
          if (
            (object.synonyms && object.synonyms.length) ||
            (object.useSynonyms && object.enum && object.enumNames)
          ) {
            const name = object.title.toLowerCase().replace(/ /g, "_");

            dot.set(properties, `${propertiesPath}.fields.raw`, {
              type: "text",
              analyzer: `${name}_text`
            });
          }
        } else if (
          JSONSchemaUtils.isTypeValid(object.type, "string") &&
          (object.format === "date-time" || object.format === "date")
        ) {
          dot.set(properties, propertiesPath, { type: "date", index: true });
        } else if (JSONSchemaUtils.isTypeValid(object.type, "string") && object.sortable) {
          dot.set(properties, propertiesPath, {
            type: "text",
            fields: { keyword: { type: "keyword" } }
          });
        } else if (
          JSONSchemaUtils.isTypeValid(object.type, "string") &&
          !object.enum &&
          additionalFields.indexOf(key) == -1
        ) {
          dot.set(properties, propertiesPath, {
            type: "text",
            copy_to: ["copied", "copied_ngrams"],
            index: true
          });
        } else if (JSONSchemaUtils.isTypeValid(object.type, "integer")) {
          dot.set(properties, propertiesPath, { type: "long", index: true });
        } else if (JSONSchemaUtils.isTypeValid(object.type, "number")) {
          dot.set(properties, propertiesPath, { type: "double", index: true });
        } else if (JSONSchemaUtils.isTypeValid(object.type, "boolean")) {
          dot.set(properties, propertiesPath, { type: "boolean", index: true });

          if (object.synonyms && object.synonyms.length) {
            const name = object.title.toLowerCase().replace(/ /g, "_");
            dot.set(properties, `${propertiesPath}.fields.raw`, {
              type: "text",
              analyzer: `${name}_text`
            });
          }
        } else if (JSONSchemaUtils.isTypeValid(object.type, "object")) {
          if (!object.properties) {
            dot.set(properties, propertiesPath, { type: "object" });
          } else {
            if (object.synonyms && object.synonyms.length) {
              object.synonyms.forEach(synonym => {
                if (synonym.indexOf("/") > -1) {
                  const key = synonym.split("/")[0].toLowerCase();
                  const value = synonym.split("/")[1];
                  if (object.properties[key]) {
                    object.properties[key].synonyms = [value];
                    object.properties[key].title = object.title;
                  }
                }
              });
            }
            dot.set(properties, `${propertiesPath}.properties`, {});
          }
        } else if (
          JSONSchemaUtils.isTypeValid(object.type, "array") &&
          object.items &&
          !object.items.properties
        ) {
          if (JSONSchemaUtils.isTypeValid(object.items.type, "object")) {
            dot.set(properties, propertiesPath, { type: "object" });
          } else {
            // set the type
            dot.set(properties, propertiesPath, { type: "text" });
          }
        }
      });
    }
    return properties;
  }

  /**
   * Create filters and analyzers based on the object synonyms.
   */
  createSynonymAnalysis() {
    const filter = {};
    const analyzer = {};
    const attributes = this.explorer.getAttributes();
    const sorted = attributes.sort();

    sorted.forEach(path => {
      const object = this.explorer.getSchema(path);
      if (object.synonyms && object.synonyms.length) {
        const name = object.title.toLowerCase().replace(/ /g, "_");
        const synonyms = object.synonyms.map(synonym => {
          if (synonym.indexOf("/") > -1) {
            return synonym.split("/")[1];
          }
          return synonym;
        });
        filter[`${name}_syn`] = {
          type: "synonym",
          synonyms
        };
        analyzer[`${name}_text`] = {
          tokenizer: "standard",
          filter: [`${name}_syn`]
        };
      }

      if (object.useSynonyms && object.enumNames && object.enum) {
        const name = object.title.toLowerCase().replace(/ /g, "_");
        const synonyms = [];
        for (let i = 0; i < object.enum.length; i++) {
          if (object.enum[i]) {
            synonyms.push(`${object.enum[i]}, ${object.enumNames[i]}`);
          }
        }

        filter[`${name}_syn`] = {
          type: "synonym",
          synonyms
        };
        analyzer[`${name}_text`] = {
          tokenizer: "standard",
          filter: [`${name}_syn`]
        };
      }
    });

    return { filter, analyzer };
  }

  /**
   * Cleanup empty properties
   * @param {*} properties
   * @param {*} key
   */
  cleanup(properties, key) {
    if (
      properties[key] &&
      properties[key].properties &&
      !Object.keys(properties[key].properties).length
    ) {
      delete properties[key];
    }
  }
}

export default MappingService;
