import MappingService from "./MappingService";

/**
 * A service responsible for generating the mapping for es V7
 */
class MappingServiceV7 extends MappingService {
  generateJsonMapping(options) {
    const { mapping, properties } = super.generateJsonMapping(options);
    mapping.settings.max_ngram_diff = 11;

    mapping.mappings = { properties };

    return mapping;
  }

  async getLiveMapping() {
    const mapping = await this.client.indices.getMapping({
      index: this.index
    });
    return mapping;
  }
}

export default MappingServiceV7;
