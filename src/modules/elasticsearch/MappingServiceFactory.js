import elasticsearch from "elasticsearch";
import compareVersions from "compare-versions";

import MappingServiceV6 from "./MappingServiceV6";
import MappingServiceV7 from "./MappingServiceV7";

class MappingServiceFactory {
  async get(config, schema, options) {
    const client = new elasticsearch.Client({
      host: config.host,
      log: config.log
    });

    const clientInfo = await client.info();
    const version = clientInfo.version.number;

    // less than version 6
    if (compareVersions(version, "6.0.0") === -1) {
      return null;
    }

    // version 6
    if (compareVersions(version, "7.0.0") === -1) {
      return new MappingServiceV6(config, schema, options);
    }

    // version 7+
    return new MappingServiceV7(config, schema, options);
  }
}

const factory = new MappingServiceFactory();
export default factory;
