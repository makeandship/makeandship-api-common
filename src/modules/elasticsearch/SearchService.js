import Promise from "bluebird";
import elasticsearch from "elasticsearch";

/**
 * A service responsible for generating the mapping
 */
class SearchService {
  /**
   * The constructor
   * @param {*} config
   * @param {*} options
   */
  constructor(config, options) {
    this.options = options;
    this.client = new elasticsearch.Client({
      host: config.host,
      log: config.log
    });
    this.index = config.type ? `${config.index}-${config.type}` : config.index;
    this.options = options;
  }

  buildSearch(search, options) {
    if (search) {
      const query = {
        body: search,
        index: this.index,
        ...options
      };
      return query;
    }
    return null;
  }

  buildScroll(scrollId, options) {
    if (scrollId) {
      return {
        scrollId,
        ...options
      };
    }

    return null;
  }

  /**
   * Run the search in ES
   */
  async search(query, options = {}) {
    if (query && query.isMulti() && query.hasFilters()) {
      const promises = {};

      const jsons = query.toJSON();
      const keys = Object.keys(jsons);
      for (const key of keys) {
        const json = jsons[key];
        const elasticQuery = this.buildSearch(json, options);
        promises[key] = this.client.search(elasticQuery);
      }

      const results = await Promise.props(promises);
      return query.processResults(results);
    } else {
      const json = query.toJSON();
      const elasticQuery = this.buildSearch(json, options);
      const results = await this.client.search(elasticQuery);
      return results;
    }
  }

  /**
   * Scroll a search
   * @param {*} id
   */
  async scroll(scrollId, options = { scroll: "1m" }) {
    if (!scrollId) {
      return null;
    }

    const elasticQuery = this.buildScroll(scrollId, options);
    const results = await this.client.scroll(elasticQuery);
    return results;
  }

  /**
   * Get a document by id
   */
  get(id) {
    return this.client.get({
      index: this.index,
      type: this.type || "_doc",
      id
    });
  }
}

export default SearchService;
