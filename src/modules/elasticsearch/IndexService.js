import elasticsearch from "elasticsearch";

/**
 * A service responsible for indexing the data to ES
 */
class IndexService {
  /**
   * The constructor
   * @param {*} config
   * @param {*} options
   */
  constructor(config, options) {
    this.config = config;
    this.client = new elasticsearch.Client({
      host: config.host,
      log: config.log
    });
    this.options = options;
  }

  /**
   * Check if the index exists in ES
   */
  indexExists() {
    return this.client.indices.exists({
      index: `${this.config.index}-${this.config.type}`
    });
  }

  /**
   * Delete index from ES
   */
  async deleteIndex() {
    const exists = await this.indexExists();
    if (exists) {
      return this.client.indices.delete({
        index: `${this.config.index}-${this.config.type}`
      });
    }
    return null;
  }

  /**
   * Create index in ES
   * @param {*} body
   */
  createIndex(body) {
    const indexOptions = Object.assign({
      index: `${this.config.index}-${this.config.type}`,
      body
    });

    return this.client.indices.create(indexOptions);
  }

  /**
   * Index a document to ES
   * @param {*} document
   */
  async indexDocument(document) {
    return await this.client.index(document);
  }

  /**
   * Update a document in ES
   * @param {*} document
   */
  async updateDocument(document) {
    try {
      await this.client.update(document);
    } catch (e) {
      // not found
      return e;
    }
  }

  /**
   * Delete a document from ES
   * @param {*} document
   */
  async deleteDocument(document) {
    try {
      await this.client.delete(document);
    } catch (e) {
      // not found
      return e;
    }
  }

  /**
   * Bulk indexing
   * @param {*} documents
   */
  async indexDocuments(documents) {
    return await this.client.bulk({ body: documents });
  }
}

export default IndexService;
