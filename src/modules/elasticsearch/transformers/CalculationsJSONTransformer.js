import ElasticsearchJSONTransformer from "./ElasticsearchJSONTransformer";
import AggregationsJSONTransformer from "./AggregationsJSONTransformer";

/**
 * Transform choice calculations from elasticsearch results
 */
class CalculationsJSONTransformer extends ElasticsearchJSONTransformer {
  /**
   * Convert aggregations into calculations containing either
   * - categories and counts for keywords
   * - sum and avg for summables (integers and numbers)
   *
   * @param {object} o a result
   */
  async transform(o, options = {}) {
    const res = super.transform(o, options);
    const { titles = {} } = options;

    const that = this;

    const aggregations = new AggregationsJSONTransformer().transform(res, options);
    // response object
    const calculations = {};
    if (aggregations) {
      const aggregationKeys = Object.keys(aggregations);
      for (let key of aggregationKeys) {
        const aggregation = aggregations[key];
        if (aggregation) {
          const isSummable = key.startsWith("avg_") || key.startsWith("sum_");
          const isSummary = !!aggregation.buckets;

          if (isSummable) {
            this.transformAggregation(calculations, key, aggregation);
          } else if (isSummary) {
            const buckets = aggregation.buckets;
            if (buckets) {
              // create container and set titles
              calculations[key] = {};

              if (titles && titles[key]) {
                calculations[key].title = titles[key].title;
                calculations[key].titles = titles[key].titles;
              }

              // add sub-aggregation calculations
              buckets.forEach(bucket => {
                const bucketValue = bucket.key;
                delete bucket["key"];
                delete bucket["doc_count"];

                if (!calculations[key]["values"]) {
                  calculations[key]["values"] = {};
                }
                if (!calculations[key][bucketValue]) {
                  calculations[key]["values"][bucketValue] = {};
                }
                for (const bucketKey of Object.keys(bucket)) {
                  const bucketAggregation = bucket[bucketKey];
                  that.transformAggregation(
                    calculations[key]["values"][bucketValue],
                    bucketKey,
                    bucketAggregation
                  );
                }
              });
            }
          }
        }
      }
    }

    return calculations;
  }

  transformAggregation(calculations, key, aggregation, titles) {
    if (calculations && key && aggregation) {
      // will have a min and max
      const fieldKey = key.substring(4);
      if (!calculations[fieldKey]) {
        calculations[fieldKey] = {};
      }
      if (titles && titles[fieldKey]) {
        calculations[fieldKey].title = titles[fieldKey].title;
        calculations[fieldKey].titles = titles[fieldKey].titles;
      }

      const type = key.startsWith("sum_") ? "sum" : "avg";
      calculations[fieldKey][type] = aggregation.value_as_string || aggregation.value;
    }
  }
}

export default CalculationsJSONTransformer;
