import ElasticsearchJSONTransformer from "./ElasticsearchJSONTransformer";

/**
 * Return hits from an elasticsearch search response
 */
class SuggestJSONTransformer extends ElasticsearchJSONTransformer {
  /**
   * Transform hits in search results
   *
   * @param {object} o the object to transform
   */
  transform(o, options) {
    const res = super.transform(o);
    const { suggest } = res;

    if (suggest[options.name] && suggest[options.name].length === 1) {
      return suggest[options.name][0].options;
    }

    return [];
  }
}

export default SuggestJSONTransformer;
