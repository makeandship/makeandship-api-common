import ElasticsearchJSONTransformer from "./ElasticsearchJSONTransformer";
import AggregationsJSONTransformer from "./AggregationsJSONTransformer";
import SchemaEnricher from "../../cache/SchemaEnricher";

/**
 * Transform choice ranges from elasticsearch results
 */
class ChoicesJSONTransformer extends ElasticsearchJSONTransformer {
  /**
   * Convert aggregations into choices containing either
   * - categories and counts for keywords
   * - min and max for ranges (numbers and dates)
   *
   * @param {object} o a result
   */
  async transform(o, options = {}) {
    const res = super.transform(o, options);
    const { titles = {}, formats = {} } = options;
    const aggregations = new AggregationsJSONTransformer().transform(res, options);
    // response object
    const choices = {};
    if (aggregations) {
      const aggregationKeys = Object.keys(aggregations);
      for (let key of aggregationKeys) {
        const aggregation = aggregations[key];
        if (aggregation) {
          const isRange = key.startsWith("max_") || key.startsWith("min_");
          if (isRange) {
            // will have a min and max
            const rangeKey = key.substring(4);
            if (!choices[rangeKey]) {
              choices[rangeKey] = {};
            }
            if (titles[rangeKey]) {
              choices[rangeKey].title = titles[rangeKey].title;
              choices[rangeKey].titles = titles[rangeKey].titles;
            }

            const type = key.startsWith("min_") ? "min" : "max";

            if (
              formats[rangeKey] &&
              (formats[rangeKey].format === "date" || formats[rangeKey].format === "date-time")
            ) {
              choices[rangeKey][type] = new Date(aggregation.value).toISOString();
            } else {
              choices[rangeKey][type] = aggregation.value_as_string || aggregation.value;
            }
          } else {
            const choice = [];

            // will have categories and counts
            const buckets = aggregation.buckets;

            if (Array.isArray(buckets)) {
              buckets.forEach(bucket => {
                const category = this.getBucketCategory(bucket);
                const count = bucket.doc_count;

                choice.push({
                  key: category,
                  count
                });
              });
            }

            choices[key] = {};
            if (titles[key]) {
              choices[key].title = titles[key].title;
              choices[key].titles = titles[key].titles;
            }

            // Enrich choice with description
            await SchemaEnricher.enrichChoices(key, choice);

            choices[key].choices = choice;
          }
        }
      }
    }

    return choices;
  }

  /**
   * Handle booleans keys
   * @param {*} bucket
   */
  getBucketCategory(bucket) {
    if (bucket.key === 0 && bucket.key_as_string === "false") {
      return false;
    } else if (bucket.key === 1 && bucket.key_as_string === "true") {
      return true;
    } else {
      return bucket.key;
    }
  }
}

export default ChoicesJSONTransformer;
