import ElasticService from "./ElasticService";
import AggregationSearchQuery from "./queries/AggregationSearchQuery";
import CalculationSearchQuery from "./queries/CalculationSearchQuery";
import SearchQuery from "./queries/SearchQuery";

export { ElasticService, AggregationSearchQuery, CalculationSearchQuery, SearchQuery };
export default Object.freeze({
  ElasticService,
  AggregationSearchQuery,
  CalculationSearchQuery,
  SearchQuery
});
