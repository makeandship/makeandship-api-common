import elasticsearch from "elasticsearch";

import CountSearchQuery from "./queries/CountSearchQuery";

/**
 * A service responsible for getting stats data data to ES
 */
class StatsService {
  /**
   * The constructor
   * @param {*} config
   * @param {*} options
   */
  constructor(config, options) {
    this.config = config;
    this.client = new elasticsearch.Client({
      host: config.host,
      log: config.log
    });
    this.options = options;
  }

  buildSearch(search = null, options = {}) {
    const index = `${this.config.index}-${this.config.type}`;
    const query = {
      body: search,
      type: `_doc`,
      index,
      ...options
    };

    return query;
  }

  async count(query = null, options = {}) {
    const countQuery = query ? new CountSearchQuery(query) : null;
    const elasticQuery = this.buildSearch(countQuery, options);
    const responses = await this.client.count(elasticQuery);
    if (responses && typeof responses.count !== "undefined") {
      return responses.count;
    }
    return 0;
  }
}

export default StatsService;
