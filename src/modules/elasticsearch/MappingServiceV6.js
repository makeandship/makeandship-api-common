import MappingService from "./MappingService";

/**
 * A service responsible for generating the mapping for es V6
 */
class MappingServiceV6 extends MappingService {
  generateJsonMapping(options) {
    const { mapping, properties } = super.generateJsonMapping(options);
    mapping.mappings[`${options.type}`] = { properties };

    return mapping;
  }
}

export default MappingServiceV6;
