import MappingServiceFactory from "./MappingServiceFactory";

/**
 * A service responsible for generating the mapping based on the es version
 */
class MappingGenerator {
  /**
   * The constructor
   * @param {*} config
   * @param {*} schema
   * @param {*} options
   */
  constructor(config, schema, options) {
    this.config = config;
    this.schema = schema;
    this.options = options;
  }

  /**
   * Check cache and create the mapping if not available
   */
  async generate() {
    const service = MappingServiceFactory.get(this.config, this.schema, this.options);

    if (service) {
      const mapping = service.getMapping();
      return mapping;
    } else {
      throw new Error("Elasticsearch version not supported");
    }
  }
}

export default MappingGenerator;
