import KeycloakAdmin from "keycloak-admin";
import KeycloakConnect from "keycloak-connect";
import axios from "axios";
import qs from "qs";
import PhoneHelper from "../../helpers/PhoneHelper";
import { ValidationError } from "../error";

/**
 * Merge arrays without duplicates
 */
const mergeArrays = (array1 = [], array2 = []) => {
  const array = [...array1, ...array2];
  const set = new Set(array); // remove duplicates
  return [...set];
};

class Keycloak {
  constructor(options) {
    /**
     * Model and transformer
     * Core keycloak configuration
     * Core attributes used in various calls
     */
    Object.assign(this, {
      userModel: options.userModel,
      userTransformer: options.userTransformer,
      apiError: options.apiError,
      adminSettings: options.keycloakConfig.admin,
      clientSettings: options.keycloakConfig.client,
      realm: options.keycloakConfig.realm,
      tokenUrl: options.keycloakConfig.tokenUrl,
      redirectUri: options.keycloakConfig.redirectUri
    });
    /**
     * Connect instance
     */
    const connectSettings = {
      realm: this.realm,
      "auth-server-url": this.adminSettings.baseUrl,
      "ssl-required": this.clientSettings.sslRequired,
      "bearer-only": this.clientSettings.bearerOnly,
      "enable-cors": this.clientSettings.enableCors,
      resource: this.clientSettings.clientId,
      credentials: {
        secret: this.clientSettings.secret
      },
      "confidential-port": 0 // what is this?
    };
    this.adminClient = new KeycloakAdmin(this.adminSettings);
    this.connect = new KeycloakConnect({ scope: "openid" }, connectSettings);
    this.connect.accessDenied = (req, res) => {
      const { apiError } = options;
      apiError.setMessage("Not Authorised");
      return res.jerror(apiError);
    };
  }

  // register new users by email
  async register(user, password) {
    const keycloakUser = {
      realm: this.realm,
      enabled: true,
      requiredActions: ["VERIFY_EMAIL"],
      ...user
    };
    try {
      await this.adminClient.auth(this.adminSettings);
      const createdUser = await this.adminClient.users.create(keycloakUser);
      await this.adminClient.users.sendVerifyEmail({
        id: createdUser.id,
        redirectUri: this.redirectUri,
        clientId: this.clientSettings.clientId,
        realm: this.realm
      });
      await this.adminClient.users.resetPassword({
        id: createdUser.id,
        credential: {
          temporary: false,
          type: "password",
          value: password
        },
        realm: this.realm
      });

      return createdUser.id;
    } catch (e) {
      const error = new ValidationError(10000, e.message);
      error.addErrors({ "": { message: e.message } });

      throw error;
    }
  }

  // register new users by phone
  async registerByPhone(user) {
    const keycloakUser = {
      enabled: true,
      realm: this.realm,
      ...user
    };
    try {
      await this.adminClient.auth(this.adminSettings);
      const createdUser = await this.adminClient.users.create(keycloakUser);
      return createdUser.id;
    } catch (e) {
      const message = e.errorMessage;
      const error = new ValidationError(10000, message);
      error.addErrors({ "": { message } });

      throw error;
    }
  }

  // list users
  async list() {
    try {
      await this.adminClient.auth(this.adminSettings);
      const users = await this.adminClient.users.find({ realm: this.realm });

      return users;
    } catch (e) {
      const message = e.errorMessage;
      const error = new ValidationError(10000, message);
      error.addErrors({ "": { message } });

      throw error;
    }
  }

  // remove a user
  async remove(userId) {
    try {
      await this.adminClient.auth(this.adminSettings);
      const deleteUser = await this.adminClient.users.del({
        realm: this.realm,
        id: userId
      });

      return deleteUser;
    } catch (e) {
      const message = e.errorMessage;
      const error = new ValidationError(10000, message);
      error.addErrors({ "": { message } });

      throw error;
    }
  }

  // resend verification email
  async resend(id) {
    try {
      await this.adminClient.auth(this.adminSettings);
      await this.adminClient.users.sendVerifyEmail({
        id: id,
        redirectUri: this.redirectUri,
        clientId: this.clientSettings.clientId,
        realm: this.realm
      });
    } catch (e) {
      const message = e.errorMessage;
      const error = new ValidationError(10000, message);
      error.addErrors({ "": { message } });

      throw error;
    }
  }

  // authenticate users
  async authenticate(username, password) {
    const { grantType } = this.adminSettings;
    const body = qs.stringify({
      username,
      password,
      grant_type: grantType,
      client_id: this.clientSettings.clientId,
      client_secret: this.clientSettings.secret,
      scope: "openid"
    });
    const uri = `${this.adminSettings.baseUrl}/realms/${this.realm}/${this.tokenUrl}`;

    const response = await axios.post(uri, body);

    return response.data;
  }

  // change password
  async reset(id) {
    await this.adminClient.auth(this.adminSettings);

    await this.adminClient.users.executeActionsEmail({
      realm: this.realm,
      id,
      actions: ["UPDATE_PASSWORD"]
    });
  }

  // refresh token
  async refreshToken(data) {
    const uri = `${this.adminSettings.baseUrl}/realms/${this.realm}/${this.tokenUrl}`;
    const body = {
      refresh_token: data.refreshToken,
      grant_type: "refresh_token",
      client_id: this.clientSettings.clientId,
      client_secret: this.clientSettings.secret
    };
    const response = await axios.post(uri, qs.stringify(body));
    return response.data;
  }

  async getUserByEmail(email) {
    await this.adminClient.auth(this.adminSettings);
    const query = { email, realm: this.realm };

    const users = await this.adminClient.users.find(query);
    if (users && users.length) {
      return users.find(item => item.email === email);
    }

    return null;
  }

  async syncUser(username, next) {
    const keycloakUser = PhoneHelper.isValid(username)
      ? await this.getUserByPhone(username)
      : await this.getUserByEmail(username);
    const mongoUser = new this.userModel({
      firstname: keycloakUser.firstName,
      lastname: keycloakUser.lastName,
      email: keycloakUser.email,
      keycloakId: keycloakUser.id,
      username: keycloakUser.username
    });
    if (keycloakUser.attributes && keycloakUser.attributes.phone) {
      const { phone } = keycloakUser.attributes;
      if (Array.isArray(phone) && phone.length === 1) {
        mongoUser.phone = phone[0];
      } else {
        mongoUser.phone = phone;
      }
    }
    try {
      const savedUser = await mongoUser.save();
      return savedUser;
    } catch (e) {
      next(e);
    }
  }

  async getUserMiddleware(req, res, next) {
    let user;
    if (req.kauth && req.kauth.grant && req.kauth.grant.access_token) {
      const accessToken = req.kauth.grant.access_token;
      if (accessToken) {
        const content = accessToken.content;
        if (content) {
          const { azp: clientId, preferred_username: username, email } = content;
          req.email = email;
          try {
            if (PhoneHelper.isValid(username)) {
              user = await this.userModel.findByPhone(username);
            } else if (email) {
              user = await this.userModel.findByEmail(email);
            }
          } catch (e) {
            // error in mongo fetch the user from keycloak and save it
            user = await this.syncUser(username, next);
          }
          if (user) {
            const userObject = user.toObject();

            // gather client roles from keycloak
            if (content.resource_access && content.resource_access[clientId]) {
              const clientAccess = content.resource_access[clientId];
              if (clientAccess.roles) {
                userObject.clientRoles = clientAccess.roles;
              }
            }

            // get realm roles
            if (content.realm_roles) {
              userObject.realmRoles = content.realm_roles;
            }

            // merge roles
            userObject.roles = mergeArrays(userObject.clientRoles, userObject.realmRoles);

            const userJson = new this.userTransformer().transform(userObject);
            req.user = userJson;
          }
        }
      }
    }
    next();
  }

  async syncUserByPhone(phone) {
    const formattedPhone = PhoneHelper.getPhone(phone);
    const keycloakUser = await this.getUserByPhone(formattedPhone);
    const mongoUser = new this.userModel({
      firstname: keycloakUser.firstName,
      lastname: keycloakUser.lastName,
      email: keycloakUser.email,
      keycloakId: keycloakUser.id,
      username: formattedPhone,
      phone: formattedPhone
    });

    const savedUser = await mongoUser.save();
    return savedUser;
  }

  async getUserByPhone(phone) {
    await this.adminClient.auth(this.adminSettings);
    const query = { username: phone, realm: this.realm };

    const users = await this.adminClient.users.find(query);
    if (users && users.length) {
      const user = users.shift();
      return user;
    }

    return null;
  }

  async createGroupIfMissing(name) {
    await this.adminClient.auth(this.adminSettings);
    const groups = await this.adminClient.groups.find({
      realm: this.realm
    });
    const foundGroup = groups.find(item => item.name === name);
    if (foundGroup) {
      return foundGroup;
    }
    const group = await this.adminClient.groups.create({
      name,
      realm: this.realm
    });
    return group;
  }

  async createRoleIfMissing(name) {
    await this.adminClient.auth(this.adminSettings);
    const foundRole = await this.adminClient.roles.findOneByName({
      name,
      realm: this.realm
    });
    if (foundRole && foundRole.name) {
      return foundRole.name;
    }
    const role = await this.adminClient.roles.create({
      name,
      realm: this.realm
    });
    return role.roleName;
  }

  async createGroupRoleMapping(groupId, roleName) {
    await this.adminClient.auth(this.adminSettings);
    const role = await this.adminClient.roles.findOneByName({
      name: roleName,
      realm: this.realm
    });
    await this.adminClient.groups.addRealmRoleMappings({
      id: groupId,
      roles: [
        {
          id: role.id,
          name: role.name
        }
      ],
      realm: this.realm
    });
  }

  async addToGroup(groupId, userId) {
    await this.adminClient.auth(this.adminSettings);
    await this.adminClient.users.addToGroup({
      groupId,
      id: userId,
      realm: this.realm
    });
  }

  async deleteFromGroup(groupId, userId) {
    await this.adminClient.auth(this.adminSettings);
    await this.adminClient.users.delFromGroup({
      groupId,
      id: userId,
      realm: this.realm
    });
  }
}

export default Keycloak;
