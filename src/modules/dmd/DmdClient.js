import { ElasticService, AggregationSearchQuery, SearchQuery } from "../elasticsearch";
import utils from "./utils";

/**
 * A Helper class for elasticsearch operations
 */
class DmdClient {
  constructor(config = {}, schema) {
    this.config = config;
    this.elasticService = new ElasticService(config.elasticsearch, schema);
    this.schema = schema;
  }

  // Search data
  async search(query) {
    const searchQuery = new SearchQuery(utils.translateQuery(query), this.schema, this.config.type);
    return await this.elasticService.search(searchQuery);
  }

  async aggregate(schema, query) {
    const aggregationQuery = new AggregationSearchQuery(
      utils.translateQuery(query),
      schema,
      this.config.type
    );

    return await this.elasticService.search(aggregationQuery);
  }

  async findById(id) {
    return await this.elasticService.get(id);
  }
}

export default DmdClient;
