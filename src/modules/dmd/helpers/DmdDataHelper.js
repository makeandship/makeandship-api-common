const MAPPING = {
  vppid: "vmppId",
  appid: "amppId",
  apid: "ampId",
  vpid: "vmpId",
  vtmid: "vtmId",
  nm: "name",
  desc: "description",
  packinfo: "packInfo",
  priceinfo: "priceInfo",
  pricedt: "priceDate",
  priceprev: "previousPrice",
  pricebasiscd: "priceBasisCode",
  legalcatcd: "legalCategoryCode",
  discdt: "discDate",
  prescinfo: "prescInfo",
  reimbinfo: "reimbInfo",
  disccd: "discCode",
  suppcd: "supplierCode",
  availrestrictcd: "availabilityRestrictionCode",
  licauthcd: "licensingAuthorityCode",
  nmchangecd: "nameChangeCode",
  basiscd: "basisCode",
  nmdt: "nameDate",
  UdfsUnit: "udfsUnit",
  nmprev: "previousName",
  unitdoseuomcd: "unitDoseCode",
  dfindcd: "dfIndicatorCode",
  qtyval: "quantityValue",
  qtyuomcd: "quantityUnitCode",
  vtmidprev: "previousVtmId",
  vtmiddt: "vtmIdDate"
};

class DmdDataHelper {
  // map the result
  static mapResult(result) {
    const mapped = {};
    const keys = Object.keys(result);
    keys.forEach(key => {
      if (MAPPING[key] && result[key] && typeof result[key] === "object") {
        mapped[MAPPING[key]] = this.mapResult(result[key]);
      } else if (MAPPING[key] && result[key]) {
        mapped[MAPPING[key]] = result[key];
      } else if (result[key] && typeof result[key] === "object") {
        mapped[key] = this.mapResult(result[key]);
      } else if (result[key]) {
        mapped[key] = result[key];
      }
    });
    return mapped;
  }
}

export default DmdDataHelper;
