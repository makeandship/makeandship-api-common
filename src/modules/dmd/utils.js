const MAPPING = {
  vmppId: "vppid",
  amppId: "appid",
  ampId: "apid",
  vmpId: "vpid",
  vtmId: "vtmid"
};

/**
 * Translate the query from the client to match es attributes
 * @param {*} query
 */
const translateQuery = query => {
  const translated = {};
  const keys = Object.keys(query);
  keys.forEach(key => {
    if (MAPPING[key]) {
      translated[MAPPING[key]] = query[key];
    } else {
      translated[key] = query[key];
    }
  });
  return translated;
};

const utils = Object.freeze({
  translateQuery
});

export default utils;
