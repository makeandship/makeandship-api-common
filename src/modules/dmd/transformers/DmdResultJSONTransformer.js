import ElasticsearchJSONTransformer from "../../elasticsearch/transformers/ElasticsearchJSONTransformer";
import HitsJSONTransformer from "../../elasticsearch/transformers/HitsJSONTransformer";
import SuggestJSONTransformer from "../../elasticsearch/transformers/SuggestJSONTransformer";
import DmdDataHelper from "../helpers/DmdDataHelper";

/**
 * Translate an elasticsearch response onto an Medicine response
 */
class DmdResultJSONTransformer extends ElasticsearchJSONTransformer {
  /**
   * Convert an elasticsearch response
   *
   * @param {object} o the elasticsearch result
   */
  transform(o, options) {
    const res = super.transform(o, options);
    const hits = res.suggest
      ? new SuggestJSONTransformer().transform(res, { name: "dmd", ...options })
      : new HitsJSONTransformer().transform(res, options);

    const results = hits.map(element => {
      return {
        ...element._source,
        relevance: element._score
      };
    });

    const mappedResults = [];
    results.forEach(result => {
      mappedResults.push(DmdDataHelper.mapResult(result));
    });

    // add regimens where they exist
    if (options && options.regimensByAmppId) {
      mappedResults.forEach(mappedResult => {
        const amppId = mappedResult.amppId;
        if (options.regimensByAmppId[amppId]) {
          mappedResult.regimens = options.regimensByAmppId[amppId];
        }
      });
    }

    return mappedResults;
  }
}

export default DmdResultJSONTransformer;
