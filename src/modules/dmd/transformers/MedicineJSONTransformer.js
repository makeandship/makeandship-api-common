import ElasticsearchJSONTransformer from "../../elasticsearch/transformers/ElasticsearchJSONTransformer";
import DmdDataHelper from "../helpers/DmdDataHelper";

/**
 * Transform a single dmd document
 */
class MedicineJSONTransformer extends ElasticsearchJSONTransformer {
  /**
   * Transform a dmd document
   *
   * @param {object} o the object to transform
   */
  transform(o) {
    const res = super.transform(o);
    const source = res._source;

    return DmdDataHelper.mapResult(source);
  }
}

export default MedicineJSONTransformer;
