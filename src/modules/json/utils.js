/**
 * flatten the object to return readable keys
 * @param {*} obj
 * @param {*} path
 */
const flatten = (obj, path = "") => {
  if (!(obj instanceof Object)) return { [path.replace(/\.$/g, "")]: obj };

  return Object.keys(obj).reduce((output, key) => {
    return { ...output, ...flatten(obj[key], path + key + ".") };
  }, {});
};

/**
 * Set a value to an object
 * @param {*} propertyPath
 * @param {*} value
 * @param {*} obj
 */
const setValue = (propertyPath, value, obj) => {
  let properties = Array.isArray(propertyPath) ? propertyPath : propertyPath.split(".");
  if (properties.length > 1) {
    const checkProperty =
      typeof obj.toObject === "function"
        ? Object.prototype.hasOwnProperty.call(obj.toObject(), properties[0])
        : Object.prototype.hasOwnProperty.call(obj, properties[0]);
    if (!checkProperty || typeof obj[properties[0]] !== "object") obj[properties[0]] = {};
    return setValue(properties.slice(1), value, obj[properties[0]]);
  } else {
    obj[properties[0]] = value;
    return true;
  }
};

const utils = Object.freeze({
  setValue,
  flatten
});

export default utils;
