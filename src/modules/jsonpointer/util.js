import jsonpointer from "jsonpointer";

/**
 * Create a set of jsonPointers for an object given a json pointer
 * potentially containing wildcards and an object to evaluate
 *
 * e.g.
 * /people//firstname
 * with
 * { "people": [ {firstname: 'Mark"}, {firstname: "Adam" } ] }
 *
 * will return pointers
 * /people/0/firstname
 * /people/1/firstname
 *
 * @param {string} jsonPointer
 * @param {object} data
 *
 * @return {array} jsonPointers
 */
const getPointers = (jsonPointer, data) => {
  const pointers = [];

  if (jsonPointer.indexOf("//") === -1) {
    pointers.push(jsonPointer);
  } else {
    const segments = jsonPointer.split("//");
    const segmentsWithoutTrailingSlashes = segments
      .map(segment => {
        if (segment.startsWith("/")) {
          return segment.slice(1);
        } else {
          return segment;
        }
      })
      .filter(Boolean);
    const indexedPointers = getIndexedPointers("", segmentsWithoutTrailingSlashes, data);
    if (indexedPointers && indexedPointers.length) {
      pointers.push(...indexedPointers);
    }
  }

  return pointers;
};

const getIndexedPointers = (base, segments, data) => {
  const pointers = [];
  if (segments && segments.length) {
    const segment = segments.shift();

    // root of the pointer to extend
    const pointerBase = [base];
    if (segment) {
      pointerBase.push(segment);
    }

    // check for arrays to expand
    const matches = jsonpointer.get(data, `/${segment}`);
    if (matches && Array.isArray(matches) && matches.length) {
      matches.forEach((item, index) => {
        const indexedPointerBase = [];
        indexedPointerBase.push(...pointerBase);
        indexedPointerBase.push(index);

        if (segments && segments.length) {
          // recurse using cloned segments array to preserve it for each indexed item
          const subpointers = getIndexedPointers(
            `${indexedPointerBase.join("/")}`,
            JSON.parse(JSON.stringify(segments)),
            matches[index]
          );
          if (subpointers) {
            pointers.push(...subpointers);
          }
        } else {
          // return indexed pointer if there are no deeper segments to process
          pointers.push(`${indexedPointerBase.join("/")}`);
        }
      });
    } else {
      if (segments && segments.length) {
        // process remaining segments asssuming a single array element
        const indexedPointerBase = [];
        indexedPointerBase.push(...pointerBase);
        indexedPointerBase.push("0");

        const subpointers = getIndexedPointers(
          `${indexedPointerBase.join("/")}`,
          JSON.parse(JSON.stringify(segments)),
          {}
        );
        if (subpointers) {
          pointers.push(...subpointers);
        }
      } else {
        // return the current segment if there are no indexed items
        pointers.push(`${pointerBase.join("/")}`);
      }
    }
  }

  return pointers;
};

const util = Object.freeze({
  getPointers
});

export default util;
