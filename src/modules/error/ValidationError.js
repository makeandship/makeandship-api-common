import httpCodes from "http-status";
import APIError from "./APIError";
/**
 * A class for handeling validation errors
 */
class ValidationError extends APIError {
  constructor(code, message, data = { errors: {} }, httpStatus = httpCodes.OK) {
    super(code, message, data, httpStatus);
  }

  addError(error) {
    const { path } = error;
    this.data.errors[path] = error;
  }

  addErrors(errors) {
    Object.keys(errors).forEach(key => {
      this.data.errors[key] = errors[key];
    });
  }
}

export default ValidationError;
