import httpCodes from "http-status";

/**
 * A class for handeling api errors
 */
class APIError {
  constructor(code, message, data, httpStatus = httpCodes.OK) {
    this.message = message;
    this.code = code;
    this.data = data;
    this.httpStatus = httpStatus;
  }

  setCode(code) {
    this.code = code;
  }

  setMessage(message) {
    this.message = message;
  }

  setData(data) {
    this.data = data;
  }

  setHttpStatus(httpStatus) {
    this.httpStatus = httpStatus;
  }

  getCode() {
    return this.code;
  }

  getMessage() {
    return this.message;
  }

  getData() {
    return this.data;
  }

  getHttpStatus() {
    return this.httpStatus;
  }
}

export default APIError;
