import APIError from "./APIError";
/**
 * A class for handeling authentication errors
 */
class AuthError extends APIError {
  constructor(code, message, data, httpStatus) {
    super(code, message, data, httpStatus);
  }
}

export default AuthError;
