import httpCodes from "http-status";
import APIError from "./APIError";
/**
 * A class for handeling runtime errors
 */
class RuntimeError extends APIError {
  constructor(code, message, data, httpStatus = httpCodes.INTERNAL_SERVER_ERROR) {
    super(code, message, data, httpStatus);
  }
}

export default RuntimeError;
