import APIError from "./APIError";
import ValidationError from "./ValidationError";

/**
 * A class for converting errors to standard api errors
 */
class ErrorUtil {
  /**
   * Convert AJV Error to ValidationError
   * @param {*} error
   * @param {*} code
   */
  static convertAjvToApi(error, code) {
    const validationError = new ValidationError(code, error.message);
    const { errors } = error;

    if (errors) {
      validationError.addErrors(errors);
    }

    return validationError;
  }

  /**
   * Convert mongoose Error to ValidationError
   * @param {*} error
   * @param {*} code
   */
  static convertMongooseToApi(error, code) {
    const validationError = new ValidationError(code, error.message);
    const { errors } = error;

    if (errors) {
      Object.keys(errors).forEach(key => {
        const err = errors[key];
        delete err.properties;
        delete err.name;
        validationError.addError(err);
      });
    }

    return validationError;
  }

  /**
   * A generic conversion function
   * @param {*} error
   * @param {*} code
   */
  static convert(error, code) {
    if (this.isAjvValidationError(error)) {
      return this.convertAjvToApi(error, code);
    } else if (this.isMongooseValidationError(error)) {
      return this.convertMongooseToApi(error, code);
    }
    return new APIError(code, error.message, error);
  }

  /**
   * Check if the error is ajv validation error
   * @param {*} error
   */
  static isAjvValidationError(error) {
    return error.errors;
  }

  /**
   * Check if the error is a mongoose validation error
   * @param {*} error
   */
  static isMongooseValidationError(error) {
    return error.name === "ValidationError";
  }
}

export default ErrorUtil;
