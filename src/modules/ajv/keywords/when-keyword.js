import ajvUtil from "ajv/lib/compile/util";
import jsonpointer from "jsonpointer";
import jsonpointerUtil from "../../jsonpointer/util";
import _ from "lodash";

const whenKeyword = ajv => {
  const IDENTIFIER = /^[a-z$_][a-z$_0-9]*$/i;
  const INTEGER = /^[0-9]+$/;

  const META_SCHEMA_ID = "http://json-schema.org/draft-07/schema";

  // from ajv-keywords/keywords/_util.js
  const getMetaSchemaRef = ajv => {
    const defaultMeta = ajv._opts.defaultMeta;

    if (typeof defaultMeta == "string") {
      return { $ref: defaultMeta };
    }
    if (ajv.getSchema(META_SCHEMA_ID)) {
      return { $ref: META_SCHEMA_ID };
    }

    console.warn("meta schema not defined"); // eslint-disable-line no-console

    return {};
  };
  const metaSchemaRef = getMetaSchemaRef(ajv);

  const whenSpecification = {
    type: "object",
    validate: function validateSwitch(schema, data, parentSchema) {
      // ajv expects errors to be added to the function instance
      const errors = [];

      const unifiedDeepRequired = [];

      const params = {};

      schema.forEach(definition => {
        // compile and evaluate each schema
        const validate = ajv.compile(definition.case);
        const match = validate(data);

        if (match) {
          unifiedDeepRequired.push(...definition.deepRequired);
          params.case = definition.case;
        }
      });

      if (unifiedDeepRequired && unifiedDeepRequired.length) {
        const deepRequired = _.uniq(unifiedDeepRequired);

        const jsonPointers = [];
        deepRequired.forEach(requiredJsonPointer => {
          const requiredJsonPointers = jsonpointerUtil.getPointers(requiredJsonPointer, data);
          jsonPointers.push(...requiredJsonPointers);
        });

        jsonPointers.forEach(jsonPointer => {
          const value = jsonpointer.get(data, jsonPointer);
          const property = getPropertyName(jsonPointer);
          if (typeof value === "undefined" || value === null) {
            // select a path based on options settings
            const dataPath = ajv._opts.jsonPointers
              ? jsonPointer
              : getDotNotationFromJsonPointer(jsonPointer);

            params.required = jsonPointer;

            errors.push({
              keyword: "switch",
              dataPath,
              message: `should have required property '${property}'`,
              schema,
              parentSchema,
              data,
              params
            });
          }
        });
      }

      if (errors && errors.length) {
        validateSwitch.errors = errors;
        return false;
      }

      // no matching cases considered valid
      return true;
    },
    metaSchema: {
      type: "array",
      items: {
        required: ["case", "deepRequired"],
        properties: {
          case: metaSchemaRef,
          deepRequired: {
            type: "array",
            items: {
              type: "string",
              format: "json-pointer"
            }
          }
        }
      }
    }
  };

  /**
   * Get a wildcarded dot notation expression from a jsonPointer
   *
   * Convert // into a wildcard to support array matching without
   * needing to use indexes
   *
   * e.g.
   * /type/subType becomes type.subType
   * /involved//type beccomes involved[*].type
   * /involved/0/type becomes involved[0].type
   *
   * @param {string} jsonPointer
   * @return {string} dot notation expression
   */
  const getDotNotationFromJsonPointer = jsonPointer => {
    if (jsonPointer) {
      const segments = jsonPointer.split("/");
      if (segments && segments.length) {
        segments.shift(); // remove first element is empty due to preceeding /
      }
      const dotNotation = [];
      segments.forEach((segment, index) => {
        const unescapedSegment = ajvUtil.unescapeJsonPointer(segment);

        if (unescapedSegment === null || unescapedSegment === "") {
          if (index > 0) {
            dotNotation.push(".");
          }
          dotNotation.push("*");
        } else if (INTEGER.test(unescapedSegment)) {
          dotNotation.push(`[${segment}]`);
        } else if (IDENTIFIER.test(segment)) {
          if (index > 0) {
            dotNotation.push(".");
          }
          dotNotation.push(`${segment}`);
        } else {
          dotNotation.push(`['${segment}]`);
        }
      });

      return dotNotation.join("");
    }

    return null;
  };

  /**
   * Given a json pointer return the name of the lowest nested property
   * e.g.
   * /type/subType will return subType
   *
   * @param {string} jsonPointer
   * @return {string} property
   */
  const getPropertyName = jsonPointer => {
    if (jsonPointer) {
      if (jsonPointer.indexOf("/")) {
        return jsonPointer;
      } else {
        const segments = jsonPointer.split("/");
        const property = segments.pop();
        return property;
      }
    }
    return null;
  };

  ajv.addKeyword("when", whenSpecification);
  return ajv;
};

export default whenKeyword;
