/**
 * Formats an array of errors to a standard format
 *
 * @param {array} errors
 *
 * @return {object} validationError
 */
const formatErrors = errors => {
  const validationError = { errors: {} };
  errors.forEach(error => {
    const path = error.dataPath.startsWith(".") ? error.dataPath.slice(1) : error.dataPath;

    const formattedError = {
      path,
      message: error.message
    };
    validationError.errors[formattedError.path] = formattedError;
  });
  return validationError;
};

export { formatErrors };
export default { formatErrors };
