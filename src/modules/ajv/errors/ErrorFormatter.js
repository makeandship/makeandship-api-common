import Formatter from "./Formatter";

class ErrorFormatter extends Formatter {
  format(errors) {
    const formatted = {};

    errors.forEach(error => {
      const dataPath = error.dataPath.startsWith(".") ? error.dataPath.slice(1) : error.dataPath;

      // https://github.com/epoberezkin/ajv#error-parameters
      Object.keys(error.params).forEach(type => {
        const errorPath = error.params[type];

        // specific error types move property into a param
        const path = ["missingProperty"].includes(type)
          ? [dataPath, errorPath].filter(Boolean).join(".")
          : [dataPath].filter(Boolean).join(".");

        const formattedError = {
          path,
          type: error.keyword,
          params: error.params,
          message: error.message
        };

        formatted[formattedError.path] = formattedError;
      });
    });

    return formatted;
  }
}

export default ErrorFormatter;
