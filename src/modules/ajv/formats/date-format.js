const validate = data => {
  const date = new Date(data);
  return isValidDate(date);
};

const isValidDate = d => d instanceof Date && !isNaN(d);

export default {
  validate
};
