import date from "./date-format";
import binary from "./binary-format";

export default {
  date,
  binary
};
