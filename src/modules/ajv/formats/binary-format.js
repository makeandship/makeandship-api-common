import File from "../../../util/file";

const validate = data => {
  const file = File.decodeBase64(data);
  return !!file;
};

export default {
  validate
};
