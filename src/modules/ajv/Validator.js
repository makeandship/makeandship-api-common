import Ajv from "ajv";
import nhsNumberValidator from "nhs-number-validator";

import normalizer from "../jsonschema/normalizer";

import keywords from "./keywords";
import formats from "./formats";

import ErrorFormatter from "./errors/ErrorFormatter";

class Validator {
  constructor(schema, options = {}) {
    this.schema = schema;

    const defaults = {
      allErrors: true,
      $data: true,
      removeAdditional: false,
      format: "full"
    };
    this.options = Object.assign({}, defaults, options);

    this.ajv = this.init(this.options);
  }

  init(options = {}) {
    const ajv = new Ajv(options);
    keywords.when(ajv);

    ajv.addFormat("nhsNumber", data => nhsNumberValidator.validate(data));
    ajv.addFormat("date", data => formats.date.validate(data));
    ajv.addFormat("binary", data => formats.binary.validate(data));

    return ajv;
  }

  validate(data) {
    const validate = this.ajv.compile(this.schema);
    const body = normalizer.normalize(this.schema, data);
    const valid = validate(body);

    if (!valid) {
      return this.format(validate.errors);
    }

    return null;
  }

  format(errors) {
    return new ErrorFormatter().format(errors);
  }
}

export default Validator;
