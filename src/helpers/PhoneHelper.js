import { PhoneNumberUtil } from "google-libphonenumber";

const phoneUtil = PhoneNumberUtil.getInstance();
const PhoneNumberFormat = {
  E164: 0,
  INTERNATIONAL: 1,
  NATIONAL: 2,
  RFC3966: 3
};

class PhoneHelper {
  static getPhone(phone) {
    const number = phoneUtil.parseAndKeepRawInput(phone);
    const formatted = phoneUtil.format(number, PhoneNumberFormat.E164);
    return formatted;
  }

  static isValid(phone) {
    try {
      phoneUtil.parseAndKeepRawInput(phone);
      return true;
    } catch (e) {
      return false;
    }
  }
}

export default PhoneHelper;
