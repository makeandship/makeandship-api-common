import email from "./email";

export default {
  notification: "email",
  username: "email",
  email
};
