import body from "./body";
import rateLimit from "./rate-limit";

export default Object.assign({ port: 3000, swaggerApis: "./**/*.route.js" }, body, rateLimit);
