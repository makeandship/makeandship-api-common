export default {
  rateLimitReset: 15 * 60 * 1000,
  rateLimitMax: 1000
};
