import nodemailer from "nodemailer";

export default {
  accounts: {
    jwtSecret: process.env.JWT_SECRET
  },
  communications: {
    email: {
      nodemailer
    }
  },
  express: {
    uploadDir: "/app/uploads",
    apiBaseUrl: process.env.API_HOST,
    swaggerApis: "/app/dist/server/routes/*.route.js"
  }
};
