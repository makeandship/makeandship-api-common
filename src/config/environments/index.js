import development from "./development";
import test from "./test";
import production from "./production";

export default {
  development,
  test,
  production
};
