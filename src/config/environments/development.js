import nodemailer from "nodemailer-mock";

export default {
  db: {
    MONGOOSE_DEBUG: true
  },
  accounts: {
    jwtSecret: process.env.JWT_SECRET
  },
  communications: {
    email: {
      nodemailer
    }
  },
  express: {
    uploadDir: "/tmp/uploads",
    apiBaseUrl: "localhost:3000"
  }
};
