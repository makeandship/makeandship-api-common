import deepmerge from "deepmerge";

import accounts from "./accounts";
import communications from "./communications";
import elasticsearch from "./elasticsearch";
import express from "./express";

import environments from "./environments";

const env = process.env.NODE_ENV || "development";
const environmentConfig = environments[env];

const config = deepmerge(
  {
    accounts,
    communications,
    elasticsearch,
    express
  },
  environmentConfig
);
export default config;
