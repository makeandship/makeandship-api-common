import fs from "fs";
import Promise from "bluebird";
import crypto from "crypto";

class File {
  static async verifyFile(filepath, delay = 50, retries = 3) {
    const delays = [];
    [...Array(retries)].forEach(() => {
      const currentDelay = delays.length ? delays[delays.length - 1] : 0;
      delays.push(currentDelay === 0 ? delay : currentDelay * 2);
    });

    const promises = [];
    promises.push(File.delayedExists(filepath, 0));
    delays.forEach(delay => {
      promises.push(File.delayedExists(filepath, delay));
    });

    try {
      const result = await Promise.any(promises);
      return Promise.resolve(result);
    } catch (e) {
      if (typeof e === "object" && e["0"]) {
        return Promise.reject(e["0"]);
      } else {
        Promise.reject(e);
      }
    }
  }

  // wait [delay]ms to check if a file exists
  static delayedExists(filepath, delay) {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        try {
          const result = await File.exists(filepath);
          resolve(result);
        } catch (e) {
          reject(e);
        }
      }, delay);
    });
  }

  static exists(filepath) {
    return new Promise((resolve, reject) => {
      fs.stat(filepath, err => {
        if (err) {
          return reject(err);
        } else {
          return resolve(filepath);
        }
      });
    });
  }

  static decodeBase64(data) {
    const matches = data.match(/^data:(.*?);base64,(.+)$/);

    const file = {};

    if (!matches || matches.length !== 3) {
      return null;
    }

    // metadata
    const metadata = matches[1];
    const items = metadata.split(";");
    if (items && items.length) {
      file.mimetype = items[0];
      items.forEach((item, i) => {
        if (i === 0) {
          file.type = item;
        } else {
          const parts = item.split("=");
          file[parts[0]] = parts[1];
        }
      });
    }

    // file data
    const binary = matches[2];
    file.data = new Buffer.from(binary, "base64");

    return file;
  }

  static getRandomFilename() {
    return new Promise((resolve, reject) => {
      crypto.pseudoRandomBytes(16, (err, raw) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(raw.toString("hex"));
        }
      });
    });
  }

  static getSize(filepath) {
    return new Promise((resolve, reject) => {
      fs.stat(filepath, (err, stats) => {
        if (err) {
          return reject(err);
        } else {
          return resolve(stats["size"]);
        }
      });
    });
  }

  /**
   * Copy over the file
   * @param {*} source
   * @param {*} target
   */
  static copy(source, target) {
    return new Promise((resolve, reject) => {
      fs.copyFile(source, target, err => {
        if (err) {
          return reject(err);
        } else {
          return resolve(target);
        }
      });
    });
  }

  /**
   * Remove a file from a given location
   * @param {*} target
   */
  static remove(target) {
    return new Promise((resolve, reject) => {
      fs.unlink(target, err => {
        if (err) {
          return reject(err);
        } else {
          return resolve(true);
        }
      });
    });
  }
}

export default File;
