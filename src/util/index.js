import File from "./file";

const util = Object.freeze({
  File
});

export default util;
