import config from "./config";
import elasticsearch from "./modules/elasticsearch";
import ajv from "./modules/ajv";
import jsonpointer from "./modules/jsonpointer";
import Randomizer from "./modules/schema-faker/Randomizer";
import DmdClient from "./modules/dmd/DmdClient";

/**
 * Enable module-based imports e.g.
 *
 * import { keywords } from "makeandship-api-common/modules/ajv";
 * const when = keywords.when;
 * import { util } from "makeandship-api-common/modules/jsonpointer";
 * import config from "makeandship/config";
 */

export default {
  config,
  elasticsearch,
  ajv,
  jsonpointer,
  Randomizer,
  DmdClient
};
